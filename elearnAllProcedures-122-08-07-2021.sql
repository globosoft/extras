-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.60 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

ALTER TABLE `attendance`
	CHANGE COLUMN `date` `date` DATE NULL AFTER `id`;
UPDATE exercise SET exercise.mark = 1 WHERE exercise.mark IS NULL;
UPDATE exam SET exam.negativeMark = 0  WHERE exam.negativeMark IS null ;
UPDATE newexam SET newexam.negativeMark = 0  WHERE newexam.negativeMark IS null;

-- Dumping structure for procedure newprocedures.checkSubscriptionLimit
DROP PROCEDURE IF EXISTS `checkSubscriptionLimit`;
DELIMITER //
CREATE PROCEDURE `checkSubscriptionLimit`(
	IN `collegeId` VARCHAR(20),
	IN `keyCount` VARCHAR(10)
)
BEGIN


SELECT collegesubscription.userCount FROM collegesubscription WHERE collegesubscription.isActive = 1 
AND collegesubscription.isDeleted = 0 AND collegesubscription.collegeId = collegeId 
AND keyCount <= collegesubscription.userCount 
AND  (SELECT (COUNT(*) + keyCount) FROM studentsubscription 
 WHERE studentsubscription.collegeId = collegeId 
 AND studentsubscription.isActive = 1 AND studentsubscription.isDeleted = 0) <= collegesubscription.userCount LIMIT 1;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.common_getLiveClassVideos
DROP PROCEDURE IF EXISTS `common_getLiveClassVideos`;
DELIMITER //
CREATE PROCEDURE `common_getLiveClassVideos`(
	IN `role` VARCHAR(15),
	IN `username` VARCHAR(350)

)
BEGIN


IF role = 'STUDENT' THEN
 SELECT student.standard INTO @classid FROM student WHERE student.username = username AND student.isDeleted = 0;
END IF;

IF role='ADMIN' || role='TEACHER' THEN
 SELECT teacher.id INTO @userid FROM teacher WHERE teacher.userName=username AND teacher.isDeleted=0;
 END IF;  

SELECT liveclassdtls.meetingid, liveclassdtls.id,liveclassdtls.userName , DATE_FORMAT(liveclassdtls.createdDate,'%d/%m/%Y') 
AS createdDate , DATE_FORMAT(liveclassdtls.createdDate,'%h:%i %p') AS createdTime , liveclassdtls.isConverted ,
typesclass.className AS standard , liveclassdtls.internalMeetingId ,
(case when (CONVERT(TIME_FORMAT(TIMEDIFF(CURRENT_TIMESTAMP,liveclassdtls.createdDate), '%H'), SIGNED) <= 12 ) 
then TRUE ELSE FALSE END) AS isNow,liveclassserver.secret, liveclassserver.url , 
CONCAT(typesclass.className, '_', DATE_FORMAT(liveclassdtls.createdDate,'%d-%m-%Y')) AS meetingName
FROM liveclassdtls JOIN typesclass ON typesclass.id = liveclassdtls.standard  
JOIN liveclassserver ON liveclassserver.id = liveclassdtls.serverId  
WHERE liveclassdtls.isDeleted = 0 AND liveclassdtls.isDeleted = 0
 AND (CASE WHEN (role = 'TEACHER') THEN  liveclassdtls.userId = @userid  ELSE 
  (case when (role = 'STUDENT') THEN  liveclassdtls.standard = @classid ELSE  '1'='1' END)   END)
 and liveclassdtls.internalMeetingId
 IS NOT NULL ORDER BY liveclassdtls.id DESC ;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.examfile_clearExamFileAnswerBeforeInsertion
DROP PROCEDURE IF EXISTS `examfile_clearExamFileAnswerBeforeInsertion`;
DELIMITER //
CREATE PROCEDURE `examfile_clearExamFileAnswerBeforeInsertion`(
	IN `assignedExamId` INT,
	IN `sid` INT
)
BEGIN

DELETE FROM examfileanswer WHERE examfileanswer.assignedExamId = assignedExamId AND examfileanswer.sid = sid; 

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.examfile_getTotalMarkFromAssignedExamId
DROP PROCEDURE IF EXISTS `examfile_getTotalMarkFromAssignedExamId`;
DELIMITER //
CREATE PROCEDURE `examfile_getTotalMarkFromAssignedExamId`(
	IN `assignedExamId` INT
)
BEGIN

SELECT examfile.totalMark FROM examfile JOIN assignedexamfile ON examfile.id = assignedexamfile.examId 
WHERE assignedexamfile.id =  assignedExamId ;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.exam_deleteMarkAndQuestion
DROP PROCEDURE IF EXISTS `exam_deleteMarkAndQuestion`;
DELIMITER //
CREATE PROCEDURE `exam_deleteMarkAndQuestion`(
	IN `sid` VARCHAR(10),
	IN `assignedExamId` VARCHAR(10),
	IN `examType` INT
)
BEGIN

IF examType =  '0' THEN

DELETE FROM completedexam WHERE completedexam.assignedExamid = assignedExamId  
       AND completedexam.sid = sid AND completedexam.examtype = 0 ;
DELETE FROM mark WHERE mark.assignedExamId = assignedExamId AND mark.sid = sid and mark.examtype = 0 ;         

ELSE IF examType =  '1' THEN

DELETE FROM completedexam WHERE completedexam.assignedExamid = assignedExamId 
       AND completedexam.sid = sid AND completedexam.examtype = 1;
DELETE FROM mark WHERE mark.assignedExamId = assignedExamId AND mark.sid = sid and mark.examtype = 1 ;  

END IF;
END IF; 

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.exam_getAssignedExamDetails
DROP PROCEDURE IF EXISTS `exam_getAssignedExamDetails`;
DELIMITER //
CREATE PROCEDURE `exam_getAssignedExamDetails`(
	IN `role` VARCHAR(10),
	IN `username` VARCHAR(100)
)
BEGIN

select teacher.id  into @tid from teacher where teacher.userName  = username; 	

SELECT * FROM ( SELECT  exam.userId ,assignedexam.id,TIME_FORMAT(assignedexam.endTime, '%h:%i %p') AS endTime, TIME_FORMAT(assignedexam.startTime,'%h:%i %p') AS startTime,
 DATE_FORMAT(assignedexam.examDate,'%d/%m/%Y') AS examDate ,exam.examName,  typesclass.id as classids , typesclass.className , assignedexam.createdBy ,
assignedexam.groupTimeStamp from assignedexam join exam on exam.id=assignedexam.examId join typesclass ON  typesclass.id=assignedexam.classId 
WHERE assignedexam.isDeleted=0  AND assignedexam.groupTimeStamp 
UNION 
SELECT exam.userId ,assignedexam.id,TIME_FORMAT(assignedexam.endTime, '%h:%i %p') AS endTime, TIME_FORMAT(assignedexam.startTime,'%h:%i %p') AS startTime,
 DATE_FORMAT(assignedexam.examDate,'%d/%m/%Y') AS examDate , exam.examName,  typesclass.id as classIds,typesclass.className , assignedexam.createdBy , 
 assignedexam.groupTimeStamp from assignedexam JOIN  
exam on exam.id=assignedexam.examId join  student ON student.id = assignedexam.studentId JOIN typesclass 
on typesclass.id= student.standard WHERE assignedexam.isDeleted=0 
AND assignedexam.groupTimeStamp ) AS tab WHERE
  (CASE WHEN ( role = 'ADMIN' OR role = 'SADMIN' ) THEN  1 = 1  
   WHEN  (role = 'TEACHER') THEN tab.userId =  @tid 
	ELSE  1 = 2  END) 
GROUP BY tab.groupTimeStamp ;	 
		 

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.exam_getAssignednewExamDetails
DROP PROCEDURE IF EXISTS `exam_getAssignednewExamDetails`;
DELIMITER //
CREATE PROCEDURE `exam_getAssignednewExamDetails`(
  IN role VARCHAR(10),
  IN username VARCHAR(250)
)
BEGIN

SELECT teacher.id into @uid from teacher where teacher.userName  = 	username;
	
SELECT * FROM ( SELECT  newexam.userId ,DATE_FORMAT(assignednewexam.examDate ,'%d/%m/%Y') as examDate,
TIME_FORMAT(assignednewexam.endTime, '%h:%i %p') AS endTime, TIME_FORMAT(assignednewexam.startTime,'%h:%i %p') AS startTime, 
assignednewexam.createdBy,assignednewexam.groupTimeStamp,newexam.examName,  typesclass.id as classids , typesclass.className 
 from assignednewexam join newexam on newexam.id=assignednewexam.examId join typesclass ON 
 typesclass.id=assignednewexam.classId   WHERE assignednewexam.isDeleted=0  GROUP BY assignednewexam.groupTimeStamp 
 UNION 
 SELECT newexam.userId ,DATE_FORMAT(assignednewexam.examDate ,'%d/%m/%Y') as examDate,
TIME_FORMAT(assignednewexam.endTime, '%h:%i %p') AS endTime, TIME_FORMAT(assignednewexam.startTime,'%h:%i %p') AS startTime, 
assignednewexam.createdBy,assignednewexam.groupTimeStamp,
newexam.examName,  typesclass.id as classIds,typesclass.className  from assignednewexam JOIN 
 newexam on newexam.id=assignednewexam.examId join student ON student.id = assignednewexam.studentId JOIN typesclass 
 on typesclass.id= student.standard WHERE assignednewexam.isDeleted=0 GROUP BY assignednewexam.groupTimeStamp ) 
  AS tab WHERE 
   (CASE WHEN ( role = 'ADMIN' ) THEN  1=1 ELSE tab.userId = @uid END) GROUP BY tab.groupTimeStamp ;	
	
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.exam_getRegisteredExamList
DROP PROCEDURE IF EXISTS `exam_getRegisteredExamList`;
DELIMITER //
CREATE PROCEDURE `exam_getRegisteredExamList`(
   IN role VARCHAR(10),
   IN userId VARCHAR(10)
)
BEGIN

SELECT * ,   COALESCE(( SELECT COUNT(*) FROM assignedexam WHERE assignedexam.examId = exam.id AND assignedexam.isDeleted = 0),0) AS isExamined  
 FROM  exam where exam.isDeleted=0
 and (CASE WHEN (role = 'ADMIN' or role = 'SADMIN') THEN 1=1 ELSE exam.userId = userId END)
 ORDER BY exam. createdDate DESC;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.exam_getRegisteredNewExamDetails
DROP PROCEDURE IF EXISTS `exam_getRegisteredNewExamDetails`;
DELIMITER //
CREATE PROCEDURE `exam_getRegisteredNewExamDetails`(
  IN role VARCHAR(10),
  IN username VARCHAR(250)
)
BEGIN
	
SELECT teacher.id into @uid from teacher where teacher.userName  = username;	
	
 SELECT * , COALESCE(( SELECT COUNT(*) FROM assignednewexam 
 WHERE assignednewexam.examId = newexam.id AND assignednewexam.isDeleted = 0),0) AS isExamined
 FROM  newexam where newexam.isDeleted=0 
AND	(CASE WHEN (role = 'ADMIN') THEN 1=1 ELSE newexam.userId = @uid END);
	
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.exam_getStudentAttendedExamCount
DROP PROCEDURE IF EXISTS `exam_getStudentAttendedExamCount`;
DELIMITER //
CREATE PROCEDURE `exam_getStudentAttendedExamCount`(
	IN `role` VARCHAR(10),
	IN `username` VARCHAR(250)
)
BEGIN


SELECT tab.*
FROM(
SELECT *
FROM (
SELECT assignednewexam.id,examname AS examName,1 AS examtype, (
SELECT DATE_FORMAT(assignednewexam.examDate,'%d/%m/%Y')) AS examDate, assignednewexam.examDate AS examDates, TIME_FORMAT(assignednewexam.createdDate, '%h:%i %p') AS `time`, typesclass.id AS classids, assignednewexam.createdDate, assignednewexam.createdBy, assignednewexam.groupTimeStamp
FROM assignednewexam
JOIN newexam ON newexam.id=assignednewexam.examId
JOIN typesclass ON typesclass.id=assignednewexam.classId
WHERE assignednewexam.isDeleted=0 UNION
SELECT assignednewexam.id,examname AS examName,1 AS examtype, (
SELECT DATE_FORMAT(assignednewexam.examDate,'%d/%m/%Y')) AS examDate, assignednewexam.examDate AS examDates, TIME_FORMAT(assignednewexam.createdDate, '%h:%i %p') AS `time`, typesclass.id AS classids, assignednewexam.createdDate, assignednewexam.createdBy, assignednewexam.groupTimeStamp
FROM assignednewexam
JOIN newexam ON newexam.id=assignednewexam.examId
JOIN student ON student.id = assignednewexam.studentId
JOIN typesclass ON typesclass.id= student.standard
WHERE assignednewexam.isDeleted=0) AS tab1
GROUP BY tab1.groupTimeStamp UNION
SELECT *
FROM (
SELECT assignedexam.id,examName,0 AS examtype, (
SELECT DATE_FORMAT(assignedexam.examDate,'%d/%m/%Y')) AS examDate, assignedexam.examDate AS examDates, (TIME_FORMAT(assignedexam.createdDate, '%h:%i %p')) AS `time`, typesclass.id AS classids, assignedexam.createdDate, assignedexam.createdBy, assignedexam.groupTimeStamp
FROM assignedexam
JOIN exam ON exam.id=assignedexam.examId
JOIN typesclass ON typesclass.id=assignedexam.classId
WHERE assignedexam.isDeleted=0 UNION
SELECT assignedexam.id,examName,0 AS examtype, (
SELECT DATE_FORMAT(assignedexam.examDate,'%d/%m/%Y')) AS examDate, assignedexam.examDate AS examDates, (TIME_FORMAT(assignedexam.createdDate, '%h:%i %p')) AS `time`, typesclass.id AS classids, assignedexam.createdDate, assignedexam.createdBy, assignedexam.groupTimeStamp
FROM assignedexam
JOIN exam ON exam.id=assignedexam.examId
JOIN student ON student.id = assignedexam.studentId
JOIN typesclass ON typesclass.id= student.standard
WHERE assignedexam.isDeleted=0) AS tab2
GROUP BY tab2.groupTimeStamp) AS tab WHERE  (CASE WHEN(role = 'ADMIN') THEN 1=1 ELSE
tab.createdBy = username  END) ORDER BY tab.createdDate desc;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.exam_getStudentAttendedExamCountFilter
DROP PROCEDURE IF EXISTS `exam_getStudentAttendedExamCountFilter`;
DELIMITER //
CREATE PROCEDURE `exam_getStudentAttendedExamCountFilter`(
	IN `classId` VARCHAR(10),
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50)
)
BEGIN

SELECT qry.id,qry.`name`,qry.image,SUM(qry.`count`) AS `count` FROM (
SELECT student.id,student.`name`,student.image,COUNT(student.id) AS `count`, 0 AS type
FROM (SELECT completedexam.id,completedexam.assignedExamid, completedexam.sid
FROM completedexam JOIN assignedexam ON assignedexam.id = completedexam.assignedExamid
AND assignedexam.isDeleted = 0 AND COALESCE(completedexam.examtype,0)=0
GROUP BY sid,completedexam.assignedExamid) AS tab
JOIN assignedexam ON tab.assignedExamId = assignedexam.id AND assignedexam.isDeleted = 0
JOIN student ON student.id=tab.sid AND student.isDeleted = 0
WHERE student.standard= classId AND assignedexam.examDate BETWEEN fromDate AND toDate 
GROUP BY student.id
UNION
SELECT student.id,student.`name`,student.image,COUNT(student.id) AS `count`, 1 AS type
FROM (SELECT completedexam.id,completedexam.assignedExamid, completedexam.sid
FROM completedexam JOIN assignednewexam 
ON assignednewexam.id = completedexam.assignedExamid AND completedexam.examtype=1 
AND assignednewexam.isDeleted = 0
GROUP BY sid,completedexam.assignedExamid) AS tab
JOIN assignednewexam ON tab.assignedExamId = assignednewexam.id AND assignednewexam.isDeleted = 0
JOIN student ON student.id=tab.sid AND student.isDeleted = 0
WHERE student.standard= classId AND assignednewexam.examDate BETWEEN fromDate AND toDate 
GROUP BY student.id
) AS qry GROUP BY qry.id ;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.exam_isExamPublished
DROP PROCEDURE IF EXISTS `exam_isExamPublished`;
DELIMITER //
CREATE PROCEDURE `exam_isExamPublished`(
	IN `assignedExamId` INT,
	IN `classId` INT,
	IN `examType` INT
)
BEGIN

select * from exampublishstatus 
where  exampublishstatus.assignedId=assignedExamId and exampublishstatus.classId=classId AND exampublishstatus.examType = examType;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.fetchExamFileStudents
DROP PROCEDURE IF EXISTS `fetchExamFileStudents`;
DELIMITER //
CREATE PROCEDURE `fetchExamFileStudents`(
	IN `assignedExamId` INT,
	IN `classid` INT
)
BEGIN






SELECT s.id,s.name, s.userName, COALESCE(stu.sid,0) AS isAttended , COALESCE(mark.mark,' ') AS mark, 
(case when (stu.createdDate IS NOT NULL) THEN DATE_FORMAT(stu.createdDate,'%d/%m/%Y') ELSE '' END) AS submittedDate
, DATE_FORMAT(assignedexamfile.examDate,'%d/%m/%Y') AS submissionDate, COALESCE(mark.grade,' ') AS grade  FROM student s  
LEFT JOIN (SELECT e.sid , e.createdDate FROM examfileanswer e 
WHERE e.classid = classid AND  e.assignedExamId = assignedExamId GROUP BY e.sid) AS stu 
ON stu.sid = s.id LEFT JOIN examfilemark mark ON mark.sid = s.id AND mark.assignedExamId = assignedExamId
JOIN assignedexamfile ON assignedexamfile.id = assignedExamId
WHERE s.isDeleted = 0 AND s.standard = classid  ORDER BY COALESCE(stu.sid,0)  DESC;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getAssignedExamFileDetails
DROP PROCEDURE IF EXISTS `getAssignedExamFileDetails`;
DELIMITER //
CREATE PROCEDURE `getAssignedExamFileDetails`(
	IN `username` VARCHAR(150),
	IN `role` VARCHAR(50)
)
BEGIN

if role = 'ADMIN' OR role ='SADMIN' THEN

SELECT * FROM ( SELECT  assignedexamfile.*,examfile.examName,  typesclass.id as classids , typesclass.className 
from assignedexamfile join examfile on examfile.id=assignedexamfile.examId AND examfile.isDeleted = 0
join typesclass  ON  typesclass.id=assignedexamfile.classId 
WHERE assignedexamfile.isDeleted=0  AND assignedexamfile.groupTimeStamp  
UNION 
SELECT assignedexamfile.*,examfile.examName,  typesclass.id as classIds,typesclass.className 
from assignedexamfile JOIN  examfile on examfile.id=assignedexamfile.examId  AND examfile.isDeleted =0
join  student ON student.id = assignedexamfile.studentId JOIN typesclass  on typesclass.id= student.standard
WHERE assignedexamfile.isDeleted=0  AND assignedexamfile.groupTimeStamp ) AS tab GROUP BY tab.groupTimeStamp ;
		 
ELSE		 

SELECT * FROM ( SELECT  assignedexamfile.*,examfile.examName,  typesclass.id as classids , typesclass.className 
 from assignedexamfile join examfile on examfile.id=assignedexamfile.examId AND examfile.isDeleted = 0 join typesclass
  ON  typesclass.id=assignedexamfile.classId 
   WHERE assignedexamfile.isDeleted=0  AND assignedexamfile.groupTimeStamp  
	UNION 
	 SELECT assignedexamfile.*,examfile.examName,  typesclass.id as classIds,typesclass.className 
	  from assignedexamfile JOIN  examfile on examfile.id=assignedexamfile.examId AND examfile.isDeleted = 0 join  student
	   ON student.id = assignedexamfile.studentId JOIN typesclass  on typesclass.id= student.standard
		 WHERE assignedexamfile.isDeleted=0  AND assignedexamfile.groupTimeStamp ) AS tab  WHERE tab.createdBy = username  GROUP BY tab.groupTimeStamp; 

END IF;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getAssignedLesson
DROP PROCEDURE IF EXISTS `getAssignedLesson`;
DELIMITER //
CREATE PROCEDURE `getAssignedLesson`(
	IN `folderId` INT
)
BEGIN
SELECT qry.*,COALESCE(typesclass.className,student.`name`) AS classId, 
(CASE WHEN qry.studentId!=0 THEN 'Student' ELSE 'Class' END) AS classType FROM (
SELECT a.id AS aid,a.classId AS cid,a.studentId,
tf.id AS thirdfolderId,lesson.`name`,3 AS lessonType  FROM assignfolder a
INNER JOIN assignedmodules am ON am.id=a.moduleId AND am.isDeleted=0
INNER JOIN lesson ON lesson.id=am.moduleId AND lesson.isDeleted=0
LEFT JOIN fourthfolder ff ON ff.id=am.folderId AND am.folderType='fourth' AND ff.isDeleted=0
INNER JOIN thirdfolder tf ON case when am.folderType='fourth' then tf.id=ff.thirdFolderId
ELSE tf.id=am.folderId END AND tf.isDeleted=0
WHERE a.isDeleted=0 
UNION 
SELECT a.id AS aid,a.classId AS cid,a.studentId,
tf.id AS thirdfolderId,COALESCE(ff.`name`,tf.`name`)AS `name`,
4 AS lessonType FROM assignallfolders a
LEFT JOIN fourthfolder ff ON ff.id=a.folderId AND a.foldertype='fourth' AND ff.isDeleted=0
INNER JOIN thirdfolder tf ON case when a.foldertype='fourth'
then ff.thirdFolderId=tf.id  ELSE a.folderId=tf.id END AND tf.isDeleted=0  
WHERE a.isDeleted=0 )AS qry
LEFT JOIN typesclass ON typesclass.id=qry.cid AND typesclass.isDeleted=0
LEFT JOIN student ON student.id=qry.studentId AND student.isDeleted=0
WHERE qry.thirdfolderId=folderId AND COALESCE(typesclass.className,student.`name`) IS NOT NULL;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getAssignedLessonFromFolder
DROP PROCEDURE IF EXISTS `getAssignedLessonFromFolder`;
DELIMITER //
CREATE PROCEDURE `getAssignedLessonFromFolder`(
	IN `standard` INT,
	IN `studentId` INT,
	IN `currentDate` VARCHAR(50)

)
BEGIN
SELECT * FROM ((SELECT COALESCE(lesson.name,part.part) AS name ,COALESCE(lesson.module,part.module) AS moduleId,
'-' AS moduleName,'-' AS `level`, module.moduleName AS module,
COALESCE(lesson.partId,part.id) AS partId,assignlesson.lessonId AS id,p.part,
COALESCE(assignlesson.`type`,'lesson') AS `type`,assignlesson.isTextContent,
'0' AS lessonType,assignlesson.createdDate FROM assignlesson 
LEFT JOIN lesson ON assignlesson.lessonId=lesson.id AND lesson.isDeleted=0
LEFT JOIN part ON assignlesson.lessonId=part.id AND part.isDeleted=0
LEFT JOIN part AS p ON COALESCE(lesson.partId,part.id)=p.id AND p.isDeleted=0
LEFT JOIN  module ON COALESCE(lesson.module,part.module)=module.id
WHERE assignlesson.isDeleted=0 AND (CASE WHEN (COALESCE(assignlesson.classId,0)!=0) 
THEN assignlesson.classId=standard ELSE assignlesson.studentId=studentId END) 
AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(assignlesson.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(assignlesson.endDate,assignlesson.assigningDate),'%d/%m/%Y')
GROUP BY assignlesson.lessonId)
 UNION 
(SELECT DISTINCT COALESCE(lesson.`name`,part.part) AS `name`, COALESCE(part.module,lesson.module) AS moduleId,
folder.`name` AS moduleName,secondfolder.`name` AS `level`,thirdfolder.`name` AS module,
COALESCE(part.id,lesson.partId) AS partId,assignlessonfromfolder.moduleId AS id,part.part,
assignlessonfromfolder.moduleType as `type`,assignlessonfromfolder.isTextContent,'0' AS lessonType,assignlessonfromfolder.createdDate 
FROM assignlessonfromfolder
LEFT JOIN lesson ON lesson.id=assignlessonfromfolder.moduleId AND assignlessonfromfolder.moduleType='lesson' AND lesson.isDeleted=0 
LEFT JOIN part ON (CASE WHEN assignlessonfromfolder.moduleType='part' THEN part.id=assignlessonfromfolder.moduleId 
	 ELSE lesson.partId=part.id END ) AND part.isDeleted=0 
LEFT JOIN sixthfolder ON assignlessonfromfolder.folderId=sixthfolder.id AND assignlessonfromfolder.folderType=6 
AND sixthfolder.isDeleted=0 
LEFT JOIN fifthfolder ON (CASE WHEN (assignlessonfromfolder.folderType=5)   
	THEN assignlessonfromfolder.folderId=fifthfolder.id ELSE sixthfolder.fifthFolderId = fifthfolder.id END) AND fifthfolder.isDeleted=0 
LEFT JOIN fourthfolder ON (CASE WHEN (assignlessonfromfolder.folderType=4)   
	THEN assignlessonfromfolder.folderId=fourthfolder.id ELSE fifthfolder.fourthFolderId = fourthfolder.id END) 
	AND fourthfolder.isDeleted=0 
LEFT JOIN thirdfolder ON (CASE WHEN (assignlessonfromfolder.folderType=3)   
	THEN assignlessonfromfolder.folderId=thirdfolder.id ELSE fourthfolder.thirdFolderId = thirdfolder.id END) 
	AND thirdfolder.isDeleted=0 
LEFT JOIN secondfolder ON (CASE WHEN (assignlessonfromfolder.folderType=2)   
	THEN assignlessonfromfolder.folderId=secondfolder.id ELSE thirdfolder.secondFolderId = secondfolder.id END) 
	AND secondfolder.isDeleted=0 	  
LEFT JOIN folder ON (CASE WHEN (assignlessonfromfolder.folderType=1) 
	THEN assignlessonfromfolder.folderId=folder.id ELSE secondfolder.folderId = folder.id END) AND folder.isDeleted=0
WHERE assignlessonfromfolder.isDeleted=0 AND (CASE WHEN (COALESCE(assignlessonfromfolder.classId,0)!=0) 
THEN assignlessonfromfolder.classId=standard ELSE assignlessonfromfolder.studentId=studentId END) 
AND  STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(assignlessonfromfolder.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(assignlessonfromfolder.endDate,assignlessonfromfolder.assigningDate),'%d/%m/%Y')
AND COALESCE(folder.NAME,'')!=''  
GROUP BY assignlessonfromfolder.moduleId ORDER BY assignlessonfromfolder.id)
UNION 
(SELECT DISTINCT COALESCE(lesson.`name`,part.part) AS `name`,module.id AS moduleId,
folder.`name` AS moduleName,secondfolder.`name` AS `level`,thirdfolder.`name` AS module,
COALESCE(lesson.partId,part.id) AS partId, assignedmodules.moduleId AS id,
COALESCE(sixthfolder.`name`,fifthfolder.`name`,fourthfolder.`name`) AS part, 
assignedmodules.`type`, assignfolder.isTextContent,'0' AS lessonType,assignfolder.createdDate FROM assignfolder 
LEFT JOIN assignedmodules ON assignedmodules.id=assignfolder.moduleId 
LEFT JOIN lesson ON lesson.id=assignedmodules.moduleId AND assignedmodules.type='lesson' AND lesson.isDeleted=0 
LEFT JOIN part ON (CASE WHEN assignedmodules.type='part' THEN part.id=assignedmodules.moduleId  	
	ELSE part.id=lesson.partId END) AND part.isDeleted=0 
LEFT JOIN module ON (CASE WHEN assignedmodules.type='module' THEN module.id=assignedmodules.moduleId 
	ELSE module.id=part.module END) AND module.isDeleted=0    
LEFT JOIN sixthfolder ON assignedmodules.folderId=sixthfolder.id AND assignedmodules.folderType='sixth' AND sixthfolder.isDeleted=0  
LEFT JOIN fifthfolder ON (CASE WHEN (assignedmodules.folderType='fifth')   
	THEN assignedmodules.folderId=fifthfolder.id ELSE sixthfolder.fifthFolderId = fifthfolder.id END) AND fifthfolder.isDeleted=0 
LEFT JOIN fourthfolder ON (CASE WHEN (assignedmodules.folderType='fourth')   
	THEN assignedmodules.folderId=fourthfolder.id ELSE fifthfolder.fourthFolderId = fourthfolder.id END) AND fourthfolder.isDeleted=0 
LEFT JOIN thirdfolder ON (CASE WHEN (assignedmodules.folderType='third')   
	THEN assignedmodules.folderId=thirdfolder.id ELSE fourthfolder.thirdFolderId = thirdfolder.id END) AND thirdfolder.isDeleted=0	  
LEFT JOIN secondfolder ON (CASE WHEN (assignedmodules.folderType='second')   
	THEN assignedmodules.folderId=secondfolder.id ELSE thirdfolder.secondFolderId = secondfolder.id END) AND secondfolder.isDeleted=0  
LEFT JOIN folder ON (CASE WHEN (assignedmodules.folderType='first')   
	THEN assignedmodules.folderId=folder.id ELSE secondfolder.folderId = folder.id END) AND folder.isDeleted=0	  
WHERE assignfolder.isDeleted=0 AND (CASE WHEN (COALESCE(assignfolder.classId,0)!=0) THEN assignfolder.classId=standard 
ELSE assignfolder.studentId=studentId END) 
AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(assignfolder.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(assignfolder.endDate,assignfolder.assigningDate),'%d/%m/%Y')
AND COALESCE(folder.NAME,'')!=''  GROUP BY lesson.id)
UNION 
(SELECT DISTINCT COALESCE(lesson.`name`,part.part) AS `name`,module.id AS moduleId,
folder.`name` AS moduleName,secondfolder.`name` AS `level`,thirdfolder.`name` AS module,
COALESCE(lesson.partId,part.id) AS partId, assignedmodules.moduleId AS id,
COALESCE(sixthfolder.`name`,fifthfolder.`name`,fourthfolder.`name`) AS part, 
assignedmodules.`type`, assignfolder.isTextContent,'0' AS lessonType,assignfolder.createdDate FROM assignfolder 
LEFT JOIN assignedmodules ON assignedmodules.id=assignfolder.moduleId 
LEFT JOIN lesson ON lesson.id=assignedmodules.moduleId AND assignedmodules.type='lesson' AND lesson.isDeleted=0 
LEFT JOIN part ON (CASE WHEN assignedmodules.type='part' THEN part.id=assignedmodules.moduleId  	
	ELSE part.id=lesson.partId END) AND part.isDeleted=0 
LEFT JOIN module ON (CASE WHEN assignedmodules.type='module' THEN module.id=assignedmodules.moduleId 
	ELSE module.id=part.module END) AND module.isDeleted=0    
LEFT JOIN sixthfolder ON assignedmodules.folderId=sixthfolder.id AND assignedmodules.folderType='sixth' AND sixthfolder.isDeleted=0  
LEFT JOIN fifthfolder ON (CASE WHEN (assignedmodules.folderType='fifth')   
	THEN assignedmodules.folderId=fifthfolder.id ELSE sixthfolder.fifthFolderId = fifthfolder.id END) AND fifthfolder.isDeleted=0 
LEFT JOIN fourthfolder ON (CASE WHEN (assignedmodules.folderType='fourth')   
	THEN assignedmodules.folderId=fourthfolder.id ELSE fifthfolder.fourthFolderId = fourthfolder.id END) AND fourthfolder.isDeleted=0 
LEFT JOIN thirdfolder ON (CASE WHEN (assignedmodules.folderType='third')   
	THEN assignedmodules.folderId=thirdfolder.id ELSE fourthfolder.thirdFolderId = thirdfolder.id END) AND thirdfolder.isDeleted=0	  
LEFT JOIN secondfolder ON (CASE WHEN (assignedmodules.folderType='second')   
	THEN assignedmodules.folderId=secondfolder.id ELSE thirdfolder.secondFolderId = secondfolder.id END) AND secondfolder.isDeleted=0  
LEFT JOIN folder ON (CASE WHEN (assignedmodules.folderType='first')   
	THEN assignedmodules.folderId=folder.id ELSE secondfolder.folderId = folder.id END) AND folder.isDeleted=0	  
WHERE assignfolder.isDeleted=0 AND (CASE WHEN (COALESCE(assignfolder.classId,0)!=0) THEN assignfolder.classId=standard 
ELSE assignfolder.studentId=studentId END) 
AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(assignfolder.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(assignfolder.endDate,assignfolder.assigningDate),'%d/%m/%Y')
AND COALESCE(folder.NAME,'')!=''  GROUP BY lesson.id)
UNION
(SELECT COALESCE(sixthfolder.`name`,fifthfolder.`name`,fourthfolder.`name`) AS `name`, 0 AS moduleId,folder.`name` AS moduleName,
secondfolder.`name` AS `level`,thirdfolder.`name` AS module, 0 AS partId,a.folderId AS id,secondfolder.`name` AS part,
'elt-studio' AS `type`,a.isTextContent,a.foldertype AS lessonType,a.createdDate
FROM assignallfolders AS a
LEFT JOIN sixthfolder ON a.folderId=sixthfolder.id AND a.folderType='sixth' AND sixthfolder.isDeleted=0 
LEFT JOIN fifthfolder ON (CASE WHEN (a.folderType='fifth')   
	THEN a.folderId=fifthfolder.id ELSE sixthfolder.fifthFolderId = fifthfolder.id END) AND fifthfolder.isDeleted=0 
LEFT JOIN fourthfolder ON (CASE WHEN (a.folderType='fourth')   
	THEN a.folderId=fourthfolder.id ELSE fifthfolder.fourthFolderId = fourthfolder.id END) AND fourthfolder.isDeleted=0 
LEFT JOIN thirdfolder ON (CASE WHEN (a.folderType='third')   
	THEN a.folderId=thirdfolder.id ELSE fourthfolder.thirdFolderId = thirdfolder.id END) AND thirdfolder.isDeleted=0 
LEFT JOIN secondfolder ON (CASE WHEN (a.folderType='second')   
	THEN a.folderId=secondfolder.id ELSE thirdfolder.secondFolderId = secondfolder.id END) AND secondfolder.isDeleted=0 	  
LEFT JOIN folder ON (CASE WHEN (a.folderType='first') 
	THEN a.folderId=folder.id ELSE secondfolder.folderId = folder.id END) AND folder.isDeleted=0
WHERE a.isDeleted=0 AND (CASE WHEN COALESCE(a.classId,0)!=0 THEN a.classId=standard 
ELSE a.studentId=studentId END) AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y')))  AS qry ORDER BY qry.createdDate;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getAssignedLessonsTeacher
DROP PROCEDURE IF EXISTS `getAssignedLessonsTeacher`;
DELIMITER //
CREATE PROCEDURE `getAssignedLessonsTeacher`(
	IN `currentDate` VARCHAR(50)




)
BEGIN
(SELECT a.id,COALESCE(lesson.`name`,part.part) AS `lesson`, COALESCE(a.`type`,'lesson') AS `type`,a.isTextContent, '1' AS lessonType,a.assigningDate,a.endDate,
COALESCE(typesclass.className,student.`name`) AS classId, (CASE WHEN a.studentId!=0 THEN 'Student' ELSE 'Class' END) AS classType  FROM assignlesson AS a
LEFT JOIN lesson ON a.lessonId=lesson.id AND  COALESCE(a.`type`,'lesson')='lesson' AND lesson.isDeleted=0
LEFT JOIN part ON a.lessonId=part.id AND a.`type`='part' AND part.isDeleted=0 
LEFT JOIN typesclass ON a.classId=typesclass.id
LEFT JOIN student ON a.studentId=student.id 
WHERE a.isDeleted=0  AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'))
UNION
(SELECT DISTINCT a.id, COALESCE(lesson.`name`,part.part) AS `lesson`, 
a.moduleType as `type`,a.isTextContent,'2' AS lessonType,a.assigningDate,a.endDate,
COALESCE(typesclass.className,student.`name`) AS classId, (CASE WHEN a.studentId!=0 THEN 'Student' ELSE 'Class' END) AS classType  
FROM assignlessonfromfolder AS a
LEFT JOIN lesson ON lesson.id=a.moduleId AND a.moduleType='lesson' AND lesson.isDeleted=0 
LEFT JOIN part ON part.id=a.moduleId AND a.moduleType='part' AND part.isDeleted=0
LEFT JOIN typesclass ON a.classId=typesclass.id
LEFT JOIN student ON a.studentId=student.id  
WHERE a.isDeleted=0 AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'))
union 
(SELECT DISTINCT a.id,COALESCE(lesson.`name`,part.part) AS `lesson`, assignedmodules.`type`, a.isTextContent, '3' AS lessonType,a.assigningDate,a.endDate,
COALESCE(typesclass.className,student.`name`) AS classId, (CASE WHEN a.studentId!=0 THEN 'Student' ELSE 'Class' END) AS classType   FROM assignfolder AS a
LEFT JOIN assignedmodules ON assignedmodules.id=a.moduleId 
LEFT JOIN lesson ON lesson.id=assignedmodules.moduleId AND assignedmodules.type='lesson' AND lesson.isDeleted=0 
LEFT JOIN part ON part.id=assignedmodules.moduleId AND assignedmodules.type='part' AND part.isDeleted=0 
LEFT JOIN typesclass ON a.classId=typesclass.id
LEFT JOIN student ON a.studentId=student.id 
WHERE a.isDeleted=0 AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'))
UNION
(SELECT a.id,COALESCE(sixthfolder.`name`,fifthfolder.`name`,fourthfolder.`name`,thirdfolder.`name`,secondfolder.`name`) AS `lesson`,
'elt-studio' AS `type`,a.isTextContent,4 AS lessonType,a.assigningDate,a.endDate,
COALESCE(typesclass.className,student.`name`) AS classId, (CASE WHEN a.studentId!=0 THEN 'Student' ELSE 'Class' END) AS classType  
FROM assignallfolders AS a
LEFT JOIN sixthfolder ON a.folderId=sixthfolder.id AND a.folderType='sixth' AND sixthfolder.isDeleted=0 
LEFT JOIN fifthfolder ON a.folderId=fifthfolder.id AND a.folderType='fifth' AND fifthfolder.isDeleted=0 
LEFT JOIN fourthfolder ON a.folderId=fourthfolder.id AND a.folderType='fourth' AND fourthfolder.isDeleted=0 
LEFT JOIN thirdfolder ON a.folderId=thirdfolder.id AND a.folderType='third' AND thirdfolder.isDeleted=0 
LEFT JOIN secondfolder ON a.folderId=secondfolder.id AND a.folderType='second' AND secondfolder.isDeleted=0 	  
LEFT JOIN folder ON a.folderId=folder.id AND a.folderType='first' AND folder.isDeleted=0
LEFT JOIN typesclass ON a.classId=typesclass.id
LEFT JOIN student ON a.studentId=student.id 
WHERE a.isDeleted=0 AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'));
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getAssignedModulesFromAllFolders
DROP PROCEDURE IF EXISTS `getAssignedModulesFromAllFolders`;
DELIMITER //
CREATE PROCEDURE `getAssignedModulesFromAllFolders`(
	IN `standard` INT,
	IN `studentId` INT,
	IN `currentDate` VARCHAR(50)
)
BEGIN
SELECT qry1.* FROM (
SELECT DISTINCT qry.lesson,qry.module,qry.partId,qry.`name`,qry.classId,qry.studentId,qry.assigningDate,qry.endDate,
COALESCE(qry.f4name,'') AS folder4,COALESCE(qry.f3name,f31.`name`) AS folder3,
f21.`name` AS folder2,f11.`name` AS folder1,qry.createdDate FROM(
SELECT af.createdDate,af.classId,af.studentId,af.assigningDate,af.endDate,af.isTextContent,
af.foldertype,af.folderId,lesson.`name`,lesson.id AS lesson,
lesson.module,lesson.partId, f3.secondFolderId AS f2id, f3.`name` AS f3name,
COALESCE(f3.id,f4.thirdFolderId)  AS f3id, f4.`name` AS f4name, f4.id AS f4id
FROM assignallfolders af
LEFT JOIN thirdfolder f3 ON f3.id=af.folderId AND af.foldertype='third' AND f3.isDeleted=0
LEFT JOIN fourthfolder f4 ON CASE WHEN af.foldertype='fourth' THEN f4.id=af.folderId
	ELSE f4.thirdFolderId=f3.id END AND f4.isDeleted=0
INNER JOIN assignedmodules am ON CASE WHEN f4.id!="" THEN am.folderId=f4.id AND am.folderType='fourth' 
	ELSE am.folderId=f3.id AND am.folderType='third' END AND am.isDeleted=0
INNER JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson' AND lesson.isDeleted=0
WHERE af.isDeleted=0) AS qry 
LEFT JOIN thirdfolder f31 ON qry.f3id=f31.id AND qry.folderType='fourth' AND f31.isDeleted=0
INNER JOIN secondfolder f21 ON CASE WHEN qry.folderType='third' THEN qry.f2id=f21.id 
	ELSE f21.id=f31.secondFolderId END AND f21.isDeleted=0
INNER JOIN folder f11 ON f11.id=f21.folderId AND f11.isDeleted=0

UNION

SELECT lesson.id AS lesson,lesson.module,lesson.partId,lesson.`name`,af.classId,af.studentId,af.assigningDate,af.endDate,
COALESCE(f4.`name`,'')  AS folder4,f3.`name` AS folder3,f2.`name` AS folder2,f1.`name` AS folder1,
af.createdDate FROM assignfolder af
INNER JOIN assignedmodules am ON am.id=af.moduleId AND am.isDeleted=0
INNER JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson' AND lesson.isDeleted=0
LEFT JOIN fourthfolder f4 ON f4.id=am.folderId AND am.folderType='fourth' AND f4.isDeleted=0
INNER JOIN thirdfolder f3 ON CASE WHEN am.folderType='third' THEN f3.id=am.folderId 
	ELSE f3.id=f4.thirdFolderId END  AND f3.isDeleted=0
INNER JOIN secondfolder f2 ON f2.id=f3.secondFolderId AND f2.isDeleted=0
INNER JOIN folder f1 ON f1.id=f2.folderId AND f1.isDeleted=0 
WHERE af.isDeleted=0 ) AS qry1
WHERE (CASE WHEN (COALESCE(qry1.classId,0)!=0) THEN qry1.classId=standard ELSE qry1.studentId=studentId END)
AND STR_TO_DATE(currentDate,'%d/%m/%Y') 
BETWEEN STR_TO_DATE(qry1.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(qry1.endDate,qry1.assigningDate),'%d/%m/%Y')
GROUP BY qry1.lesson ORDER BY qry1.createdDate;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getAssignedModulesFromAllFolders1
DROP PROCEDURE IF EXISTS `getAssignedModulesFromAllFolders1`;
DELIMITER //
CREATE PROCEDURE `getAssignedModulesFromAllFolders1`(
	IN `standard` INT,
	IN `studentId` INT,
	IN `currentDate` VARCHAR(50)
)
BEGIN
SELECT DISTINCT lesson.`name`,qry.*,lesson.id AS lesson,lesson.module,lesson.partId  
FROM (SELECT COALESCE(a2.folder1,a3.folder1,a4.folder1,a5.folder1,a6.folder1) AS folder1,  
COALESCE(a2.folder2,a3.folder2,a4.folder2,a5.folder2,a6.folder2) AS folder2,  
COALESCE(a3.folder3,a4.folder3,a5.folder3,a6.folder3) AS folder3,  
COALESCE(a4.folder4,a5.folder4,a6.folder4) AS folder4,COALESCE(a5.folder5,a6.folder5) AS folder5,  
COALESCE(a6.folder6) AS folder6,alls.folderId,alls.folderType,  
COALESCE(a6.id6,a5.id5,a4.id4,a3.id3,a2.id2,a2.id1) AS fid,  
COALESCE(a2.p1,a3.p1,a4.p1,a5.p1,a6.p1) AS p1,  
COALESCE(a2.p2,a3.p2,a4.p2,a5.p2,a6.p2) AS p2,  
COALESCE(a3.p3,a4.p3,a5.p3,a6.p3) AS p3,  
COALESCE(a4.p4,a5.p4,a6.p4) AS p4,  
COALESCE(a5.p5,a6.p5) AS p5,  
COALESCE(a6.p6) AS p6,alls.isTextContent,
COALESCE(a6.`type`,a5.`type`,a5.`type`,a4.`type`,a3.`type`,a2.`type`) AS ftype
FROM assignallfolders AS alls  
LEFT JOIN (SELECT folder.priority AS p1 ,secondfolder.priority AS p2,'second' AS `type`, secondfolder.id AS id2,folder.id as id1,folder.NAME AS folder1,secondfolder.NAME AS folder2 FROM secondfolder   
 INNER JOIN folder ON folder.id=secondfolder.folderId AND folder.isDeleted=0 
WHERE secondfolder.isDeleted=0 )AS a2 ON (case when(alls.foldertype='second') then alls.folderId=a2.id2 END)  
LEFT JOIN (SELECT folder.priority AS p1 ,secondfolder.priority AS p2,thirdfolder.priority AS p3,'third' AS `type`,thirdfolder.id AS id3,secondfolder.id AS id2,folder.NAME AS folder1,secondfolder.NAME AS folder2,thirdfolder.NAME AS folder3  
 FROM thirdfolder INNER JOIN secondfolder ON secondfolder.id=thirdfolder.secondFolderId  AND secondfolder.isDeleted=0 
 INNER JOIN folder ON folder.id=secondfolder.folderId AND folder.isDeleted=0  
WHERE thirdfolder.isDeleted=0 ) AS a3 ON (case when(alls.foldertype='third') then alls.folderId=a3.id3 ELSE a3.id2=a2.id2 END)	  
LEFT JOIN (SELECT folder.priority AS p1 ,secondfolder.priority AS p2,thirdfolder.priority AS p3,fourthfolder.priority AS p4,'fourth' AS `type`,fourthfolder.id AS id4,thirdfolder.id AS id3,folder.NAME AS folder1,secondfolder.NAME AS folder2,  
 thirdfolder.NAME AS folder3,fourthfolder.NAME AS folder4 FROM fourthfolder  
 INNER JOIN thirdfolder ON thirdfolder.id=fourthfolder.thirdFolderId AND thirdfolder.isDeleted=0 
 INNER JOIN secondfolder ON secondfolder.id=thirdfolder.secondFolderId  AND secondfolder.isDeleted=0 
 INNER JOIN folder ON folder.id=secondfolder.folderId AND folder.isDeleted=0  
WHERE fourthfolder.isDeleted=0) AS a4 ON (case when(alls.foldertype='fourth') then alls.folderId=a4.id4	ELSE a4.id3= a3.id3 END)	  
LEFT JOIN (SELECT folder.priority AS p1 ,secondfolder.priority AS p2,thirdfolder.priority AS p3,fourthfolder.priority AS p4,fifthfolder.priority AS p5,  
 'fifth' AS `type`,fifthfolder.id AS id5,fourthfolder.id AS id4,folder.NAME AS folder1,secondfolder.NAME AS folder2,  
 thirdfolder.NAME AS folder3,fourthfolder.NAME AS folder4,fifthfolder.name AS folder5 FROM fifthfolder   
 INNER JOIN fourthfolder ON fourthfolder.id=fifthfolder.fourthFolderId  AND fourthfolder.isDeleted=0 
 INNER JOIN thirdfolder ON thirdfolder.id=fourthfolder.thirdFolderId AND thirdfolder.isDeleted=0 
 INNER JOIN secondfolder ON secondfolder.id=thirdfolder.secondFolderId  AND secondfolder.isDeleted=0 
 INNER JOIN folder ON folder.id=secondfolder.folderId AND folder.isDeleted=0  
WHERE fifthfolder.isDeleted=0 ) AS a5 ON (case when(alls.foldertype='fifth') then alls.folderId=a5.id5 ELSE a5.id4=a4.id4 END)	  
LEFT JOIN (SELECT folder.priority AS p1 ,secondfolder.priority AS p2,thirdfolder.priority AS p3,fourthfolder.priority AS p4,fifthfolder.priority AS p5,sixthfolder.priority AS p6,  
 'sixth' AS `type`,sixthfolder.id AS id6,fifthfolder.id AS id5,folder.NAME AS folder1,secondfolder.NAME AS folder2,  
 thirdfolder.NAME AS folder3,fourthfolder.NAME AS folder4,fifthfolder.name AS folder5,  
 sixthfolder.name AS folder6 FROM sixthfolder   
 INNER JOIN fifthfolder ON fifthfolder.id=sixthfolder.fifthFolderId AND fifthfolder.isDeleted=0 
 INNER JOIN fourthfolder ON fourthfolder.id=fifthfolder.fourthFolderId AND fourthfolder.isDeleted=0 
 INNER JOIN thirdfolder ON thirdfolder.id=fourthfolder.thirdFolderId AND thirdfolder.isDeleted=0 
 INNER JOIN secondfolder ON secondfolder.id=thirdfolder.secondFolderId AND secondfolder.isDeleted=0 
 INNER JOIN folder ON folder.id=secondfolder.folderId AND folder.isDeleted=0  
WHERE sixthfolder.isDeleted=0)AS a6 ON (case when(alls.foldertype='sixth') then alls.folderId=a6.id6  ELSE a6.id5=a5.id5 END) 	 
WHERE (CASE WHEN (COALESCE(alls.classId,0)!=0) THEN alls.classId=standard  
ELSE alls.studentId=studentId END) AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(alls.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(alls.endDate,alls.assigningDate),'%d/%m/%Y') ORDER BY alls.id) AS qry  
INNER JOIN assignedmodules ON assignedmodules.folderId=qry.fid AND assignedmodules.folderType=qry.ftype  
INNER JOIN lesson ON lesson.id=assignedmodules.moduleId AND assignedmodules.`type`='lesson'   
GROUP BY lesson.`name` ORDER BY qry.p1,qry.p2,qry.p3,qry.p4,qry.p5,qry.p6;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getAssignedModulesFromAllFolders2
DROP PROCEDURE IF EXISTS `getAssignedModulesFromAllFolders2`;
DELIMITER //
CREATE PROCEDURE `getAssignedModulesFromAllFolders2`(
	IN `standard` INT,
	IN `studentId` INT,
	IN `currentDate` VARCHAR(50)
)
BEGIN
SELECT qry1.* FROM (
SELECT DISTINCT qry.id,qry.lesson,qry.module,qry.partId,qry.`name`,qry.classId,qry.studentId,qry.assigningDate,qry.endDate,
COALESCE(qry.f4name,'') AS folder4,COALESCE(qry.f3name,f31.`name`) AS folder3,
f21.`name` AS folder2,f11.`name` AS folder1,qry.createdDate FROM(
SELECT af.id,af.createdDate,af.classId,af.studentId,af.assigningDate,af.endDate,af.isTextContent,
af.foldertype,af.folderId,lesson.`name`,lesson.id AS lesson,
lesson.module,lesson.partId, f3.secondFolderId AS f2id, f3.`name` AS f3name,
COALESCE(f3.id,f4.thirdFolderId)  AS f3id, f4.`name` AS f4name, f4.id AS f4id
FROM assignallfolders af
LEFT JOIN thirdfolder f3 ON f3.id=af.folderId AND af.foldertype='third' AND f3.isDeleted=0
LEFT JOIN fourthfolder f4 ON CASE WHEN af.foldertype='fourth' THEN f4.id=af.folderId
	ELSE f4.thirdFolderId=f3.id END AND f4.isDeleted=0
INNER JOIN assignedmodules am ON CASE WHEN f4.id!="" THEN am.folderId=f4.id AND am.folderType='fourth' 
	ELSE am.folderId=f3.id AND am.folderType='third' END AND am.isDeleted=0
INNER JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson' AND lesson.isDeleted=0
WHERE af.isDeleted=0) AS qry 
LEFT JOIN thirdfolder f31 ON qry.f3id=f31.id AND qry.folderType='fourth' AND f31.isDeleted=0
INNER JOIN secondfolder f21 ON CASE WHEN qry.folderType='third' THEN qry.f2id=f21.id 
	ELSE f21.id=f31.secondFolderId END AND f21.isDeleted=0
INNER JOIN folder f11 ON f11.id=f21.folderId AND f11.isDeleted=0

UNION

SELECT af.id,lesson.id AS lesson,lesson.module,lesson.partId,lesson.`name`,af.classId,af.studentId,af.assigningDate,af.endDate,
COALESCE(f4.`name`,'')  AS folder4,f3.`name` AS folder3,f2.`name` AS folder2,f1.`name` AS folder1,
af.createdDate FROM assignfolder af
INNER JOIN assignedmodules am ON am.id=af.moduleId AND am.isDeleted=0
INNER JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson' AND lesson.isDeleted=0
LEFT JOIN fourthfolder f4 ON f4.id=am.folderId AND am.folderType='fourth' AND f4.isDeleted=0
INNER JOIN thirdfolder f3 ON CASE WHEN am.folderType='third' THEN f3.id=am.folderId 
	ELSE f3.id=f4.thirdFolderId END  AND f3.isDeleted=0
INNER JOIN secondfolder f2 ON f2.id=f3.secondFolderId AND f2.isDeleted=0
INNER JOIN folder f1 ON f1.id=f2.folderId AND f1.isDeleted=0 
WHERE af.isDeleted=0 ) AS qry1
WHERE (CASE WHEN (COALESCE(qry1.classId,0)!=0) THEN qry1.classId=standard ELSE qry1.studentId=studentId END) AND STR_TO_DATE(currentDate,'%d/%m/%Y') 
BETWEEN STR_TO_DATE(qry1.assigningDate,'%d/%m/%Y') AND STR_TO_DATE(COALESCE(qry1.endDate,qry1.assigningDate),'%d/%m/%Y')
GROUP BY qry1.lesson ORDER BY qry1.createdDate,qry1.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getAssignedModulesFromFolder
DROP PROCEDURE IF EXISTS `getAssignedModulesFromFolder`;
DELIMITER //
CREATE PROCEDURE `getAssignedModulesFromFolder`(
	IN `standard` INT,
	IN `studentId` INT,
	IN `currentDate` VARCHAR(50)
)
BEGIN
SELECT distinct assignfolder.id,assignedmodules.type, assignfolder.isTextContent, 
COALESCE(lesson.module,part.module,module.id) AS module,  
COALESCE(lesson.partId,part.id) as partId,  
lesson.id AS lesson, assignedmodules.moduleId,lesson.`name`,   
assignedmodules.folderType,assignedmodules.folderId,  
sixthfolder.NAME AS folder6, fifthfolder.NAME AS folder5,  
fourthfolder.NAME AS folder4, thirdfolder.NAME AS folder3,  
secondfolder.NAME AS folder2, folder.NAME AS folder1  
from assignfolder      
LEFT JOIN assignedmodules ON assignedmodules.id=assignfolder.moduleId    
LEFT JOIN module ON module.id=assignedmodules.moduleId AND assignedmodules.type='module' AND module.isDeleted=0    
LEFT JOIN part ON (CASE WHEN (assignedmodules.type='part') THEN part.id=assignedmodules.moduleId  
		WHEN (assignedmodules.`type`='module')  THEN part.module=module.id END) AND part.isDeleted=0 
LEFT JOIN lesson ON (CASE WHEN (assignedmodules.type='lesson') THEN  lesson.id=assignedmodules.moduleId   
		WHEN (assignedmodules.`type`='module' OR assignedmodules.`type`='part')  THEN lesson.partId=part.id END) AND lesson.isDeleted=0 
LEFT JOIN sixthfolder ON assignedmodules.folderId=sixthfolder.id AND assignedmodules.folderType='sixth' AND sixthfolder.isDeleted=0  
LEFT JOIN fifthfolder ON (CASE WHEN (assignedmodules.folderType='fifth')   
	THEN assignedmodules.folderId=fifthfolder.id ELSE sixthfolder.fifthFolderId = fifthfolder.id END) AND fifthfolder.isDeleted=0 
LEFT JOIN fourthfolder ON (CASE WHEN (assignedmodules.folderType='fourth')   
	THEN assignedmodules.folderId=fourthfolder.id ELSE fifthfolder.fourthFolderId = fourthfolder.id END) AND fourthfolder.isDeleted=0 
LEFT JOIN thirdfolder ON (CASE WHEN (assignedmodules.folderType='third')   
	THEN assignedmodules.folderId=thirdfolder.id ELSE fourthfolder.thirdFolderId = thirdfolder.id END) AND thirdfolder.isDeleted=0	  
LEFT JOIN secondfolder ON (CASE WHEN (assignedmodules.folderType='second')   
	THEN assignedmodules.folderId=secondfolder.id ELSE thirdfolder.secondFolderId = secondfolder.id END) AND secondfolder.isDeleted=0  
LEFT JOIN folder ON (CASE WHEN (assignedmodules.folderType='first')   
	THEN assignedmodules.folderId=folder.id ELSE secondfolder.folderId = folder.id END) AND folder.isDeleted=0	  
WHERE (CASE WHEN (COALESCE(assignfolder.classId,0)!=0) THEN assignfolder.classId=standard   
ELSE assignfolder.studentId=studentId END)  AND assignfolder.assigningDate=currentDate  AND COALESCE(folder.NAME,'')!=''  GROUP BY lesson.id; 

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getDetailsForEditLesson
DROP PROCEDURE IF EXISTS `getDetailsForEditLesson`;
DELIMITER //
CREATE PROCEDURE `getDetailsForEditLesson`(
	IN `lessonid` VARCHAR(50)
)
BEGIN
select lesson.id, lesson.listenFile, lesson.module, 
lesson.`name`, lesson.partId, lesson.listenFilePdf, 
lesson.repeatFile,lesson.roleplayFile,lesson.text,
lesson.repeatFilePdf,lesson.roleplayFilePdf,lesson.listenSubtitle,
lesson.repeatSubtitle,lesson.roleplaySubtitle, 
module.moduleName, module.moduleType, part.part AS part 
FROM lesson 
INNER JOIN module ON lesson.module=module.id 
INNER JOIN part ON lesson.partId=part.id 
WHERE lesson.id=lessonid AND lesson.isDeleted=0 
AND module.isDeleted=0 AND part.isDeleted=0;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getExamFileListForReport
DROP PROCEDURE IF EXISTS `getExamFileListForReport`;
DELIMITER //
CREATE PROCEDURE `getExamFileListForReport`(
	IN `classid` VARCHAR(10),
	IN `startDate` VARCHAR(10),
	IN `endDate` VARCHAR(10)
)
BEGIN

IF startDate != '' AND endDate!='' THEN

SELECT assignedexamfile.id,examfile.examName, 
DATE_FORMAT(assignedexamfile.examDate,'%d/%m/%Y') AS examDate ,
TIME_FORMAT(assignedexamfile.createdDate,'%h:%i %p') AS startTime , examfile.totalMark FROM examfile 
JOIN assignedexamfile ON examfile.id = assignedexamfile.examId AND examfile.isDeleted =0
WHERE DATE_FORMAT(assignedexamfile.examDate,'%Y-%m-%d') 
 BETWEEN DATE_FORMAT(STR_TO_DATE(startDate, '%d/%m/%Y'),'%Y-%m-%d') 
 AND DATE_FORMAT(STR_TO_DATE(endDate, '%d/%m/%Y'),'%Y-%m-%d') AND assignedexamfile.isDeleted = 0 
 AND  (CASE WHEN assignedexamfile.classId = 0 THEN (assignedexamfile.studentId in
  (SELECT student.id FROM student WHERE student.isDeleted = 0 AND student.standard = classid)) ELSE assignedexamfile.classId = classid END) 
  GROUP BY assignedexamfile.id ;
  
 ELSE
 
 SELECT assignedexamfile.id,examfile.examName, 
DATE_FORMAT(assignedexamfile.examDate,'%d/%m/%Y') AS examDate ,
TIME_FORMAT(assignedexamfile.createdDate,'%h:%i %p') AS startTime , examfile.totalMark FROM examfile 
JOIN assignedexamfile ON examfile.id = assignedexamfile.examId  AND examfile.isDeleted =0
WHERE  assignedexamfile.isDeleted = 0 
 AND  (CASE WHEN assignedexamfile.classId = 0 THEN (assignedexamfile.studentId in
  (SELECT student.id FROM student WHERE student.isDeleted = 0 AND student.standard = classid)) ELSE assignedexamfile.classId = classid END) 
  GROUP BY assignedexamfile.id ;
 
 END IF; 
  
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getExamQuestions
DROP PROCEDURE IF EXISTS `getExamQuestions`;
DELIMITER //
CREATE PROCEDURE `getExamQuestions`(
	IN `StringValue` LONGTEXT
)
    COMMENT 'Get selected Exam Questions'
BEGIN

DECLARE INDX INT;
 DECLARE SLICE nvarchar(4000); 
 SET INDX = 1;
 
 DROP TEMPORARY TABLE IF EXISTS Que;
 CREATE TEMPORARY TABLE Que(Items VARCHAR(100));
 
 select exam.selectedQuestions into StringValue  from exam where exam.id=StringValue;
 
 
 MyLoop: WHILE INDX != 0 DO
 
 SET INDX = LOCATE(',',StringValue);
 
 IF INDX !=0 THEN
  SET SLICE = LEFT(StringValue,INDX - 1);
 ELSE
  SET SLICE = StringValue;
 END IF;
 
 INSERT INTO Que(Items) VALUES(SLICE);
 
 SET StringValue = RIGHT(StringValue,LENGTH(StringValue) - INDX);
 
 IF LENGTH(StringValue) = 0 THEN
  LEAVE MyLoop;
 END IF;
 END WHILE;

select exercise.id as exerciseId,exercise.question,exercise.answer,exercise.type,options.* , exercise.mark from Que join exercise on exercise.id=Que.Items left join options on options.exerciseId=exercise.id where exercise.isDeleted=0;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getFifthFolderList
DROP PROCEDURE IF EXISTS `getFifthFolderList`;
DELIMITER //
CREATE PROCEDURE `getFifthFolderList`(
	IN `id` INT,
	IN `standard` INT,
	IN `studentId` INT,
	IN `currentDate` VARCHAR(50)



)
BEGIN
(SELECT DISTINCT 0 AS lid,0 AS partId,0 AS moduleId,
COALESCE(fif5.id,f5.id) AS id,COALESCE(fif5.`name`,f5.`name`)AS `name`,'folder' AS `type`
FROM assignallfolders a  
LEFT JOIN sixthfolder f6 ON a.folderId=f6.id AND a.foldertype='sixth'
LEFT JOIN fifthfolder f5 ON (case 
		when  a.foldertype='fifth' then a.folderId=f5.id 
		when a.foldertype='sixth' then f5.id=f6.fifthFolderId END)
LEFT JOIN fourthfolder f4 ON (case 
		when a.foldertype='third' then a.folderId=f4.thirdFolderId
		when a.foldertype='fourth' then a.folderId=f4.id
		when a.foldertype='fifth' then f4.id=f5.fourthFolderId
		when a.foldertype='sixth' then f4.id=f5.fourthFolderId END) 
LEFT JOIN fifthfolder fif5 ON (case when  a.foldertype='third' then fif5.fourthFolderId=f4.id 
		when a.foldertype='fourth' then fif5.fourthFolderId=f4.id  END) 
WHERE COALESCE(fif5.id,f5.id,'')!='' AND f4.id=id AND a.isDeleted=0 AND (CASE WHEN COALESCE(a.classId,0)!=0 
THEN a.classId=standard  ELSE a.studentId=studentId END) AND STR_TO_DATE(currentDate,'%d/%m/%Y') 
BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'))
UNION 
(SELECT DISTINCT lesson.id AS lid, COALESCE(part.id,lesson.partId) AS partid,
COALESCE(module.id,part.module,lesson.module) AS moduleid,am.id,
COALESCE(module.moduleName, part.part, lesson.`name`) AS `name`,
COALESCE(am.`type`,'folder') AS `type` FROM assignallfolders a  
LEFT JOIN thirdfolder f3 ON a.foldertype='third' AND f3.id=a.folderId 
LEFT JOIN fourthfolder f4 ON (case when a.foldertype='third' then f3.id=f4.thirdFolderId
		when a.foldertype='fourth' then f4.id=a.folderId END) 
LEFT JOIN assignedmodules am ON am.folderId=f4.id AND am.folderType='fourth'
LEFT JOIN module ON module.id=am.moduleId AND am.`type`='module'
LEFT JOIN part ON part.id=am.moduleId AND am.`type`='part'	
LEFT JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson'
WHERE COALESCE(module.id,part.module,lesson.module,0)!=0 AND f4.id=id AND a.isDeleted=0 
AND (CASE WHEN COALESCE(a.classId,0)!=0 THEN a.classId=standard ELSE a.studentId=studentId END) 
AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'))
UNION 
(SELECT DISTINCT lesson.id AS lid, COALESCE(part.id,lesson.partId) AS partid,
COALESCE(module.id,part.module,lesson.module) AS moduleid,am.id,
COALESCE(module.moduleName, part.part, lesson.`name`) AS `name`, 
am.`type` AS `type` FROM assignfolder a  
LEFT JOIN assignedmodules am ON a.moduleId=am.id AND am.folderType='fourth' 
 LEFT JOIN module ON module.id=am.moduleId AND am.`type`='module'
 LEFT JOIN part ON part.id=am.moduleId AND am.`type`='part'	
 LEFT JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson'
WHERE COALESCE(module.id,part.module,lesson.module,0)!=0 AND am.folderId=id AND a.isDeleted=0 
AND (CASE WHEN COALESCE(a.classId,0)!=0 THEN a.classId=standard ELSE a.studentId=studentId END) 
AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'));
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getFourthFolderList
DROP PROCEDURE IF EXISTS `getFourthFolderList`;
DELIMITER //
CREATE PROCEDURE `getFourthFolderList`(
	IN `id` INT,
	IN `standard` INT,
	IN `studentId` INT,
	IN `currentDate` VARCHAR(50)




)
BEGIN
(SELECT DISTINCT 0 AS lid,0 AS partId,0 AS moduleId,f4.id,f4.`name`,'folder' AS `type` 
FROM assignallfolders a  
LEFT JOIN sixthfolder f6 ON a.folderId=f6.id AND a.foldertype='sixth'
LEFT JOIN fifthfolder f5 ON (case 
		when  a.foldertype='fifth' then a.folderId=f5.id 
		when a.foldertype='sixth' then f5.id=f6.fifthFolderId END)
LEFT JOIN fourthfolder f4 ON (case 
		when a.foldertype='third' then a.folderId=f4.thirdFolderId
		when a.foldertype='fourth' then a.folderId=f4.id
		when a.foldertype='fifth' then f4.id=f5.fourthFolderId
		when a.foldertype='sixth' then f4.id=f5.fourthFolderId END) 
WHERE COALESCE(f4.id,'')!='' AND f4.thirdFolderId=id AND a.isDeleted=0 AND (CASE WHEN COALESCE(a.classId,0)!=0 
THEN a.classId=standard ELSE a.studentId=studentId END) AND STR_TO_DATE(currentDate,'%d/%m/%Y') 
BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'))
UNION 
(SELECT DISTINCT lesson.id AS lid, COALESCE(part.id,lesson.partId) AS partid,
COALESCE(module.id,part.module,lesson.module) AS moduleid,am.id,
COALESCE(module.moduleName, part.part, lesson.`name`) AS `name`,
COALESCE(am.`type`,'folder') AS `type` FROM assignallfolders a  
LEFT JOIN thirdfolder f3 ON a.foldertype='third' AND f3.id=a.folderId 	
LEFT JOIN assignedmodules am ON am.folderId=f3.id AND am.folderType='third'
LEFT JOIN module ON module.id=am.moduleId AND am.`type`='module'
LEFT JOIN part ON part.id=am.moduleId AND am.`type`='part'	
LEFT JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson'
WHERE COALESCE(module.id,part.module,lesson.module,0)!=0 AND f3.id=id AND a.isDeleted=0 
AND (CASE WHEN COALESCE(a.classId,0)!=0 THEN a.classId=standard ELSE a.studentId=studentId END) 
AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'))
 UNION 
(SELECT DISTINCT lesson.id AS lid, COALESCE(part.id,lesson.partId) AS partid,
COALESCE(module.id,part.module,lesson.module) AS moduleid,am.id,
COALESCE(module.moduleName, part.part, lesson.`name`) AS `name`, 
am.`type` AS `type` FROM assignfolder a  
LEFT JOIN assignedmodules am ON a.moduleId=am.id AND am.folderType='third' 
 LEFT JOIN module ON module.id=am.moduleId AND am.`type`='module'
 LEFT JOIN part ON part.id=am.moduleId AND am.`type`='part'	
 LEFT JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson'
WHERE COALESCE(module.id,part.module,lesson.module,0)!=0 AND am.folderId=id AND a.isDeleted=0 
AND (CASE WHEN COALESCE(a.classId,0)!=0 THEN a.classId=standard ELSE a.studentId=studentId END) 
AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'));
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getGoogleMeetEventsForTeacherandStudent
DROP PROCEDURE IF EXISTS `getGoogleMeetEventsForTeacherandStudent`;
DELIMITER //
CREATE PROCEDURE `getGoogleMeetEventsForTeacherandStudent`(
	IN `userid` INT,
	IN `userrole` CHAR(50)
)
BEGIN

IF userrole='STUDENT' THEN

SELECT standard INTO @standard FROM student WHERE id=userid;
SELECT g.id,g.url,g.classid,g.eventdate,g.starttime,g.endtime,
COALESCE(g.classtype,0) as classtype,t.`name` AS teacherName 
FROM googlemeetevents g
INNER JOIN teacher t ON t.id=g.teacherid
WHERE CASE WHEN COALESCE(g.classtype,0)=0 THEN g.classid=@standard
ELSE g.classid IN (SELECT combinedClassId FROM combinedclassstudent WHERE sid=userid) END 
AND g.isStart=0 AND g.isDeleted=0 AND g.eventdate>=CURDATE();

ELSE 

SELECT  g.id,g.url,g.classid,g.eventdate,g.starttime,g.endtime,
COALESCE(g.classtype,0) as classtype,COALESCE(tc.className,cc.`name`) AS className 
FROM googlemeetevents g
LEFT JOIN typesclass tc ON tc.id=g.classid AND COALESCE(g.classtype,0)=0 AND tc.isDeleted=0
LEFT JOIN combinedclass cc ON cc.id=g.classid AND COALESCE(g.classtype,0)=1 AND cc.isDeleted=0
WHERE g.teacherid=userid AND g.isStart=0 AND g.isDeleted=0 AND g.eventdate>=CURDATE();

END IF;


END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getHomeworkCompletedByClassId
DROP PROCEDURE IF EXISTS `getHomeworkCompletedByClassId`;
DELIMITER //
CREATE PROCEDURE `getHomeworkCompletedByClassId`(
	IN `classId` VARCHAR(50)
)
BEGIN

select homework.description,homework.name,student.name as studentName, studenthomeworkcompleted.fileName ,  studenthomeworkcompleted.id as studentHomeWorkCompletedId, studenthomeworkcompleted.feedbackStatus, studenthomeworkcompleted.description as answer,  DATE_FORMAT(studenthomeworkcompleted.createdDate,'%d/%m/%Y %h:%i %p') AS submitted from homework join  assignedhomework on (homework.id=assignedhomework.homeWorkId) join studenthomeworkcompleted on   studenthomeworkcompleted.assignedHomeWorkId=assignedhomework.id join student on student.id=studenthomeworkcompleted.sid   where student.standard= classId and homework.isDeleted=0 ORDER BY studenthomeworkcompleted.id DESC ;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getLastFolderList
DROP PROCEDURE IF EXISTS `getLastFolderList`;
DELIMITER //
CREATE PROCEDURE `getLastFolderList`(
	IN `id` INT,
	IN `standard` INT,
	IN `studentId` INT,
	IN `currentDate` VARCHAR(50)

)
BEGIN
(SELECT DISTINCT lesson.id AS lid, COALESCE(part.id,lesson.partId) AS partid,
COALESCE(module.id,part.module,lesson.module) AS moduleid,am.id,
COALESCE(module.moduleName, part.part, lesson.`name`) AS `name`,
COALESCE(am.`type`,'folder') AS `type` FROM assignallfolders a  
LEFT JOIN thirdfolder f3 ON a.foldertype='third' AND f3.id=a.folderId 
LEFT JOIN fourthfolder f4 ON (case when a.foldertype='third' then f3.id=f4.thirdFolderId
		when a.foldertype='fourth' then f4.id=a.folderId END) 
LEFT JOIN fifthfolder f5 ON (case when a.foldertype='third' then f4.id=f5.fourthFolderId
		when a.foldertype='fourth' then f4.id=f4.id=f5.fourthFolderId
		when a.foldertype='fifth' then f5.id=a.folderId END) 
LEFT JOIN sixthfolder f6 ON (case when a.foldertype='third' then f5.id=f6.fifthFolderId
		when a.foldertype='fourth' then f5.id=f6.fifthFolderId
		when a.foldertype='fifth' then f5.id=f6.fifthFolderId 
		when a.foldertype='fifth' then f6.id=a.folderId END) 		
LEFT JOIN assignedmodules am ON am.folderId=f6.id AND am.folderType='sixth'
LEFT JOIN module ON module.id=am.moduleId AND am.`type`='module'
LEFT JOIN part ON part.id=am.moduleId AND am.`type`='part'	
LEFT JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson'
WHERE COALESCE(module.id,part.module,lesson.module,0)!=0 AND f6.id=id AND a.isDeleted=0 
AND (CASE WHEN COALESCE(a.classId,0)!=0 THEN a.classId=standard ELSE a.studentId=studentId END) 
AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'))
UNION 
(SELECT DISTINCT lesson.id AS lid, COALESCE(part.id,lesson.partId) AS partid,
COALESCE(module.id,part.module,lesson.module) AS moduleid,am.id,
COALESCE(module.moduleName, part.part, lesson.`name`) AS `name`, 
am.`type` AS `type` FROM assignfolder a  
LEFT JOIN assignedmodules am ON a.moduleId=am.id AND am.folderType='sixth' 
 LEFT JOIN module ON module.id=am.moduleId AND am.`type`='module'
 LEFT JOIN part ON part.id=am.moduleId AND am.`type`='part'	
 LEFT JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson'
WHERE COALESCE(module.id,part.module,lesson.module,0)!=0 AND am.folderId=id AND a.isDeleted=0 
AND (CASE WHEN COALESCE(a.classId,0)!=0 THEN a.classId=standard ELSE a.studentId=studentId END) 
AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'));
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getLiveclassIdForCombinedClassStudent
DROP PROCEDURE IF EXISTS `getLiveclassIdForCombinedClassStudent`;
DELIMITER //
CREATE PROCEDURE `getLiveclassIdForCombinedClassStudent`(
	IN `classId` VARCHAR(10),
	IN `std` VARCHAR(10)
)
BEGIN

SELECT liv.* FROM combinedclass cb JOIN combinedclassstudent cbs ON cb.id = cbs.combinedClassId 
JOIN student ON student.id = cbs.sid JOIN liveclassdtls liv  ON liv.standard = cb.id  WHERE cbs.sid = std 
AND student.standard = classId AND liv.id = (  SELECT max(liv.id) FROM combinedclass cb 
JOIN combinedclassstudent cbs ON cb.id = cbs.combinedClassId 
JOIN student ON student.id = cbs.sid JOIN liveclassdtls liv  ON liv.standard = cb.id  WHERE cbs.sid = std  
AND student.standard = classId and liv.isCombined = 1 AND cb.isDeleted = 0 AND cbs.isDeleted = 0 ) AND cbs.isDeleted = 0 and liv.isCombined = 1 ;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getMessagesforEachuser
DROP PROCEDURE IF EXISTS `getMessagesforEachuser`;
DELIMITER //
CREATE PROCEDURE `getMessagesforEachuser`(
	IN `username` VARCHAR(50)
)
BEGIN
SELECT qry.*,a.USRDEFROL FROM (
SELECT COUNT(chat.id) AS `count`,chat.sender AS userName,chat.message,student.`name`
FROM chat INNER JOIN student ON student.userName=chat.sender AND student.isDeleted=0
WHERE chat.receiver=username AND chat.isStatus=0 GROUP BY student.id
UNION 
SELECT COUNT(chat.id) AS `count`,chat.sender AS userName,chat.message,teacher.`name`
FROM chat 
INNER JOIN teacher ON teacher.userName=chat.sender AND teacher.isDeleted=0 
WHERE chat.receiver=username AND chat.isStatus=0 GROUP BY teacher.id
UNION 
SELECT COUNT(chat.id) AS `count`,chat.sender AS userName,chat.message,parent.`name`
FROM chat INNER JOIN parent ON parent.userName=chat.sender AND parent.isDeleted=0
WHERE chat.receiver=username AND chat.isStatus=0 GROUP BY parent.id ) AS qry 
INNER JOIN admusrmst a ON a.USRCOD = qry.userName;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getQuestionCount
DROP PROCEDURE IF EXISTS `getQuestionCount`;
DELIMITER //
CREATE PROCEDURE `getQuestionCount`(

	IN `assignedExamId` INT



)
BEGIN



 DECLARE INDX INT;

 DECLARE SLICE nvarchar(4000); 

 DECLARE StringValue nvarchar(4000); 

 SET INDX = 1;

 

 DROP TEMPORARY TABLE IF EXISTS countTables;

 CREATE TEMPORARY TABLE countTables(Items VARCHAR(100));

 

 select exam.selectedQuestions into StringValue from assignedexam join exam on assignedexam.examId=exam.id  where assignedexam.id=assignedExamId;

 

 

 MyLoop: WHILE INDX != 0 DO

 

 SET INDX = LOCATE(',',StringValue);

 

 IF INDX !=0 THEN

  SET SLICE = LEFT(StringValue,INDX - 1);

 ELSE

  SET SLICE = StringValue;

 END IF;

 

 INSERT INTO countTables(Items) VALUES(SLICE);

 

 SET StringValue = RIGHT(StringValue,LENGTH(StringValue) - INDX);

 

 IF LENGTH(StringValue) = 0 THEN

  LEAVE MyLoop;

 END IF;

 END WHILE;

 select * from countTables;



END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getSixthFolderList
DROP PROCEDURE IF EXISTS `getSixthFolderList`;
DELIMITER //
CREATE PROCEDURE `getSixthFolderList`(
	IN `id` INT,
	IN `standard` INT,
	IN `studentId` INT,
	IN `currentDate` VARCHAR(50)




)
BEGIN
(SELECT DISTINCT 0 AS lid,0 AS partId,0 AS moduleId,
COALESCE(f16.id,f6.id) AS id,COALESCE(f16.`name`,f6.`name`)AS `name`,'folder' AS `type`
FROM assignallfolders a  
LEFT JOIN sixthfolder f6 ON a.folderId=f6.id AND a.foldertype='sixth'
LEFT JOIN fifthfolder f5 ON (case 
		when  a.foldertype='fifth' then a.folderId=f5.id 
		when a.foldertype='sixth' then f5.id=f6.fifthFolderId END)
LEFT JOIN fourthfolder f4 ON (case 
		when a.foldertype='third' then a.folderId=f4.thirdFolderId
		when a.foldertype='fourth' then a.folderId=f4.id
		when a.foldertype='fifth' then f4.id=f5.fourthFolderId
		when a.foldertype='sixth' then f4.id=f5.fourthFolderId END) 
LEFT JOIN fifthfolder fif5 ON (case 
	when a.foldertype='third' then fif5.fourthFolderId=f4.id
	when a.foldertype='fourth' then fif5.fourthFolderId=f4.id END)
LEFT JOIN sixthfolder f16 ON  (case 
	when a.foldertype='third' then f16.fifthFolderId=fif5.id
	when a.foldertype='fourth' then f16.fifthFolderId=fif5.id 
	when a.foldertype='fifth' then f5.id=f16.fifthFolderId END)
WHERE COALESCE(f16.id,f6.id,'')!='' AND COALESCE(f5.id,fif5.id)=id AND a.isDeleted=0 AND (CASE WHEN COALESCE(a.classId,0)!=0 
THEN a.classId=standard  ELSE a.studentId=studentId END) AND STR_TO_DATE(currentDate,'%d/%m/%Y') 
BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'))
UNION 
(SELECT DISTINCT lesson.id AS lid, COALESCE(part.id,lesson.partId) AS partid,
COALESCE(module.id,part.module,lesson.module) AS moduleid,am.id,
COALESCE(module.moduleName, part.part, lesson.`name`) AS `name`,
COALESCE(am.`type`,'folder') AS `type` FROM assignallfolders a  
LEFT JOIN thirdfolder f3 ON a.foldertype='third' AND f3.id=a.folderId 
LEFT JOIN fourthfolder f4 ON (case when a.foldertype='third' then f3.id=f4.thirdFolderId
		when a.foldertype='fourth' then f4.id=a.folderId END) 
LEFT JOIN fifthfolder f5 ON (case when a.foldertype='third' then f4.id=f5.fourthFolderId
		when a.foldertype='fourth' then f4.id=f4.id=f5.fourthFolderId
		when a.foldertype='fifth' then f5.id=a.folderId END) 
LEFT JOIN assignedmodules am ON am.folderId=f5.id AND am.folderType='fifth'
LEFT JOIN module ON module.id=am.moduleId AND am.`type`='module'
LEFT JOIN part ON part.id=am.moduleId AND am.`type`='part'	
LEFT JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson'
WHERE COALESCE(module.id,part.module,lesson.module,0)!=0 AND f5.id=id AND a.isDeleted=0 
AND (CASE WHEN COALESCE(a.classId,0)!=0 THEN a.classId=standard ELSE a.studentId=studentId END) 
AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'))
UNION 
(SELECT DISTINCT lesson.id AS lid, COALESCE(part.id,lesson.partId) AS partid,
COALESCE(module.id,part.module,lesson.module) AS moduleid,am.id,
COALESCE(module.moduleName, part.part, lesson.`name`) AS `name`, 
am.`type` AS `type` FROM assignfolder a  
LEFT JOIN assignedmodules am ON a.moduleId=am.id AND am.folderType='fifth' 
 LEFT JOIN module ON module.id=am.moduleId AND am.`type`='module'
 LEFT JOIN part ON part.id=am.moduleId AND am.`type`='part'	
 LEFT JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson'
WHERE COALESCE(module.id,part.module,lesson.module,0)!=0 AND am.folderId=id AND a.isDeleted=0 
AND (CASE WHEN COALESCE(a.classId,0)!=0 THEN a.classId=standard ELSE a.studentId=studentId END) 
AND STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(a.endDate,a.assigningDate),'%d/%m/%Y'));
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getStudentActivityDetailslist
DROP PROCEDURE IF EXISTS `getStudentActivityDetailslist`;
DELIMITER //
CREATE PROCEDURE `getStudentActivityDetailslist`(
	IN `id` INT,
	IN `subjectid` INT
)
BEGIN

SELECT s.id,COALESCE(s.feedback,'') AS feedback ,student.standard,DATE_FORMAT(s.createdDate, '%d/%m/%Y')AS createdDate, 
s.`file`, lesson.`name` lesson,a.`name`,f3.name AS subject 
FROM assignmentstudent s 
INNER JOIN assignment a ON a.id=s.assignmentId AND a.isDeleted=0  
INNER JOIN lesson ON lesson.id=a.lessonId  AND lesson.isDeleted=0 
INNER JOIN assignedmodules am ON am.moduleId=lesson.id AND am.`type`='lesson' AND am.isDeleted=0 
INNER JOIN fourthfolder f4 ON f4.id=am.folderId AND am.folderType='fourth' AND f4.isDeleted=0 
INNER JOIN thirdfolder f3 ON f3.id=f4.thirdFolderId AND f3.isDeleted=0 
INNER JOIN student ON student.id=s.studentId
WHERE s.isDeleted=0 
AND CASE WHEN id!=0 THEN student.id=id ELSE 1=1 END 
AND CASE WHEN subjectid!=0 THEN f3.id=subjectid ELSE 1=1 END
ORDER BY s.id DESC ;


END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getStudentAnswerSheet
DROP PROCEDURE IF EXISTS `getStudentAnswerSheet`;
DELIMITER //
CREATE PROCEDURE `getStudentAnswerSheet`(
	IN `sid` INT,
	IN `assignedExamid` INT
)
BEGIN

SELECT e.*,ex.examName, DATE_FORMAT(a.examDate,'%d/%m/%Y') AS examDate , TIME_FORMAT(a.createdDate,'%h:%i %p') AS startTime , s.name ,s.userName FROM examfileanswer e 
JOIN assignedexamfile a ON e.assignedExamId = a.id 
JOIN examfile ex ON ex.id = a.examId
JOIN student s ON s.id = e.sid
 WHERE e.sid = sid AND  e.assignedExamId = assignedExamid ; 

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getStudentDetailsForEditStudent
DROP PROCEDURE IF EXISTS `getStudentDetailsForEditStudent`;
DELIMITER //
CREATE PROCEDURE `getStudentDetailsForEditStudent`(
	IN `studentid` INT
)
BEGIN

SELECT student.id,student.`name`,student.guardianName,student.createdDate,student.standard,
student.mobile,student.address,student.image,student.userName,student.currentActivity,student.groupId,
student.isModel,student.mailid,student.whatsapp,student.dob,student.national_id,student.religion,
student.studmobilecountrycode,student.studwtsupcountrycode,student.isSuspend,typesclass.className,
parent.id AS parentId,admusrmst.isActive,COALESCE(parent.userName,'')  AS userNameParent, 
parent.parentcountrycode,parent.mobile AS parentmobile,parent.mailid AS parentmailid,
parent.parentwtsupcountrycode,parent.whatsapp AS parentwhatsapp FROM student 
LEFT JOIN typesclass ON (student.standard=typesclass.id  AND typesclass.isDeleted=0)
LEFT JOIN admusrmst ON admusrmst.USRCOD=student.userName 
LEFT JOIN parent ON student.id=parent.studentId 
WHERE student.isDeleted=0 AND student.id=studentid;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getStudentdetailsViewStudent
DROP PROCEDURE IF EXISTS `getStudentdetailsViewStudent`;
DELIMITER //
CREATE PROCEDURE `getStudentdetailsViewStudent`(
	IN `username` VARCHAR(50)
)
BEGIN

SELECT COALESCE(s.id,t.id) AS id, COALESCE(s.`name`,t.`name`) AS `name`, 
	COALESCE(s.image,t.image,'/assets/DefaultUser.jpg') AS image,
	COALESCE(s.standard,0) AS standard, a.USRDEFROL AS address,a.USRCOD AS userName 
FROM admusrmst a 
LEFT JOIN student s ON s.userName=a.USRCOD AND s.isDeleted=0
LEFT JOIN teacher t ON t.userName=a.USRCOD AND t.isDeleted=0
WHERE a.USRCOD=username;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getStudentExamFiles
DROP PROCEDURE IF EXISTS `getStudentExamFiles`;
DELIMITER //
CREATE PROCEDURE `getStudentExamFiles`(
	IN `sid` VARCHAR(20),
	IN `classid` VARCHAR(20)
)
BEGIN

SELECT examfile.*,assignedexamfile.startTime,assignedexamfile.endTime,assignedexamfile.id as aid,assignedexamfile.examDate  
FROM examfile JOIN assignedexamfile ON examfile.id=assignedexamfile.examId 
WHERE  assignedexamfile.statusOver=0 and  assignedexamfile.isDeleted=0 and assignedexamfile.examDate >= (select curdate()) 
	   and (CASE WHEN (COALESCE(assignedexamfile.classId,0)!=0) THEN assignedexamfile.classId=classid
		  ELSE assignedexamfile.studentId = sid END);

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getStudentExamListBySid
DROP PROCEDURE IF EXISTS `getStudentExamListBySid`;
DELIMITER //
CREATE PROCEDURE `getStudentExamListBySid`(
	IN `sid` VARCHAR(20),
	IN `fromDate` VARCHAR(10),
	IN `toDate` VARCHAR(10)
)
BEGIN

SELECT assignedexam.id AS id,DATE_FORMAT(assignedexam.examDate,'%d/%m/%Y') AS examDate,exam.examName,
assignedexam.startTime, assignedexam.endTime,mark.mark ,mark.grade,exam.totalMark,   
COALESCE(mark.examtype,0) AS examtype FROM mark 
JOIN assignedexam ON assignedexam.id = mark.assignedExamId AND assignedexam.isDeleted = 0
 AND COALESCE(mark.examType,0) = 0
JOIN exam ON exam.id = assignedexam.examId  AND exam.isDeleted = 0 
WHERE mark.sid = sid AND assignedexam.examDate BETWEEN fromDate AND toDate GROUP BY assignedexam.id

UNION

SELECT assignednewexam.id AS id,DATE_FORMAT(assignednewexam.examDate,'%d/%m/%Y') AS examDate,
newexam.examName,assignednewexam.startTime, assignednewexam.endTime,mark.mark ,mark.grade,newexam.totalMark,   
COALESCE(mark.examtype,1) AS examtype FROM mark 
JOIN assignednewexam ON assignednewexam.id = mark.assignedExamId AND assignednewexam.isDeleted = 0
 AND COALESCE(mark.examType,1) = 1
JOIN newexam ON newexam.id = assignednewexam.examId  AND newexam.isDeleted = 0 
WHERE mark.sid = sid AND assignednewexam.examDate BETWEEN fromDate AND toDate ;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getStudentExams
DROP PROCEDURE IF EXISTS `getStudentExams`;
DELIMITER //
CREATE PROCEDURE `getStudentExams`(
	IN `sid` VARCHAR(50),
	IN `classId` VARCHAR(50)
)
BEGIN

SELECT exam.*,TIME_FORMAT(assignedexam.startTime,'%h:%i %p') AS startTime,
TIME_FORMAT(assignedexam.endTime, '%h:%i %p') AS endTime,assignedexam.id as aid,
DATE_FORMAT(assignedexam.examDate,'%d/%m/%Y' ) AS examDate
 FROM exam   JOIN assignedexam ON exam.id=assignedexam.examId 
   WHERE  assignedexam.statusOver=0 and  assignedexam.isDeleted=0 
  and assignedexam.examDate >= (select curdate())  and  
    (assignedexam.id not in (select completedexam.assignedExamid 
   from completedexam where completedexam.sid = sid AND completedexam.examtype=0 
	GROUP by completedexam.assignedExamid)) 
		 and (CASE WHEN (COALESCE(assignedexam.classId,0)!=0) THEN assignedexam.classId= classId 
	  ELSE assignedexam.studentId = sid END);

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getStudentlistforAllStudents
DROP PROCEDURE IF EXISTS `getStudentlistforAllStudents`;
DELIMITER //
CREATE PROCEDURE `getStudentlistforAllStudents`()
BEGIN

SELECT s.id,s.guardianName,s.`name`,s.standard,s.mailid,s.image,
	CONCAT (COALESCE(s.studmobilecountrycode,''), s.mobile) AS mobile ,
	CONCAT (COALESCE(s.studwtsupcountrycode,''), s.whatsapp) AS whatsapp,
	t.className,admusrmst.isActive, parent.mailid AS parentmailid,
	CONCAT (COALESCE(parent.parentcountrycode,''), parent.mobile) AS parentmobile,
	CONCAT (COALESCE(parent.parentwtsupcountrycode,''), parent.whatsapp) AS parentwhatsapp,
	s.userName,s.address,s.dob,s.national_id,s.religion,s.studmobilecountrycode,
	s.studwtsupcountrycode,s.isSuspend 
FROM student s
	INNER JOIN admusrmst ON admusrmst.USRCOD=s.userName 
	INNER JOIN typesclass t on t.id=s.standard
	LEFT JOIN parent ON parent.studentId=s.id 
WHERE s.isDeleted=0 AND s.isSuspend=0 AND admusrmst.isActive=1;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getStudentlistforClassroom
DROP PROCEDURE IF EXISTS `getStudentlistforClassroom`;
DELIMITER //
CREATE PROCEDURE `getStudentlistforClassroom`(
	IN `standard` INT
)
BEGIN

SELECT student.id,student.guardianName,student.`name`,student.image, 
student.isSuspend,student.userName,student.mailid,student.address, 
CONCAT (COALESCE(student.studmobilecountrycode,''), student.mobile) AS mobile , 
CONCAT (COALESCE(student.studwtsupcountrycode,''), student.whatsapp) AS whatsapp, 
typesclass.className,admusrmst.isActive, 
parent.mailid AS parentmailid,parent.mobile AS parentmobile, 
parent.whatsapp AS parentwhatsapp FROM student 
JOIN typesclass ON student.standard=typesclass.id 
JOIN admusrmst ON admusrmst.USRCOD=student.userName 
LEFT JOIN parent ON parent.studentId=student.id 
WHERE student.isDeleted=0 AND typesclass.isDeleted=0 
AND student.standard=standard AND admusrmst.isActive=1 ;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getStudentNewExams
DROP PROCEDURE IF EXISTS `getStudentNewExams`;
DELIMITER //
CREATE PROCEDURE `getStudentNewExams`(
	IN `sid` INT,
	IN `classId` INT
)
BEGIN

SELECT n.id,n.examname,a.id AS aid,
DATE_FORMAT(a.examDate,'%d/%m/%Y' ) AS examDate
,n.totalhour,n.totalminute,
 n.totalmark,TIME_FORMAT(a.startTime,'%h:%i %p') AS startTime,
 TIME_FORMAT(a.endTime, '%h:%i %p') AS endTime
  FROM newexam n  
  INNER JOIN assignednewexam a ON a.examId=n.id 
  WHERE a.isDeleted=0 AND n.isDeleted=0 AND a.examDate >= (SELECT CURDATE())  
  AND (CASE WHEN (COALESCE(a.classId,0)!=0) THEN a.classId= classId ELSE a.studentId = sid END) 
   AND a.id NOT IN (SELECT mark.assignedExamId 
	FROM mark WHERE mark.sid = sid AND mark.examType = 1) ;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getStudentsListWithidlist
DROP PROCEDURE IF EXISTS `getStudentsListWithidlist`;
DELIMITER //
CREATE PROCEDURE `getStudentsListWithidlist`(
	IN `useridlist` LONGTEXT
)
BEGIN

 DECLARE INDX INT;

 DECLARE SLICE VARCHAR(10); 
 SET INDX = 1;
 
DROP TEMPORARY TABLE IF EXISTS studidlist;

 CREATE TEMPORARY TABLE studidlist(Items VARCHAR(100));

MyLoop: WHILE INDX != 0 DO 

 SET INDX = LOCATE(',',useridlist); 

 IF INDX !=0 THEN 
  SET SLICE = LEFT(useridlist,INDX - 1);
 ELSE
  SET SLICE = useridlist;
 END IF;
 
 INSERT INTO studidlist(Items) VALUES(SLICE);
 SET useridlist = RIGHT(useridlist,LENGTH(useridlist) - INDX);
 IF LENGTH(useridlist) = 0 THEN
  LEAVE MyLoop;

 END IF;

 END WHILE;
 

SELECT student.id, student.userName,student.name, 
CONCAT (COALESCE(student.studmobilecountrycode,''), student.mobile) AS mobile , 
CONCAT (COALESCE(student.studwtsupcountrycode,''), student.whatsapp) AS whatsapp , 
typesclass.className,admusrmst.isActive, 
parent.mailid AS parentmailid,CONCAT (COALESCE(parent.parentcountrycode,''),parent.mobile) AS parentmobile,  
CONCAT(COALESCE(parent.parentwtsupcountrycode,''),parent.whatsapp) AS parentwhatsapp, 
parent.userName AS userNameParent FROM student 
JOIN typesclass ON student.standard=typesclass.id 
JOIN admusrmst ON admusrmst.USRCOD=student.userName 
LEFT JOIN parent ON parent.studentId=student.id  
WHERE student.isDeleted=0 AND typesclass.isDeleted=0  
AND student.id IN (SELECT * FROM studidlist) AND admusrmst.isActive=1; 

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getTeacherActivitybyClass
DROP PROCEDURE IF EXISTS `getTeacherActivitybyClass`;
DELIMITER //
CREATE PROCEDURE `getTeacherActivitybyClass`(
	IN `id` INT,
	IN `fromdate` VARCHAR(50),
	IN `todate` VARCHAR(50)

,
	IN `cid` INT
)
BEGIN
SELECT qry.assigningDate,qry.lesson FROM (
SELECT af.assigningDate,COALESCE(module.moduleName,part.part,lesson.`name`) AS lesson,
af.createdBy,af.createdDate FROM assignlessonfromfolder af 
LEFT JOIN module ON module.id=af.moduleId AND af.moduleType='module'
LEFT JOIN part ON part.id=af.moduleId AND af.moduleType='part'
LEFT JOIN lesson ON lesson.id=af.moduleId AND af.moduleType='lesson'
WHERE STR_TO_DATE(af.assigningDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(fromdate,'%d/%m/%Y') 
AND STR_TO_DATE(todate,'%d/%m/%Y') AND af.classId=cid 
GROUP BY af.assigningDate,af.moduleId
UNION
SELECT af.assigningDate, COALESCE(f2.`name`,f3.`name`,f4.`name`,f5.`name`,f6.`name`) AS lesson,
af.createdBy,af.createdDate FROM assignallfolders af 
LEFT JOIN secondfolder f2 ON f2.id=af.folderId AND af.foldertype='second'
LEFT JOIN thirdfolder f3 ON f3.id=af.folderId AND af.foldertype='third'
LEFT JOIN fourthfolder f4 ON f4.id=af.folderId AND af.foldertype='fourth'
LEFT JOIN fifthfolder f5 ON f5.id=af.folderId AND af.foldertype='fifth'
LEFT JOIN sixthfolder f6 ON f6.id=af.folderId AND af.foldertype='sixth'
WHERE STR_TO_DATE(af.assigningDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(fromdate,'%d/%m/%Y') 
AND STR_TO_DATE(todate,'%d/%m/%Y') AND af.classId=cid 
GROUP BY af.assigningDate,af.folderId
UNION
SELECT assignfolder.assigningDate,COALESCE(module.moduleName,part.part,lesson.`name`) AS lesson,
assignfolder.createdBy,assignfolder.createdDate FROM assignfolder 
INNER JOIN assignedmodules ON assignedmodules.id=assignfolder.moduleId
LEFT JOIN module ON module.id=assignedmodules.moduleId AND assignedmodules.`type`='module'
LEFT JOIN part ON part.id=assignedmodules.moduleId AND assignedmodules.`type`='part'
LEFT JOIN lesson ON lesson.id=assignedmodules.moduleId AND assignedmodules.`type`='lesson'
WHERE STR_TO_DATE(assignfolder.assigningDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(fromdate,'%d/%m/%Y') 
AND STR_TO_DATE(todate,'%d/%m/%Y') AND assignfolder.classId=cid 
GROUP BY assignfolder.assigningDate,assignfolder.moduleId
UNION
SELECT assignlesson.assigningDate,COALESCE(lesson.`name`,part.part) AS lesson,
assignlesson.createdBy,assignlesson.createdDate FROM assignlesson
LEFT JOIN lesson ON lesson.id=assignlesson.lessonId AND assignlesson.`type`='lesson'
LEFT JOIN part ON part.id=assignlesson.lessonId AND assignlesson.`type`='part'
WHERE STR_TO_DATE(assignlesson.assigningDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(fromdate,'%d/%m/%Y') 
AND STR_TO_DATE(todate,'%d/%m/%Y') AND assignlesson.classId=cid
GROUP BY assignlesson.assigningDate,assignlesson.lessonId) AS qry
INNER JOIN teacher ON teacher.userName=qry.createdBy 
WHERE teacher.id=id ORDER BY qry.createdDate;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.getTeacherActivityReport
DROP PROCEDURE IF EXISTS `getTeacherActivityReport`;
DELIMITER //
CREATE PROCEDURE `getTeacherActivityReport`(
	IN `fromdate` VARCHAR(50),
	IN `todate` VARCHAR(50)



)
BEGIN
SELECT teacher.id,teacher.`name`,teacher.image, SUM(COALESCE(a.c,0) + COALESCE(b.c,0)+ COALESCE(c.c,0)+ COALESCE(d.c,0)) 
AS totalLessons FROM teacher
LEFT JOIN (SELECT COUNT(af.id) AS c ,af.createdBy FROM assignlessonfromfolder af
WHERE STR_TO_DATE(af.assigningDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(fromdate,'%d/%m/%Y') 
AND STR_TO_DATE(todate,'%d/%m/%Y') GROUP BY af.createdBy) AS a
	ON a.createdBy=teacher.userName 
LEFT JOIN (SELECT COUNT(assignallfolders.id) AS c ,assignallfolders.createdBy	FROM assignallfolders 
WHERE STR_TO_DATE(assignallfolders.assigningDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(fromdate,'%d/%m/%Y') 
AND STR_TO_DATE(todate,'%d/%m/%Y') GROUP BY assignallfolders.createdBy) AS b
	ON b.createdBy=teacher.userName
LEFT JOIN (SELECT COUNT(assignfolder.id) AS c , assignfolder.createdBy FROM assignfolder 
WHERE STR_TO_DATE(assignfolder.assigningDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(fromdate,'%d/%m/%Y') 
AND STR_TO_DATE(todate,'%d/%m/%Y')	GROUP BY assignfolder.createdBy) AS c
	ON c.createdBy=teacher.userName
LEFT JOIN (SELECT COUNT(assignlesson.id) AS c , assignlesson.createdBy FROM assignlesson 
WHERE STR_TO_DATE(assignlesson.assigningDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(fromdate,'%d/%m/%Y') 
AND STR_TO_DATE(todate,'%d/%m/%Y')	GROUP BY assignlesson.createdBy) AS d
	ON d.createdBy=teacher.userName
WHERE COALESCE(a.c,b.c,c.c,d.c,0)!=0 
GROUP BY teacher.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.logout_broswerCloseLogoutTime
DROP PROCEDURE IF EXISTS `logout_broswerCloseLogoutTime`;
DELIMITER //
CREATE PROCEDURE `logout_broswerCloseLogoutTime`(
	IN `username` VARCHAR(350)
)
BEGIN

SELECT MAX(id) INTO @attendanceid FROM attendance WHERE attendance.username = username;

UPDATE attendance  SET isActive = false , logout =  CURTIME() WHERE username = username  AND id = @attendanceid;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.newexam_studentMarkUpdate
DROP PROCEDURE IF EXISTS `newexam_studentMarkUpdate`;
DELIMITER //
CREATE PROCEDURE `newexam_studentMarkUpdate`(
	IN `assignedExamId` VARCHAR(50),
	IN `classId` INT
)
BEGIN

SELECT newexam.totalmark INTO @mark FROM newexam JOIN assignednewexam ON newexam.id = assignednewexam.examId 
WHERE newexam.isDeleted = 0 AND assignednewexam.isDeleted = 0 AND assignednewexam.id = assignedExamId;

 UPDATE mark , (
SELECT tab1.username,tab1.mark, 
(case when ((tab1.mark/@mark)*100 > 90) THEN 'A+' 
      when ((tab1.mark/@mark)*100  > 80) THEN 'A'
		when ((tab1.mark/@mark)*100 > 70) THEN 'B+'
		when ((tab1.mark/@mark)*100 > 60) THEN 'B'
		when ((tab1.mark/@mark)*100 > 50) THEN 'C+'
		when ((tab1.mark/@mark)*100 > 40) THEN 'C' ELSE 'D' END)  AS 'grade' , tab1.sid AS 'sid'
From (SELECT tab.* FROM (SELECT  student.username, student.id AS 'sid' ,(SELECT COUNT(completedexam.id) AS id  FROM completedexam
JOIN exercise ON completedexam.selectedQuestions=exercise.id
LEFT JOIN options opt ON opt.exerciseId=exercise.id
WHERE completedexam.assignedExamid = assignedExamId AND completedexam.sid= student.id 
AND (CASE WHEN exercise.`type`=3 THEN (CASE WHEN opt.opt1=completedexam.selectedAnswers THEN TRUE
 WHEN opt.opt2=completedexam.selectedAnswers THEN  TRUE WHEN opt.opt3=completedexam.selectedAnswers THEN TRUE 
 WHEN opt.opt4=completedexam.selectedAnswers THEN TRUE WHEN opt.opt5=completedexam.selectedAnswers THEN TRUE END ) ELSE exercise.answer=completedexam.selectedAnswers END ) = TRUE AND COALESCE(completedexam.examtype,0)= 1) AS 'mark'  FROM student WHERE student.standard = classId and student.isdeleted = 0 ) AS tab) AS tab1) AS tab2
 SET mark.mark = tab2.mark , mark.grade = tab2.grade 
WHERE tab2.sid = mark.sid AND mark.assignedExamId = assignedExamId AND mark.examtype = 1;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_checkStudentAlerts
DROP PROCEDURE IF EXISTS `orelltalk_checkStudentAlerts`;
DELIMITER //
CREATE PROCEDURE `orelltalk_checkStudentAlerts`(
	IN `userName` VARCHAR(50)
)
BEGIN
SELECT studentalert.*,COUNT(studentalert.id) AS count,student.name,student.userName,typesclass.className 
FROM studentalert JOIN student ON studentalert.alertFrom=student.id 
INNER JOIN typesclass ON student.standard=typesclass.id  
INNER JOIN teacher ON teacher.id=studentalert.alertTo
WHERE teacher.userName=userName AND studentalert.createdDate>CURDATE() GROUP BY studentalert.alertFrom;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_editExercise
DROP PROCEDURE IF EXISTS `orelltalk_editExercise`;
DELIMITER //
CREATE PROCEDURE `orelltalk_editExercise`(
	IN `eId` INT
)
BEGIN
SELECT exercise.id, exercise.lessonId ,exercise.question, exercise.answer,exercise.opttype,  
exercise.`file`, exercise.`type`, exercise.mark, exercise.descriptiveType, 
options.id AS optId,coalesce(options.opt1,'') AS opt1,COALESCE(options.opt2,'') AS opt2, 
COALESCE(options.opt3,'') as opt3,COALESCE(options.opt4,'') AS opt4,COALESCE(options.opt5,'') as opt5 
FROM exercise 
LEFT JOIN options ON options.exerciseId=exercise.id 
WHERE exercise.isDeleted=0 AND exercise.id=eId;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_editFifthFolder
DROP PROCEDURE IF EXISTS `orelltalk_editFifthFolder`;
DELIMITER //
CREATE PROCEDURE `orelltalk_editFifthFolder`(
	IN `fId` INT
)
BEGIN
SELECT fifthfolder.id, fifthfolder.name, fourthfolder.`name` AS fourthFolderName, 
thirdfolder.`name` AS thirdFolderName,secondfolder.`name` AS secondFolderName,
folder.`name` AS folderName, thirdfolder.id AS thirdFolderId,	secondfolder.id AS secondFolderId,
folder.id AS folderId,fifthfolder.fourthFolderId                                                                                       
FROM fifthfolder
INNER JOIN fourthfolder ON fourthfolder.id=fifthfolder.fourthFolderId  
INNER JOIN thirdfolder ON thirdfolder.id=fourthfolder.thirdFolderId  
INNER JOIN secondfolder ON secondfolder.id=thirdfolder.secondFolderId  
INNER JOIN folder ON folder.id=secondfolder.folderId where fifthfolder.id=fId;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_editFourthFolder
DROP PROCEDURE IF EXISTS `orelltalk_editFourthFolder`;
DELIMITER //
CREATE PROCEDURE `orelltalk_editFourthFolder`(
	IN `fId` INT
)
BEGIN
SELECT fourthfolder.id, fourthfolder.name, thirdfolder.`name` AS thirdFolderName, 
secondfolder.`name` AS secondFolderName,folder.`name` AS folderName,
secondfolder.id AS secondFolderId, folder.id AS folderId,fourthfolder.thirdFolderId  
FROM fourthfolder
INNER JOIN thirdfolder ON thirdfolder.id=fourthfolder.thirdFolderId                                         
INNER JOIN secondfolder ON secondfolder.id=thirdfolder.secondFolderId
INNER JOIN folder ON folder.id=secondfolder.folderId where fourthfolder.id=fId;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_editLesson
DROP PROCEDURE IF EXISTS `orelltalk_editLesson`;
DELIMITER //
CREATE PROCEDURE `orelltalk_editLesson`(
	IN `lessonId` VARCHAR(50)
)
BEGIN
select lesson.id, lesson.listenFile, lesson.module, 
lesson.`name`, lesson.partId, lesson.listenFilePdf,  
lesson.repeatFile,lesson.roleplayFile,lesson.`text`, 
lesson.repeatFilePdf,lesson.roleplayFilePdf,lesson.listenSubtitle,
lesson.repeatSubtitle,lesson.roleplaySubtitle,
module.moduleName, module.moduleType, part.part AS part                            
FROM lesson
INNER JOIN module ON lesson.module=module.id 
INNER JOIN part ON lesson.partId=part.id  
WHERE lesson.id=lessonId AND lesson.isDeleted=0 
AND module.isDeleted=0 AND part.isDeleted=0;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_editSixthFolder
DROP PROCEDURE IF EXISTS `orelltalk_editSixthFolder`;
DELIMITER //
CREATE PROCEDURE `orelltalk_editSixthFolder`(
	IN `sId` INT
)
BEGIN
SELECT sixthfolder.id, sixthfolder.name, fifthfolder.`name` AS fifthFolderName,
fourthfolder.`name` AS fourthFolderName, thirdfolder.`name` AS thirdFolderName,secondfolder.`name` AS secondFolderName,  
folder.`name` AS folderName,sixthfolder.fifthFolderId, fourthfolder.id AS fourthFolderId,  
thirdfolder.id AS thirdFolderId, secondfolder.id AS secondFolderId, folder.id AS folderId                      
FROM sixthfolder 
INNER JOIN fifthfolder ON fifthfolder.id=sixthfolder.fifthFolderId  
INNER JOIN fourthfolder ON fourthfolder.id=fifthfolder.fourthFolderId  
INNER JOIN thirdfolder ON thirdfolder.id=fourthfolder.thirdFolderId  
INNER JOIN secondfolder ON secondfolder.id=thirdfolder.secondFolderId  
INNER JOIN folder ON folder.id=secondfolder.folderId where sixthfolder.id=sId;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_editStudentDetails
DROP PROCEDURE IF EXISTS `orelltalk_editStudentDetails`;
DELIMITER //
CREATE PROCEDURE `orelltalk_editStudentDetails`(
	IN `studentId` INT
)
BEGIN
SELECT student.id,student.guardianName,student.standard,student.mobile,student.address,student.name,student.image,student.userName,student.groupId,
typesclass.className, DATE_FORMAT(student.createdDate,'%d/%m/%Y') AS fromDate,
admusrmst.isActive,parent.userName AS userNameParent,parent.id AS parentId FROM student 
LEFT JOIN typesclass ON (student.standard=typesclass.id  AND typesclass.isDeleted=0)
LEFT JOIN admusrmst ON admusrmst.USRCOD=student.userName 
LEFT JOIN parent ON student.id=parent.studentId 
WHERE student.isDeleted=0 AND student.id=studentId;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_editThirdFolder
DROP PROCEDURE IF EXISTS `orelltalk_editThirdFolder`;
DELIMITER //
CREATE PROCEDURE `orelltalk_editThirdFolder`(
	IN `tId` INT
)
BEGIN
SELECT thirdfolder.id, thirdfolder.name, secondfolder.`name` AS secondFolderName,
folder.`name` AS folderName,folder.id AS folderId,thirdfolder.secondFolderId 
FROM thirdfolder
INNER JOIN secondfolder ON secondfolder.id=thirdfolder.secondFolderId                                    
INNER JOIN folder ON folder.id=secondfolder.folderId 
WHERE thirdfolder.id=tId  AND thirdfolder.isDeleted=0;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getAddedFirstFolderDetails
DROP PROCEDURE IF EXISTS `orelltalk_getAddedFirstFolderDetails`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getAddedFirstFolderDetails`(
	IN `folderId` INT,
	IN `folderType` VARCHAR(50)
)
BEGIN
SELECT qry.*, COALESCE(lesson.partId, part.id) as partId,
COALESCE(lesson.module,part.module,module.id) as module,
COALESCE(module.moduleName,part.part,lesson.name)  AS `name` 
FROM (SELECT id,folderId,folderType,moduleId,type FROM assignedmodules 
WHERE assignedmodules.isDeleted=0 AND  assignedmodules.folderId=folderId                   
AND assignedmodules.folderType=folderType) AS qry 
LEFT JOIN module ON qry.moduleId = module.id AND qry.type='module' 
LEFT JOIN part ON qry.moduleId = part.id AND qry.type='part' 
LEFT JOIN lesson ON qry.moduleId = lesson.id AND qry.type='lesson';
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getAllExamQuestions
DROP PROCEDURE IF EXISTS `orelltalk_getAllExamQuestions`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getAllExamQuestions`(
	IN `examId` INT
)
BEGIN
SELECT n.id,n.lessonid,n.lessontype,n.quecount, COUNT(exercise.id) AS totalquestions, 
COALESCE(lesson.`name`,qblesson.`name`) AS `lessonname` 
FROM newexamlessons n 
LEFT JOIN lesson ON lesson.id=n.lessonid AND n.lessontype=0  
LEFT JOIN qblesson ON qblesson.id=n.lessonid AND n.lessontype=1 
INNER JOIN exercise ON exercise.lessonId=n.lessonid AND exercise.isDeleted=0  
WHERE n.examid=examId GROUP BY n.lessonid ORDER BY n.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getAssignedExamCount
DROP PROCEDURE IF EXISTS `orelltalk_getAssignedExamCount`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getAssignedExamCount`(
	IN `sId` INT,
	IN `standard` INT,
	IN `StudentId` INT
)
BEGIN
SELECT COUNT(exam.id) AS count
FROM exam 
INNER JOIN assignedexam ON exam.id=assignedexam.examId 
WHERE assignedexam.statusOver=0 AND assignedexam.isDeleted=0 AND assignedexam.examDate >= (
SELECT CURDATE()) 
AND (assignedexam.id NOT IN (
SELECT completedexam.assignedExamid  
FROM completedexam
WHERE completedexam.sid=sId
GROUP BY completedexam.assignedExamid))  
AND (CASE WHEN (COALESCE(assignedexam.classId,0)!=0) THEN assignedexam.classId=standard ELSE assignedexam.studentId=studentId END);
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getAssignedSpeechcraftList
DROP PROCEDURE IF EXISTS `orelltalk_getAssignedSpeechcraftList`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getAssignedSpeechcraftList`(
	IN `today` VARCHAR(50),
	IN `standard` INT
)
BEGIN
SELECT speechcraft.id,speechcraft.name,speechcraftunit.name AS unitName, 
speechcraft.unitId, speechcraftmodule.id AS moduleId, 
speechcraftmodule.name AS moduleName
FROM speechcraft 
INNER JOIN speechcraftassign ON speechcraftassign.speechId=speechcraft.id 
INNER JOIN speechcraftunit ON speechcraftunit.id=speechcraft.unitId 
INNER JOIN speechcraftmodule ON speechcraftmodule.id=speechcraftunit.moduleId 
WHERE speechcraftassign.assigningDate=today
AND speechcraftassign.classId=standard AND speechcraftmodule.isDeleted=0 
AND speechcraftunit.isDeleted=0 AND speechcraft.isDeleted=0
GROUP BY speechcraft.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getAssignedSpeechcraftListbyStudents
DROP PROCEDURE IF EXISTS `orelltalk_getAssignedSpeechcraftListbyStudents`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getAssignedSpeechcraftListbyStudents`(
	IN `today` VARCHAR(50),
	IN `studentId` INT
)
BEGIN
SELECT speechcraft.id,speechcraft.name,speechcraftunit.name AS unitName, 
speechcraft.unitId, speechcraftmodule.id AS moduleId, 
speechcraftmodule.name AS moduleName
FROM speechcraft 
INNER JOIN speechcraftassign ON speechcraftassign.speechId=speechcraft.id 
INNER JOIN speechcraftunit ON speechcraftunit.id=speechcraft.unitId 
INNER JOIN speechcraftmodule ON speechcraftmodule.id=speechcraftunit.moduleId 
WHERE speechcraftassign.assigningDate=today 
AND speechcraftassign.studentId=studentId AND speechcraftmodule.isDeleted=0 
AND speechcraftunit.isDeleted=0 AND speechcraft.isDeleted=0
GROUP BY speechcraft.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getAssignedTeacher
DROP PROCEDURE IF EXISTS `orelltalk_getAssignedTeacher`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getAssignedTeacher`(
	IN `userName` VARCHAR(50)
)
BEGIN
SELECT teacher.id, teacher.name, teacher.userName FROM student 
INNER JOIN classteacher ON student.standard=classteacher.classId AND classteacher.isDeleted=0  
INNER JOIN teacher ON classteacher.teacherId=teacher.id AND teacher.isDeleted=0 
WHERE student.userName=userName AND teacher.id!=1
UNION  
SELECT t.id, t.name, t.userName FROM teacher t WHERE t.userName='admin';
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getBookmarkedLesson
DROP PROCEDURE IF EXISTS `orelltalk_getBookmarkedLesson`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getBookmarkedLesson`()
BEGIN
SELECT lesson.id, lesson.`name`, module.moduleName, 
module.moduleType, lesson.partId, lesson.module, part.part
FROM `lesson` 
INNER JOIN part ON part.id=lesson.partId AND part.isDeleted=0 
INNER JOIN module ON lesson.module=module.id AND module.isDeleted=0
WHERE lesson.isDeleted=0 AND lesson.`isBook`=1  ORDER BY lesson.priority , lesson.createdDate ASC;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getClassExamResultList
DROP PROCEDURE IF EXISTS `orelltalk_getClassExamResultList`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getClassExamResultList`(
	IN `classId` INT,
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50),
	IN `cId` INT,
	IN `fDate` VARCHAR(50),
	IN `tDate` VARCHAR(50)
)
BEGIN
SELECT qry.id,qry.`name`,qry.image, SUM(qry.`count`) AS `count`
FROM (
SELECT student.id,student.`name`,student.image, COUNT(student.id) AS `count`
FROM (
SELECT completedexam.id,completedexam.assignedExamid, completedexam.sid
FROM completedexam
WHERE COALESCE(completedexam.examtype,0)=0
GROUP BY sid,completedexam.assignedExamid) AS tab
JOIN assignedexam ON tab.assignedExamId = assignedexam.id
JOIN student ON student.id=tab.sid
WHERE student.standard=classId AND assignedexam.examDate BETWEEN fromDate AND toDate 
GROUP BY student.id
UNION
SELECT student.id,student.`name`,student.image, COUNT(student.id) AS `count`
FROM (
SELECT completedexam.id,completedexam.assignedExamid, completedexam.sid
FROM completedexam
WHERE completedexam.examtype=1
GROUP BY sid,completedexam.assignedExamid) AS tab
JOIN assignednewexam ON tab.assignedExamId = assignednewexam.id
JOIN student ON student.id=tab.sid 
WHERE student.standard=cId AND assignednewexam.examDate BETWEEN fDate AND tDate 
GROUP BY student.id) AS qry
GROUP BY qry.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getClassExerciseReport
DROP PROCEDURE IF EXISTS `orelltalk_getClassExerciseReport`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getClassExerciseReport`(
	IN `classId` INT,
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50)
)
BEGIN
SELECT COUNT(st.id) AS `count`,st.id,st.image,st.`name`,lesson.id AS lessonId,
lesson.`name` AS lessonName, exercise.id AS exercise, se.id AS submittedexercise,se.createdDate AS `date` ,
 COALESCE(st.division,'') AS division  
FROM student st  
INNER JOIN submittedexercise se ON se.studentId=st.id 
INNER JOIN exercise ON exercise.id=se.exerciseId  
INNER JOIN lesson ON lesson.id=exercise.lessonId 
WHERE st.standard=classId AND (STR_TO_DATE(se.createdDate,'%Y-%m-%d')  
BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y')) 
GROUP BY st.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getClassExerciseReportList
DROP PROCEDURE IF EXISTS `orelltalk_getClassExerciseReportList`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getClassExerciseReportList`(
	IN `classId` INT,
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50),
	IN `cId` INT,
	IN `fDate` VARCHAR(50),
	IN `tDate` VARCHAR(50)
)
BEGIN
SELECT qry.id,qry.`name`,qry.image, SUM(qry.`count`) AS `count` , qry.division
FROM (
SELECT student.id,student.`name`,student.image, COUNT(student.id) AS `count`, 0 AS TYPE ,  COALESCE(student.division,'') AS division
FROM (
SELECT completedexam.id,completedexam.assignedExamid, completedexam.sid
FROM completedexam
WHERE COALESCE(completedexam.examtype,0)=0
GROUP BY sid,completedexam.assignedExamid) AS tab
JOIN assignedexam ON tab.assignedExamId = assignedexam.id AND assignedexam.isDeleted = 0
JOIN student ON student.id=tab.sid
WHERE student.standard=classId AND assignedexam.examDate BETWEEN fromDate AND toDate 
GROUP BY student.id
UNION
SELECT student.id,student.`name`,student.image, COUNT(student.id) AS `count`, 1 AS TYPE,  COALESCE(student.division,'') AS division
FROM (
SELECT completedexam.id,completedexam.assignedExamid, completedexam.sid
FROM completedexam
WHERE completedexam.examtype=1
GROUP BY sid,completedexam.assignedExamid) AS tab
JOIN assignednewexam ON tab.assignedExamId = assignednewexam.id AND assignednewexam.isDeleted = 0
JOIN student ON student.id=tab.sid 
WHERE student.standard=cId AND assignednewexam.examDate BETWEEN fDate AND tDate 
GROUP BY student.id) AS qry
GROUP BY qry.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getExamClassReportTeacherwise
DROP PROCEDURE IF EXISTS `orelltalk_getExamClassReportTeacherwise`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getExamClassReportTeacherwise`(
	IN `userName` VARCHAR(50),
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50),
	IN `uName` VARCHAR(50),
	IN `fDate` VARCHAR(50),
	IN `tDate` VARCHAR(50)
)
BEGIN
SELECT asd.id,asd.`className`,asd.c AS `count`
FROM (
SELECT tc .id,tc.className, COUNT(tc.id) AS c
FROM assignedexam ae 
INNER JOIN student s ON ae.classId=s.standard OR ae.studentId=s.id
INNER JOIN typesclass tc ON tc.id=s.standard
WHERE ae.createdBy=userName
AND ae.createdDate BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y')
GROUP BY s.id
UNION 
SELECT tc.id,tc.`className`, COUNT(tc.id) AS c
FROM assignednewexam ane 
INNER JOIN student s ON ane.classId=s.standard OR ane.studentId=s.id 
INNER JOIN typesclass tc ON tc.id=s.standard
WHERE ane.createdBy=uName
AND ane.createdDate BETWEEN STR_TO_DATE(fDate,'%d/%m/%Y') AND STR_TO_DATE(tDate,'%d/%m/%Y')
GROUP BY s.id) AS asd
GROUP BY asd.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getExamGrade
DROP PROCEDURE IF EXISTS `orelltalk_getExamGrade`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getExamGrade`(
	IN `assignId` INT,
	IN `studentId` INT,
	IN `eType` INT
)
BEGIN
SELECT COUNT(completedexam.id) AS id 
FROM completedexam
JOIN exercise ON completedexam.selectedQuestions=exercise.id 
LEFT JOIN options opt ON opt.exerciseId=exercise.id
WHERE completedexam.assignedExamid=assignId AND completedexam.sid=studentId
AND (CASE WHEN exercise.`type`=3 THEN 
(CASE WHEN opt.opt1=completedexam.selectedAnswers THEN TRUE 
WHEN opt.opt2=completedexam.selectedAnswers THEN  TRUE  
WHEN opt.opt3=completedexam.selectedAnswers THEN TRUE   
WHEN opt.opt4=completedexam.selectedAnswers THEN TRUE 
WHEN opt.opt5=completedexam.selectedAnswers THEN TRUE END )  
ELSE exercise.answer=completedexam.selectedAnswers END ) = TRUE  
AND COALESCE(completedexam.examtype,0)=eType;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getExamlistOnDate
DROP PROCEDURE IF EXISTS `orelltalk_getExamlistOnDate`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getExamlistOnDate`(
	IN `startTime` VARCHAR(50),
	IN `classId` INT
)
BEGIN
SELECT tab.*
FROM(SELECT * 
FROM (SELECT assignednewexam.id,examname AS examName,1 AS examtype, (
SELECT DATE_FORMAT(assignednewexam.examDate,'%d/%m/%Y')) 
AS examDate, assignednewexam.examDate AS examDates, TIME_FORMAT(assignednewexam.createdDate, '%h:%i %p') AS `time`, 
typesclass.id AS classids, assignednewexam.createdDate, assignednewexam.createdBy, assignednewexam.groupTimeStamp 
FROM assignednewexam
INNER JOIN newexam ON newexam.id=assignednewexam.examId
INNER JOIN typesclass ON typesclass.id=assignednewexam.classId 
WHERE assignednewexam.isDeleted=0
UNION 
SELECT assignednewexam.id,examname AS examName,1 AS examtype, (
SELECT DATE_FORMAT(assignednewexam.examDate,'%d/%m/%Y'))
AS examDate, assignednewexam.examDate AS examDates, TIME_FORMAT(assignednewexam.createdDate, '%h:%i %p') AS `time`, 
typesclass.id AS classids, assignednewexam.createdDate, assignednewexam.createdBy, assignednewexam.groupTimeStamp
FROM assignednewexam
INNER JOIN newexam ON newexam.id=assignednewexam.examId
INNER JOIN student ON student.id = assignednewexam.studentId
INNER JOIN typesclass ON typesclass.id= student.standard
WHERE assignednewexam.isDeleted=0) AS tab1
GROUP BY tab1.groupTimeStamp  
UNION 
SELECT * FROM ( 
SELECT assignedexam.id,examName,0 AS examtype, (
SELECT DATE_FORMAT(assignedexam.examDate,'%d/%m/%Y')) AS examDate,
assignedexam.examDate AS examDates, (TIME_FORMAT(assignedexam.createdDate, '%h:%i %p')) AS `time`, 
typesclass.id AS classids, assignedexam.createdDate, assignedexam.createdBy, assignedexam.groupTimeStamp 
FROM assignedexam
INNER JOIN exam ON exam.id=assignedexam.examId
INNER JOIN typesclass ON typesclass.id=assignedexam.classId 
WHERE assignedexam.isDeleted=0 
UNION 
SELECT assignedexam.id,examName,0 AS examtype, (
SELECT DATE_FORMAT(assignedexam.examDate,'%d/%m/%Y')) AS examDate , 
assignedexam.examDate AS examDates, (TIME_FORMAT(assignedexam.createdDate, '%h:%i %p')) AS `time`,  
typesclass.id AS classids, assignedexam.createdDate, assignedexam.createdBy, assignedexam.groupTimeStamp 
FROM assignedexam 
INNER JOIN exam ON exam.id=assignedexam.examId
INNER JOIN student ON student.id = assignedexam.studentId
INNER JOIN typesclass  ON typesclass.id= student.standard 
WHERE assignedexam.isDeleted=0) AS tab2 
GROUP BY tab2.groupTimeStamp 
) AS tab
WHERE DATE_FORMAT(STR_TO_DATE(tab.examDates,'%Y-%m-%d'), '%Y-%m-%d') = DATE_FORMAT(STR_TO_DATE(startTime,'%d/%m/%Y'), '%Y-%m-%d')
AND tab.classids = classId;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getExamStudentReportsTeacherwise
DROP PROCEDURE IF EXISTS `orelltalk_getExamStudentReportsTeacherwise`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getExamStudentReportsTeacherwise`(
	IN `classId` INT,
	IN `userName` VARCHAR(50),
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50),
	IN `cId` INT,
	IN `uName` VARCHAR(50),
	IN `fDate` VARCHAR(50),
	IN `tDate` VARCHAR(50)
)
BEGIN
SELECT asd.id,asd.`name`,asd.image, SUM(asd.c) AS `count`, asd.division
FROM (
SELECT s.id,s.`name`,s.image, COUNT(s.id) AS c , COALESCE(s.division,'') AS division
FROM student s
INNER JOIN assignedexam ae ON ae.classId=s.standard OR ae.studentId=s.id
WHERE s.isDeleted=0 AND s.standard=classId AND ae.createdBy=userName 
AND ae.createdDate BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y')
GROUP BY s.id
UNION
SELECT s.id,s.`name`,s.image, COUNT(s.id) AS c , COALESCE(s.division,'') AS division
FROM student s
INNER JOIN assignednewexam ane ON ane.classId=s.standard OR ane.studentId=s.id 
WHERE s.isDeleted=0 AND s.standard=cId AND ane.createdBy=uName 
AND ane.createdDate BETWEEN STR_TO_DATE(fDate,'%d/%m/%Y') AND STR_TO_DATE(tDate,'%d/%m/%Y')
GROUP BY s.id) AS asd
GROUP BY asd.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getExamwiseStudentList
DROP PROCEDURE IF EXISTS `orelltalk_getExamwiseStudentList`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getExamwiseStudentList`(
	IN `fromDate` VARCHAR(50),
	IN `classId` INT,
	IN `examId` INT,
	IN `fDate` VARCHAR(50),
	IN `cId` INT,
	IN `eId` INT
)
BEGIN
SELECT * FROM (SELECT student.id,student.`name`,student.image,assignedexam.id AS times, 
COUNT(complete.sid) AS `count`,'0' AS examtype  FROM (SELECT * FROM completedexam  
GROUP BY completedexam.assignedExamid,completedexam.sid) AS complete  
INNER JOIN assignedexam ON assignedexam.id=complete.assignedExamId 
INNER JOIN exam ON exam.id=assignedexam.examId 
INNER JOIN student ON student.id=complete.sid 
WHERE assignedexam.examDate=fromDate AND student.standard=classId AND assignedexam.examId=examId 
UNION
SELECT student.id,student.`name`,student.image,assignednewexam.id AS times, COUNT(complete.sid) AS `count`,  
'0' AS examtype  FROM (SELECT * FROM completedexam 
GROUP BY completedexam.assignedExamid,completedexam.sid) AS complete
INNER JOIN assignednewexam ON assignednewexam.id=complete.assignedExamId 
INNER JOIN exam ON exam.id=assignednewexam.examId 
INNER JOIN student ON student.id=complete.sid  
WHERE assignednewexam.examDate=fDate AND student.standard=cId AND assignednewexam.examId=eId) 
AS qry WHERE qry.id!=0 GROUP BY qry.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getExercise
DROP PROCEDURE IF EXISTS `orelltalk_getExercise`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getExercise`(
	IN `lessonId` VARCHAR(50)
)
BEGIN
SELECT exercise.lessonId, exercise.id, exercise.question, exercise.answer, 
exercise.type, coalesce(options.opt1,'') as opt1, coalesce(options.opt2,'') as opt2, 
coalesce(options.opt3,'') as opt3, coalesce(options.opt4,'') as opt4, 
coalesce(options.opt5,'') as opt5,
options.opt1File, options.opt2File, options.opt3File,                                                 
options.opt4File, options.opt5File 
FROM exercise 
left join options on options.exerciseId=exercise.id 
WHERE exercise.isDeleted=0 AND exercise.lessonId=lessonId;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getExerciseQuestions
DROP PROCEDURE IF EXISTS `orelltalk_getExerciseQuestions`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getExerciseQuestions`(
	IN `lessonId` VARCHAR(50)
)
BEGIN
SELECT exercise.type,exercise.answer,exercise.question,exercise.id, 
exercise.file,options.opt1,options.opt2,options.opt3,options.opt4,options.opt5, 
options.opt1File,options.opt2File,options.opt3File,options.opt4File,options.opt5File,
exercise.mark,(SELECT GROUP_CONCAT(optionskeyword.keyword SEPARATOR '~') 
FROM  optionskeyword 
WHERE exerciseId=exercise.id  AND optionskeyword.isDeleted =0) AS descriptive 
FROM exercise 
LEFT JOIN options ON exercise.id=options.exerciseId WHERE exercise.isDeleted=0 && exercise.lessonId=lessonId;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getFolderLessonsForOnlineHomework
DROP PROCEDURE IF EXISTS `orelltalk_getFolderLessonsForOnlineHomework`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getFolderLessonsForOnlineHomework`(
	IN `studentId` INT


)
BEGIN
select student.standard into @classid from student where student.id=studentId;


select student.groupId into @groupid from student where student.id=studentId;

SELECT DISTINCT qry.name, qry.moduleId,qry.id,qry.partId, COALESCE (qry.f4name,'') AS folder4, 
COALESCE (qry.f3name,f31.name) AS moduleName , f21.name AS secondFolderName, f11.`name` AS folderName
FROM (SELECT h.folderNo, h.assignedTo,h.isDeleted, h.assignedToId,h.endDate, 
lesson.`name` as `name`, lesson.id , lesson.module as moduleId, 
lesson.partId , f3.secondfolderId AS f2id, f3.name AS f3name, COALESCE (f3.id, f4.thirdfolderId) AS f3id,
f4.name AS f4name, f4.id AS f4id FROM onlinehomework h
LEFT JOIN thirdfolder f3 ON f3.id=h.moduleTypeId AND h.folderNo='third' AND f3.isDeleted=0
LEFT JOIN fourthfolder f4 ON case when h.folderNo='fourth' then f4.id=h.moduleTypeId
ELSE f4.thirdFolderId=f3.id END AND f4.isDeleted=0
LEFT JOIN assignedmodules am ON case when f4.id!=""then am.folderId=f4.id AND
am.folderType='fourth' ELSE am.folderId=f3.id AND am.folderType='third' END AND am.isDeleted=0
LEFT JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson' AND lesson.isDeleted=0
WHERE h.isDeleted=0) AS qry
LEFT JOIN thirdfolder f31 ON qry.f3id=f31.id AND folderNo='fourth' AND f31.isDeleted=0
LEFT JOIN secondfolder f21 ON case when qry.folderNo='third' then qry.f2id=f21.id
ELSE f21.id=f31.secondFolderId END AND f21.isDeleted=0
LEFT JOIN folder f11 ON f11.id=f21.folderId AND f11.isDeleted=0
WHERE  case when qry.assignedTo='class' then qry.assignedToId=@classid  AND qry.isDeleted=0
when qry.assignedTo='student' then qry.assignedToId=studentId
ELSE qry.assignedToId=@groupid END 
and (select DATE_FORMAT((select CURDATE()), '%d/%m/%Y'))  <= qry.endDate
UNION 
SELECT DISTINCT qry.name, qry.moduleId,qry.id,qry.partId, COALESCE (qry.f4name,'') AS folder4, 
COALESCE (qry.f3name,f31.name) AS moduleName, f21.name AS secondFolderName, f11.`name` AS folderName
FROM (SELECT h.folderNo, h.assignedTo,h.isDeleted,h.endDate, h.assignedToId, 
lesson.`name` as name, lesson.id , lesson.module as moduleId, 
lesson.partId , f3.secondfolderId AS f2id, f3.NAME AS f3name, COALESCE (f3.id, f4.thirdfolderId) AS f3id,
f4.NAME AS f4name, f4.id AS f4id FROM onlinehomework h
LEFT JOIN assignedmodules am ON h.moduleType='lessonInFolder' and am.id=h.moduleTypeId AND h.folderNo='fourth' AND  am.isDeleted=0
LEFT JOIN thirdfolder f3 ON f3.id=am.folderId AND am.folderType='third' AND f3.isDeleted=0
LEFT JOIN fourthfolder f4 ON case when am.folderType='fourth' then f4.id=am.folderId
ELSE f4.thirdFolderId=f3.id END AND f4.isDeleted=0
LEFT JOIN lesson ON lesson.id=am.moduleId AND am.`type`='lesson' AND lesson.isDeleted=0
WHERE h.isDeleted=0) AS qry
LEFT JOIN thirdfolder f31 ON qry.f3id=f31.id AND folderNo='fourth' AND f31.isDeleted=0
LEFT JOIN secondfolder f21 ON case when qry.folderNo='third' then qry.f2id=f21.id
ELSE f21.id=f31.secondFolderId END AND f21.isDeleted=0
LEFT JOIN folder f11 ON f11.id=f21.folderId AND f11.isDeleted=0
WHERE  case when qry.assignedTo='class' then qry.assignedToId=@classid  AND qry.isDeleted=0
when qry.assignedTo='student' then qry.assignedToId=studentId
ELSE qry.assignedToId=@groupid END 
and (select DATE_FORMAT((select CURDATE()), '%d/%m/%Y'))  <= qry.endDate;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getNewexamTimeQuestions
DROP PROCEDURE IF EXISTS `orelltalk_getNewexamTimeQuestions`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getNewexamTimeQuestions`(
	IN `examId` INT,
	IN `qMark` INT
)
BEGIN

SELECT tab.* FROM  ( SELECT DISTINCT exercise.id,exercise.question,exercise.`type`,exercise.`file`,exercise.mark, 
`options`.opt1,`options`.opt2,`options`.opt3,`options`.opt4,`options`.opt5,
`options`.opt1File,`options`.opt2File,`options`.opt3File,`options`.opt4File,`options`.opt5File, 
CONCAT(l.lessonid,'~',l.quecount) AS lessonId FROM newexamlessons l
INNER JOIN exercise ON exercise.lessonId=l.lessonid AND exercise.isDeleted=0
INNER JOIN `options` ON `options`.exerciseId=exercise.id AND options.isDeleted=0
INNER JOIN assignednewexam a ON l.examid=a.examId AND a.isDeleted=0
INNER JOIN newexam n ON n.id=a.examId AND n.isDeleted=0
WHERE l.isDeleted=0 AND a.id=examId AND exercise.`type`=1 AND FIND_IN_SET (exercise.`type`,n.`type`) 
UNION 
SELECT DISTINCT exercise.id,exercise.question,exercise.`type`,exercise.`file`,exercise.mark,
"" AS opt1,"" AS opt2,"" AS opt3,"" AS opt4,"" AS opt5,
"" AS opt1File,"" AS opt2File,"" AS opt3File,"" AS opt4File,"" AS opt5File,
CONCAT(l.lessonid,'~',l.quecount) AS lessonId FROM newexamlessons l
INNER JOIN exercise ON exercise.lessonId=l.lessonid AND exercise.isDeleted=0
INNER JOIN assignednewexam a ON l.examid=a.examId AND a.isDeleted=0
INNER JOIN newexam n ON n.id=a.examId AND n.isDeleted=0
WHERE l.isDeleted=0 AND a.id=examId AND exercise.`type`!=1 
AND FIND_IN_SET (exercise.`type`,n.`type`) ) AS tab ORDER BY RAND() LIMIT qMark;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getParentDetails
DROP PROCEDURE IF EXISTS `orelltalk_getParentDetails`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getParentDetails`()
BEGIN
SELECT parent.id, parent.name, parent.studentId, 
parent.userName, parent.mobile, student.name AS student 
FROM parent  
INNER JOIN student ON parent.studentId=student.id                              
AND student.isDeleted=0 
INNER JOIN typesclass ON typesclass.id = student.standard  
AND typesclass.isDeleted=0  
WHERE parent.isDeleted=0;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getPartDetails
DROP PROCEDURE IF EXISTS `orelltalk_getPartDetails`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getPartDetails`(
	IN `standard` INT,
	IN `studentId` INT,
	IN `today` VARCHAR(50)
)
BEGIN
SELECT lesson.name,module.moduleName,part.id, part.part, part.module, 
assignlesson.lessonId, assignlesson.isTextContent 
FROM lesson 
INNER JOIN module ON module.id=lesson.module 
INNER JOIN part ON lesson.partId=part.id 
INNER JOIN assignlesson ON assignlesson.lessonId=lesson.id 
WHERE lesson.isDeleted=0 AND module.isDeleted=0 AND (CASE WHEN (COALESCE(assignlesson.classId,0)!=0)  
THEN assignlesson.classId=standard  ELSE assignlesson.studentId=studentId END)  
AND today BETWEEN assignlesson.assigningDate AND assignlesson.endDate
GROUP BY part.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getPartListforStudents
DROP PROCEDURE IF EXISTS `orelltalk_getPartListforStudents`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getPartListforStudents`(
	IN `moduleId` INT,
	IN `standard` INT,
	IN `studentId` INT,
	IN `aDate` VARCHAR(50)
)
BEGIN
SELECT COALESCE(p.id,part.id) AS id, COALESCE(part.part,p.part) AS part
FROM assignlesson 
LEFT JOIN lesson ON assignlesson.lessonId=lesson.id AND lesson.isDeleted=0
LEFT JOIN part ON assignlesson.lessonId=part.id AND part.isDeleted=0
LEFT JOIN part AS p ON COALESCE(lesson.partId,part.id)=p.id AND p.isDeleted=0 
LEFT JOIN  module ON COALESCE(p.module,part.module)=module.id
WHERE module.id=moduleId AND assignlesson.isDeleted=0 AND (CASE WHEN (COALESCE(assignlesson.classId,0)!=0) 
THEN assignlesson.classId=standard ELSE assignlesson.studentId=studentId END) 
AND STR_TO_DATE(aDate,'%d/%m/%Y') BETWEEN STR_TO_DATE(assignlesson.assigningDate,'%d/%m/%Y')  
AND STR_TO_DATE(COALESCE(assignlesson.endDate,assignlesson.assigningDate),'%d/%m/%Y') 
GROUP BY COALESCE(p.id,part.id);
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getSingleAssignedExamDetails
DROP PROCEDURE IF EXISTS `orelltalk_getSingleAssignedExamDetails`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getSingleAssignedExamDetails`(
	IN `examId` INT
)
BEGIN
SELECT assignedexam.id, assignedexam.classId, assignedexam.endTime, 
assignedexam.examDate, assignedexam.examId, assignedexam.startTime, 
assignedexam.studentId, assignedexam.groupTimeStamp, exam.examName, 
typesclass.id AS classId,typesclass.className
FROM assignedexam 
INNER JOIN exam ON exam.id=assignedexam.examId 
INNER JOIN typesclass ON typesclass.id=assignedexam.classId 
WHERE assignedexam.isDeleted=0 AND statusOver=0 AND assignedexam.id=examId;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getSpeechaceLessonList
DROP PROCEDURE IF EXISTS `orelltalk_getSpeechaceLessonList`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getSpeechaceLessonList`(
	IN `username` VARCHAR(50),
	IN `currentDate` VARCHAR(50)
)
BEGIN

SELECT student.id,standard,groupId INTO @id,@cid,@gid 
	FROM student WHERE student.isDeleted=0 AND student.userName=username;

SELECT DISTINCT l.id,l.description,l.lessonName,m.`name` AS moduleName,m.id AS moduleid 
FROM speechlessonassign a
INNER JOIN speechlesson l ON l.id=a.lessonId AND l.isDeleted=0
INNER JOIN speechmodule m ON m.id=l.moduleid AND m.isDeleted=0
WHERE a.isDeleted=0 AND (CASE WHEN a.assignedType='classwise' THEN assignedTo=@cid
	WHEN a.assignedType='studentwise' THEN assignedTo=@id
	WHEN a.assignedType='groupwise' THEN assignedTo=@gid END)
AND (STR_TO_DATE(currentDate,'%d/%m/%Y') BETWEEN a.assigningDate AND a.endDate)
GROUP BY a.lessonId ORDER BY a.createdDate;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getSpeechcraftList
DROP PROCEDURE IF EXISTS `orelltalk_getSpeechcraftList`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getSpeechcraftList`(
	IN `today` VARCHAR(50),
	IN `standard` INT,
	IN `studentId` INT
)
BEGIN
SELECT speechcraft.id,speechcraft.name,speechcraftunit.name AS unitName, 
speechcraft.unitId, speechcraftmodule.id AS moduleId,
speechcraftmodule.name AS moduleName 
FROM speechcraft 
INNER JOIN speechcraftassign ON speechcraftassign.speechId=speechcraft.id 
INNER JOIN speechcraftunit ON speechcraftunit.id=speechcraft.unitId 
INNER JOIN speechcraftmodule ON  speechcraftmodule.id=speechcraftunit.moduleId 
WHERE speechcraftassign.assigningDate=today 
AND (CASE WHEN (COALESCE(speechcraftassign.classId,0)!=0) THEN speechcraftassign.classId=standard  
ELSE speechcraftassign.studentId=studentId END) AND speechcraftmodule.isDeleted=0 
AND speechcraftunit.isDeleted=0 AND speechcraft.isDeleted=0 GROUP By speechcraft.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getStudent
DROP PROCEDURE IF EXISTS `orelltalk_getStudent`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getStudent`(
	IN `userName` VARCHAR(50)
)
BEGIN
SELECT student.id, student.standard, student.name, 
student.userName, student.mailid, student.mobile, student.address, student.groupId,
student.image, student.guardianName, typesclass.className, admusrmst.isActive
FROM student
INNER JOIN typesclass ON student.standard=typesclass.id
INNER JOIN admusrmst ON admusrmst.USRCOD=student.userName
WHERE student.isDeleted=0 AND typesclass.isDeleted=0 AND student.userName=userName;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getStudentActivityView
DROP PROCEDURE IF EXISTS `orelltalk_getStudentActivityView`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getStudentActivityView`(
	IN `activityid` INT
)
BEGIN

SELECT studentactivity.voice, (CASE WHEN studentactivity.lessonType=3  THEN 'ROLEPLAY' 
WHEN studentactivity.lessonType='0' THEN 'MIXED AUDIO'    
WHEN studentactivity.lessonType!='' AND studentactivity.lessonType!=2 
THEN studentactivity.lessonType ELSE 'REPEAT' END) as lessonType ,studentactivity.studCMS,
studentactivity.feedback,DATE_FORMAT(studentactivity.createdDate,'%d/%m/%Y') AS date,
module.moduleName,lesson.id,lesson.name,
(CASE WHEN studentactivity.lessonType='CMS' THEN lesson.listenFile 
WHEN studentactivity.lessonType=3  THEN  lesson.roleplayFile 
ELSE (CASE WHEN lesson.repeatFile='' THEN lesson.listenFile ELSE lesson.repeatFile END)	 
END) as listenFile,
(CASE WHEN studentactivity.lessonType!=3 THEN lesson.repeatFilePdf
ELSE lesson.roleplayFilePdf END) as listenFilePdf,student.id AS studentId,student.standard
FROM studentactivity JOIN lesson ON lesson.id=studentactivity.lessonId 
INNER JOIN student ON student.userName=studentactivity.studentId AND student.isDeleted=0
JOIN module ON module.id=lesson.module WHERE studentactivity.id=activityid;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getStudentDetails
DROP PROCEDURE IF EXISTS `orelltalk_getStudentDetails`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getStudentDetails`(
	IN `classId` INT
)
BEGIN
SELECT student.id, student.name,student.guardianName, 
student.mobile, student.groupId, student.image,student.mailid, 
typesclass.className,student.userName, admusrmst.isActive
FROM student
INNER JOIN typesclass ON student.standard=typesclass.id
INNER JOIN admusrmst ON admusrmst.USRCOD=student.userName
WHERE student.isDeleted=0 AND typesclass.isDeleted=0 
AND student.standard=classId AND admusrmst.isActive=1;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getStudentExamListbyId
DROP PROCEDURE IF EXISTS `orelltalk_getStudentExamListbyId`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getStudentExamListbyId`(
	IN `studentId` INT,
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50),
	IN `sId` INT,
	IN `fDate` VARCHAR(50),
	IN `tDate` VARCHAR(50)
)
BEGIN
 

SELECT  assignedexam.id AS id,DATE_FORMAT(assignedexam.examDate,'%d/%m/%Y') AS examDate,exam.examName,
assignedexam.startTime, assignedexam.endTime,mark.mark ,mark.grade,   
COALESCE(mark.examtype,0) AS examtype  , exam.totalMark , exam.totalMark  AS `count` FROM mark 
JOIN assignedexam ON assignedexam.id = mark.assignedExamId AND assignedexam.isDeleted = 0
 AND COALESCE(mark.examType,0) = 0
JOIN exam ON exam.id = assignedexam.examId  AND exam.isDeleted = 0 
WHERE mark.sid = sId AND assignedexam.examDate BETWEEN fDate AND tDate GROUP BY assignedexam.id

UNION

SELECT assignednewexam.id AS id,DATE_FORMAT(assignednewexam.examDate,'%d/%m/%Y') AS examDate,
newexam.examName,assignednewexam.startTime, assignednewexam.endTime,mark.mark ,mark.grade,   
COALESCE(mark.examtype,1) AS examtype , newexam.totalmark , newexam.totalmark AS `count` FROM mark 
JOIN assignednewexam ON assignednewexam.id = mark.assignedExamId AND assignednewexam.isDeleted = 0
 AND COALESCE(mark.examType,1) = 1
JOIN newexam ON newexam.id = assignednewexam.examId  AND newexam.isDeleted = 0 
WHERE mark.sid = sId AND assignednewexam.examDate BETWEEN fDate AND tDate ;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getStudentExamResults
DROP PROCEDURE IF EXISTS `orelltalk_getStudentExamResults`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getStudentExamResults`(
	IN `studentId` INT,
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50)
)
BEGIN
select * from ( SELECT assignedexam.id,assignedexam.examDate,exam.examName,exam.totalHour,exam.totalMinute,
exam.totalMark,mark.mark,mark.grade ,  
exam.passMark , ( case when ( mark.mark >= exam.passMark ) THEN 'Pass' ELSE 'FAIL' END) AS examStatus 
FROM exampublishstatus JOIN mark ON mark.assignedExamId = exampublishstatus.assignedId 
AND mark.examType = exampublishstatus.examType 
JOIN assignedexam ON assignedexam.id = mark.assignedExamId  
JOIN exam ON exam.id = assignedexam.examId 
WHERE mark.examType = 0 AND mark.sid =studentId AND exam.isDeleted = 0 AND assignedexam.isDeleted = 0 
AND (DATE_FORMAT(STR_TO_DATE(assignedexam.examDate,'%Y-%m-%d'),'%Y-%m-%d' ) BETWEEN 
DATE_FORMAT(STR_TO_DATE(fromDate,'%d/%m/%Y'), '%Y-%m-%d') and DATE_FORMAT(STR_TO_DATE(toDate,'%d/%m/%Y'),'%Y-%m-%d') ) 
UNION 
SELECT  assignednewexam.id,assignednewexam.examDate,newexam.examName,newexam.totalHour,newexam.totalMinute, 
newexam.totalMark,mark.mark,mark.grade , newexam.passMark , ( case when ( mark.mark >= newexam.passMark ) 
THEN 'Pass' ELSE 'FAIL' END) AS examStatus  FROM exampublishstatus JOIN mark ON 
mark.assignedExamId = exampublishstatus.assignedId AND mark.examType = exampublishstatus.examType 
JOIN assignednewexam ON assignednewexam.id = mark.assignedExamId 
JOIN newexam ON newexam.id = assignednewexam.examId 
WHERE mark.examType = 1 AND mark.sid =studentId AND newexam.isDeleted = 0 AND assignednewexam.isDeleted = 0 
AND (DATE_FORMAT(STR_TO_DATE(assignednewexam.examDate,'%Y-%m-%d'),'%Y-%m-%d' ) 
BETWEEN  DATE_FORMAT(STR_TO_DATE(fromDate,'%d/%m/%Y'), '%Y-%m-%d') and 
DATE_FORMAT(STR_TO_DATE(toDate,'%d/%m/%Y'),'%Y-%m-%d') ) 
) as tab  ORDER BY tab.examDate desc;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getStudentExams
DROP PROCEDURE IF EXISTS `orelltalk_getStudentExams`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getStudentExams`(
	IN `sId` INT,
	IN `classId` INT,
	IN `studentId` INT
)
BEGIN
SELECT exam.id, exam.examName, exam.selectedQuestions, 
exam.totalHour, exam.totalMark, exam.totalMinute, 
exam.passMark, assignedexam.startTime, assignedexam.endTime, 
assignedexam.id AS aid,assignedexam.examDate  
FROM exam 
INNER JOIN assignedexam ON exam.id=assignedexam.examId
WHERE  assignedexam.statusOver=0 AND  assignedexam.isDeleted=0 
AND assignedexam.examDate >= (SELECT curdate()) 
AND  (assignedexam.id NOT IN (SELECT completedexam.assignedExamid 
FROM completedexam 
WHERE completedexam.sid=sId GROUP by completedexam.assignedExamid))
AND (CASE WHEN (COALESCE(assignedexam.classId,0)!=0) THEN assignedexam.classId=classId 
ELSE assignedexam.studentId=studentId END);
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getStudentGroupReports
DROP PROCEDURE IF EXISTS `orelltalk_getStudentGroupReports`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getStudentGroupReports`(
	IN `classId` INT,
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50)
)
BEGIN
SELECT COUNT(st.id) AS `count`,groups.id,groups.`name`,lesson.id AS lessonId, 
exercise.id AS exercise, se.id AS submittedexercise,se.createdDate AS `date`
FROM student st 
INNER JOIN submittedexercise se ON se.studentId=st.id 
INNER JOIN exercise ON exercise.id=se.exerciseId 
INNER JOIN lesson ON lesson.id=exercise.lessonId 
INNER JOIN groups ON groups.id=st.groupId
WHERE groups.standard=classId AND (STR_TO_DATE(se.createdDate,'%Y-%m-%d') 
BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y')) 
GROUP BY groups.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getStudentId
DROP PROCEDURE IF EXISTS `orelltalk_getStudentId`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getStudentId`(
	IN `username` VARCHAR(50)
)
BEGIN
SELECT parent.id, parent.name, parent.studentId, 
parent.userName, parent.mobile, parent.mailid, 
student.address, student.image, student.name AS student, 
COALESCE(typesclass.className,'') AS className 
FROM parent 
INNER JOIN student ON parent.studentId=student.id 
LEFT JOIN typesclass ON (student.standard=typesclass.id  AND typesclass.isDeleted=0)
WHERE parent.userName=username;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getStudentlistforAllStudents
DROP PROCEDURE IF EXISTS `orelltalk_getStudentlistforAllStudents`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getStudentlistforAllStudents`()
BEGIN
SELECT student.id, student.guardianName, student.standard, 
student.mobile, student.address,student.name, student.image, 
student.groupId,student.mailid, admusrmst.isActive 
FROM student 
INNER JOIN admusrmst ON admusrmst.USRCOD=student.userName 
WHERE student.isDeleted=0 AND admusrmst.isActive=1;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getStudentResult
DROP PROCEDURE IF EXISTS `orelltalk_getStudentResult`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getStudentResult`(
	IN `studentId` INT,
	IN `lessonId` VARCHAR(50)
)
BEGIN
SELECT exercise.type,exercise.answer,exercise.question,exercise.id, 
exercise.file,options.opt1,options.opt2,options.opt3,options.opt4,options.opt5, 
options.opt1File,options.opt2File,options.opt3File,options.opt4File,options.opt5File,
exercise.mark,(SELECT GROUP_CONCAT(optionskeyword.keyword SEPARATOR '~') 
FROM  optionskeyword 
WHERE exerciseId=exercise.id  AND optionskeyword.isDeleted =0) AS descriptive 
FROM exercise 
LEFT JOIN options ON exercise.id=options.exerciseId WHERE exercise.isDeleted=0 && exercise.lessonId=lessonId;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getTeacher
DROP PROCEDURE IF EXISTS `orelltalk_getTeacher`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getTeacher`(
	IN `userName` VARCHAR(50)
)
BEGIN
SELECT teacher.id,teacher.`name`, teacher.userName, teacher.image,
teacher.address, teacher.email, teacher.mobile,
admusrmst.isActive,admusrmst.USRDEFROL as role, 
GROUP_CONCAT(DISTINCT ' ',c.id ORDER BY c.id) AS assignedClass, 
GROUP_CONCAT(DISTINCT ' ',c.className ORDER BY c.id) AS assignedClassName FROM teacher  
JOIN admusrmst ON teacher.userName=admusrmst.USRCOD  
LEFT JOIN classteacher ct ON teacher.id=ct.teacherId AND ct.isDeleted=0 
LEFT JOIN typesclass c ON ct.classId=c.id AND c.isDeleted=0 
WHERE teacher.isDeleted=0 AND teacher.userName=userName;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getTeacherDetailsList
DROP PROCEDURE IF EXISTS `orelltalk_getTeacherDetailsList`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getTeacherDetailsList`()
BEGIN

SELECT teacher.id,teacher.email,teacher.mobile,teacher.`name`,teacher.image,teacher.userName, 
teacher.address, admusrmst.isActive, admusrmst.USRDEFROL AS role, admusrmst.isActive,admusrmst.USRDEFROL as role, 
GROUP_CONCAT(DISTINCT ' ',c.className ORDER BY c.id) AS assignedClass FROM teacher  
JOIN admusrmst ON teacher.userName=admusrmst.USRCOD  
LEFT JOIN classteacher ct ON teacher.id=ct.teacherId AND ct.isDeleted=0 
LEFT JOIN typesclass c ON ct.classId=c.id AND c.isDeleted=0 
WHERE teacher.isDeleted=0 
GROUP BY teacher.id ORDER BY teacher.id;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_getVotingFolderStudent
DROP PROCEDURE IF EXISTS `orelltalk_getVotingFolderStudent`;
DELIMITER //
CREATE PROCEDURE `orelltalk_getVotingFolderStudent`(
	IN `studentId` INT,
	IN `groupId` INT,
	IN `standard` INT,
	IN `today` VARCHAR(50)
)
BEGIN
SELECT a.id,f.folder FROM votingassign a
INNER JOIN votingfolder f ON f.id=a.folderid AND f.isDeleted=0
WHERE a.isDeleted=0 AND (CASE WHEN a.`type`='student' THEN a.assignto=studentId 
WHEN a.`type`='group' THEN a.assignto=groupId WHEN a.`type`='class' THEN a.assignto=standard END)
AND STR_TO_DATE(today,'%d/%m/%Y') BETWEEN STR_TO_DATE(a.fromdate,'%d/%m/%Y') 
AND STR_TO_DATE(a.todate,'%d/%m/%Y') AND a.id NOT IN 
(SELECT v.assignid FROM votingsubmission v WHERE isDeleted=0 GROUP BY v.assignid);
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_reports_generateExamReport
DROP PROCEDURE IF EXISTS `orelltalk_reports_generateExamReport`;
DELIMITER //
CREATE PROCEDURE `orelltalk_reports_generateExamReport`(
	IN `standard` INT
)
BEGIN

SELECT student.`name`, student.id AS studentId,tabmark.*, student.username AS userName
FROM student
LEFT JOIN (
SELECT tab.*,mark.mark,mark.sid
FROM (
SELECT tabb1.*
FROM (
SELECT tab1.*
FROM (
SELECT assignednewexam.id,newexam.examname AS examName,1 AS examtype, (
SELECT DATE_FORMAT(assignednewexam.examDate,'%d/%m/%Y')) AS examDate, assignednewexam.examDate AS examDates, TIME_FORMAT(assignednewexam.createdDate, '%h:%i %p') AS `time`, typesclass.id AS classids, assignednewexam.groupTimeStamp, newexam.totalmark AS totalMark,newexam.totalminute
FROM assignednewexam
JOIN newexam ON newexam.id=assignednewexam.examId
JOIN typesclass ON typesclass.id=assignednewexam.classId
WHERE assignednewexam.isDeleted=0 UNION
SELECT assignednewexam.id,newexam.examname AS examName,1 AS examtype, (
SELECT DATE_FORMAT(assignednewexam.examDate,'%d/%m/%Y')) AS examDate, assignednewexam.examDate AS examDates, TIME_FORMAT(assignednewexam.createdDate, '%h:%i %p') AS `time`, typesclass.id AS classids, assignednewexam.groupTimeStamp, newexam.totalmark AS totalMark,newexam.totalminute
FROM assignednewexam
JOIN newexam ON newexam.id=assignednewexam.examId
JOIN student ON student.id = assignednewexam.studentId
JOIN typesclass ON typesclass.id= student.standard
WHERE assignednewexam.isDeleted=0) AS tab1) AS tabb1
 
UNION

SELECT tabb2.*
FROM (
SELECT tab2.*
FROM (
SELECT assignedexam.id, exam.examName,0 AS examtype, (
SELECT DATE_FORMAT(assignedexam.examDate,'%d/%m/%Y')) AS examDate, assignedexam.examDate AS examDates, (TIME_FORMAT(assignedexam.createdDate, '%h:%i %p')) AS `time`, typesclass.id AS classids, assignedexam.groupTimeStamp, exam.totalmark AS totalMark, exam.totalminute
FROM assignedexam
JOIN exam ON exam.id=assignedexam.examId
JOIN typesclass ON typesclass.id=assignedexam.classId
WHERE assignedexam.isDeleted=0 UNION
SELECT assignedexam.id,examName,0 AS examtype, (
SELECT DATE_FORMAT(assignedexam.examDate,'%d/%m/%Y')) AS examDate, assignedexam.examDate AS examDates, (TIME_FORMAT(assignedexam.createdDate, '%h:%i %p')) AS `time`,typesclass.id AS classids, assignedexam.groupTimeStamp, exam.totalmark AS totalMark, exam.totalminute
FROM assignedexam
JOIN exam ON exam.id=assignedexam.examId
JOIN student ON student.id = assignedexam.studentId
JOIN typesclass ON typesclass.id= student.standard
WHERE assignedexam.isDeleted=0) AS tab2) AS tabb2
WHERE tabb2.groupTimeStamp) AS tab
JOIN mark ON mark.assignedExamId = tab.id AND mark.examType = tab.examtype) AS tabmark ON student.id = tabmark.sid
JOIN typesclass ON typesclass.id = student.standard
WHERE student.standard = standard;


END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_reports_student_classWiseAttendance
DROP PROCEDURE IF EXISTS `orelltalk_reports_student_classWiseAttendance`;
DELIMITER //
CREATE PROCEDURE `orelltalk_reports_student_classWiseAttendance`(
	IN `classid` INT,
	IN `fromDate` VARCHAR(20),
	IN `toDate` VARCHAR(20)
)
BEGIN

SELECT student.id,student.name,student.image, nologin.logins, nodays.days, 
totalduration.times as times, attendance.username 
FROM student 
INNER JOIN attendance on (student.userName = attendance.username ) 
INNER JOIN 
   (SELECT COUNT(id) as logins, attendance.username 
   FROM attendance WHERE attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') 
	AND STR_TO_DATE(toDate,'%d/%m/%Y') 
	GROUP BY attendance.username
	) AS nologin ON (nologin.username = attendance.username) 
INNER JOIN 
   (SELECT COUNT(DISTINCT attendance.date) AS days, attendance.username 
	FROM attendance where attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') 
	AND STR_TO_DATE(toDate,'%d/%m/%Y')  GROUP BY attendance.username 
	) AS nodays ON (nodays.username = attendance.username) 
INNER JOIN 
   (SELECT  attendance.username ,
	TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(
	(CASE WHEN ((TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) > TIME('00:00:00')) = 1)
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	ELSE (CASE WHEN (TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) = TIME('00:00:00')) 
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))  ELSE 
	TIME(TIMEDIFF(CONCAT(DATE_ADD(attendance.date, INTERVAL 1 DAY ),' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	END)  END) 
	))),'%H:%i:%s') as times 
	FROM attendance WHERE attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y')  and STR_TO_DATE(toDate,'%d/%m/%Y') 
	GROUP BY attendance.username
	) AS totalduration ON (totalduration.username = attendance.username) 
WHERE attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y')
AND STR_TO_DATE(toDate,'%d/%m/%Y') AND student.standard= classid
AND student.isDeleted=0 
GROUP BY attendance.username;


END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_reports_student_getStudentAttendance
DROP PROCEDURE IF EXISTS `orelltalk_reports_student_getStudentAttendance`;
DELIMITER //
CREATE PROCEDURE `orelltalk_reports_student_getStudentAttendance`(
	IN `sid` INT,
	IN `fromDate` VARCHAR(20),
	IN `toDate` VARCHAR(20)
)
BEGIN

SELECT qry.*,  ROUND (TIME_TO_SEC(qry.duration) / 60 ) AS minutee FROM 
( SELECT attendance.id, DATE_FORMAT(attendance.date,'%d/%m/%Y') AS date,  
TIME_FORMAT(attendance.login,'%h:%i %p') as login ,  
COALESCE(TIME_FORMAT(attendance.logout,'%h:%i %p'),TIME_FORMAT(attendance.login,'%h:%i %p'))  AS logout,student.id AS studentId , 
	TIME_FORMAT((CASE WHEN ((TIME(TIMEDIFF(CONCAT(attendance.date,' ',
COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) > TIME('00:00:00')) = 1)
 THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
 ELSE (CASE WHEN (TIME(TIMEDIFF(CONCAT(attendance.date,' ',
COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) = TIME('00:00:00')) 
THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))  ELSE 
TIME(TIMEDIFF(CONCAT(DATE_ADD(attendance.date, INTERVAL 1 DAY),' ',
COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
 END)  END),'%H:%i:%s') AS duration  
FROM attendance  
INNER JOIN student ON student.userName = attendance.username 
WHERE student.id = sid AND (STR_TO_DATE(attendance.date,'%Y-%m-%d')  
BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') and STR_TO_DATE(toDate,'%d/%m/%Y')) 
order by attendance.date DESC ) AS qry ORDER BY qry.id DESC;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_reports_teacher_getAllTeacherAttendance
DROP PROCEDURE IF EXISTS `orelltalk_reports_teacher_getAllTeacherAttendance`;
DELIMITER //
CREATE PROCEDURE `orelltalk_reports_teacher_getAllTeacherAttendance`(
	IN `fromDate` VARCHAR(20),
	IN `toDate` VARCHAR(20)
)
BEGIN

SELECT teacher.id,teacher.name,teacher.image, nologin.logins, nodays.days, 
CONCAT(' ', totalduration.times, ' ' ) AS times , attendance.username  
FROM teacher 
INNER JOIN attendance ON teacher.userName=attendance.username 
INNER JOIN (select COUNT(id) AS logins,attendance.username 
FROM attendance WHERE attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y') 
GROUP BY attendance.username) AS nologin ON (nologin.username = attendance.username) 
INNER JOIN (SELECT COUNT(DISTINCT attendance.date) as days, attendance.username 
FROM attendance WHERE attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y') 
GROUP BY attendance.username) AS nodays  ON (nodays.username = attendance.username)  
INNER JOIN 
(SELECT 
   TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(
	(CASE WHEN ((TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) > TIME('00:00:00')) = 1)
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	ELSE (CASE WHEN (TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) = TIME('00:00:00')) 
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))  ELSE 
	TIME(TIMEDIFF(CONCAT(DATE_ADD(attendance.date, INTERVAL 1 DAY ),' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	END)  END) 
	))),'%H:%i:%s') AS times ,
attendance.username FROM attendance WHERE attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y') 
GROUP BY attendance.username) AS totalduration  on (totalduration.username = attendance.username) 
WHERE attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y')
GROUP by attendance.username;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_sortLessonScoreClasswise
DROP PROCEDURE IF EXISTS `orelltalk_sortLessonScoreClasswise`;
DELIMITER //
CREATE PROCEDURE `orelltalk_sortLessonScoreClasswise`(
	IN `classId` INT,
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50)
)
BEGIN
SELECT COUNT(st.id) AS `count`,st.id,st.image,st.`name`,lesson.id AS lessonId, lesson.`name` AS lessonName, 
exercise.id AS exercise, se.id AS submittedexercise,se.createdDate AS `date`
FROM student st 
INNER JOIN submittedexercise se ON se.studentId=st.id 
INNER JOIN exercise ON exercise.id=se.exerciseId 
INNER JOIN lesson ON lesson.id=exercise.lessonId 
WHERE st.standard=classId AND (STR_TO_DATE(se.createdDate,'%Y-%m-%d') 
BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y')) 
GROUP BY st.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_sortLessonScoreGroupwise
DROP PROCEDURE IF EXISTS `orelltalk_sortLessonScoreGroupwise`;
DELIMITER //
CREATE PROCEDURE `orelltalk_sortLessonScoreGroupwise`(
	IN `classId` INT,
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50),
	IN `groupId` INT
)
BEGIN
SELECT COUNT(st.id) AS `count`,st.id,st.image,st.`name`,lesson.id AS lessonId, lesson.`name` AS lessonName, 
exercise.id AS exercise, se.id AS submittedexercise,se.createdDate AS `date`
FROM student st 
INNER JOIN submittedexercise se ON se.studentId=st.id 
INNER JOIN exercise ON exercise.id=se.exerciseId 
INNER JOIN lesson ON lesson.id=exercise.lessonId 
WHERE st.standard=classId AND (STR_TO_DATE(se.createdDate,'%Y-%m-%d') 
BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y')) 
AND st.groupId=groupId
GROUP BY st.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_sortOfflineHomeworkReport
DROP PROCEDURE IF EXISTS `orelltalk_sortOfflineHomeworkReport`;
DELIMITER //
CREATE PROCEDURE `orelltalk_sortOfflineHomeworkReport`(
	IN `classId` INT,
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50)
)
BEGIN
SELECT student.id,student.`name`, student.image, COUNT(student.id) AS `count` 
FROM homework
JOIN assignedhomework ON (homework.id=assignedhomework.homeWorkId)
JOIN studenthomeworkcompleted ON studenthomeworkcompleted.assignedHomeWorkId=assignedhomework.id
JOIN student ON student.id=studenthomeworkcompleted.sid 
WHERE student.standard=classId AND homework.isDeleted=0 
AND (STR_TO_DATE(studenthomeworkcompleted.createdDate,'%Y-%m-%d') BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y')) 
GROUP BY student.id;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_sortOfflineHomeworkStudentReport
DROP PROCEDURE IF EXISTS `orelltalk_sortOfflineHomeworkStudentReport`;
DELIMITER //
CREATE PROCEDURE `orelltalk_sortOfflineHomeworkStudentReport`(
	IN `studentId` INT,
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50)
)
BEGIN
SELECT homework.description,homework.name,student.name AS studentName, studenthomeworkcompleted.fileName, 
studenthomeworkcompleted.id AS studentHomeWorkCompletedId, studenthomeworkcompleted.feedback 
FROM homework
JOIN assignedhomework ON (homework.id=assignedhomework.homeWorkId)
JOIN studenthomeworkcompleted ON studenthomeworkcompleted.assignedHomeWorkId=assignedhomework.id 
JOIN student ON student.id=studenthomeworkcompleted.sid 
WHERE student.id=studentId AND homework.isDeleted=0
AND (STR_TO_DATE(studenthomeworkcompleted.createdDate,'%Y-%m-%d') 
BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y'));
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.orelltalk_viewActivity
DROP PROCEDURE IF EXISTS `orelltalk_viewActivity`;
DELIMITER //
CREATE PROCEDURE `orelltalk_viewActivity`(
	IN `studActivityId` INT
)
BEGIN
SELECT studentactivity.voice, (CASE WHEN studentactivity.lessonType=3  THEN 'ROLEPLAY' 
WHEN studentactivity.lessonType='0' THEN 'MIXED AUDIO'   
WHEN studentactivity.lessonType!='' AND studentactivity.lessonType!=2 
THEN studentactivity.lessonType ELSE 'REPEAT' END) as lessonType ,studentactivity.studCMS,
studentactivity.feedback,DATE_FORMAT(studentactivity.createdDate,'%d/%m/%Y') AS date,
module.moduleName,lesson.id,lesson.name,
(CASE WHEN studentactivity.lessonType='CMS' THEN lesson.listenFile 
WHEN studentactivity.lessonType=3  THEN  lesson.roleplayFile 
ELSE (CASE WHEN lesson.repeatFile='' THEN lesson.listenFile ELSE lesson.repeatFile END)	 
END) as listenFile,
(CASE WHEN studentactivity.lessonType!=3 THEN lesson.repeatFilePdf
ELSE lesson.roleplayFilePdf END) as listenFilePdf
FROM studentactivity JOIN lesson ON lesson.id=studentactivity.lessonId 
JOIN module ON module.id=lesson.module WHERE studentactivity.id=studActivityId;
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.reports_getStudentActivityDetails
DROP PROCEDURE IF EXISTS `reports_getStudentActivityDetails`;
DELIMITER //
CREATE PROCEDURE `reports_getStudentActivityDetails`(
	IN `id` INT,
	IN `subjectid` INT,
	IN `fromdate` VARCHAR(50),
	IN `todate` VARCHAR(50)
)
BEGIN

SELECT s.id,s.feedback,student.standard,DATE_FORMAT(s.createdDate, '%d/%m/%Y')AS createdDate, 
	s.`file`, lesson.`name` lesson,a.`name`,f3.name AS subject
FROM assignmentstudent s 
INNER JOIN assignment a ON a.id=s.assignmentId AND a.isDeleted=0
INNER JOIN lesson ON lesson.id=a.lessonId  AND lesson.isDeleted=0
INNER JOIN assignedmodules am ON am.moduleId=lesson.id AND am.`type`='lesson' AND am.isDeleted=0
INNER JOIN fourthfolder f4 ON f4.id=am.folderId AND am.folderType='fourth' AND f4.isDeleted=0
INNER JOIN thirdfolder f3 ON f3.id=f4.thirdFolderId AND f3.isDeleted=0
INNER JOIN student ON student.id=s.studentId

WHERE student.id=id AND s.isDeleted=0 AND CASE WHEN subjectid!=0 THEN f3.id=subjectid ELSE 1=1 END ;


END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.reports_student_classWiseAttendance
DROP PROCEDURE IF EXISTS `reports_student_classWiseAttendance`;
DELIMITER //
CREATE PROCEDURE `reports_student_classWiseAttendance`(
	IN `classid` INT,
	IN `fromDate` VARCHAR(20),
	IN `toDate` VARCHAR(20)
)
BEGIN

SELECT student.id,student.name,student.image, nologin.logins, nodays.days, 
totalduration.times as times, attendance.username 
FROM student 
INNER JOIN attendance on (student.id = attendance.userid AND attendance.userrole='STUDENT') 
INNER JOIN 
   (SELECT COUNT(id) as logins, attendance.userid 
   FROM attendance WHERE attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') 
	AND STR_TO_DATE(toDate,'%d/%m/%Y') AND attendance.userrole='STUDENT'
	GROUP BY attendance.userid
	) AS nologin ON (nologin.userid = attendance.userid) 
INNER JOIN 
   (SELECT COUNT(DISTINCT attendance.date) AS days, attendance.userid 
	FROM attendance where attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') 
	AND STR_TO_DATE(toDate,'%d/%m/%Y') AND attendance.userrole='STUDENT' GROUP BY attendance.userid 
	) AS nodays ON (nodays.userid = attendance.userid) 
INNER JOIN 
   (SELECT  attendance.userid ,
	TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(
	(CASE WHEN ((TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) > TIME('00:00:00')) = 1)
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	ELSE (CASE WHEN (TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) = TIME('00:00:00')) 
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))  ELSE 
	TIME(TIMEDIFF(CONCAT(DATE_ADD(attendance.date, INTERVAL 1 DAY ),' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	END)  END) 
	))),'%H:%i:%s') as times 
	FROM attendance WHERE attendance.userrole='STUDENT' AND attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y')  and STR_TO_DATE(toDate,'%d/%m/%Y') 
	GROUP BY attendance.userid
	) AS totalduration ON (totalduration.userid = attendance.userid) 
WHERE attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y')
AND STR_TO_DATE(toDate,'%d/%m/%Y') AND student.standard= classid
AND student.isDeleted=0 AND attendance.userrole='STUDENT'
GROUP BY attendance.userid;


END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.reports_student_getStudentAttendance
DROP PROCEDURE IF EXISTS `reports_student_getStudentAttendance`;
DELIMITER //
CREATE PROCEDURE `reports_student_getStudentAttendance`(
	IN `sid` INT,
	IN `fromDate` VARCHAR(20),
	IN `toDate` VARCHAR(20)
)
BEGIN

SELECT qry.*,  ROUND (TIME_TO_SEC(qry.duration) / 60 ) AS minutee FROM 
( SELECT attendance.id, DATE_FORMAT(attendance.date,'%d/%m/%Y') AS date,  
TIME_FORMAT(attendance.login,'%h:%i %p') as login ,  
COALESCE(TIME_FORMAT(attendance.logout,'%h:%i %p'),TIME_FORMAT(attendance.login,'%h:%i %p'))  AS logout,student.id AS studentId , 
	TIME_FORMAT((CASE WHEN ((TIME(TIMEDIFF(CONCAT(attendance.date,' ',
COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) > TIME('00:00:00')) = 1)
 THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
 ELSE (CASE WHEN (TIME(TIMEDIFF(CONCAT(attendance.date,' ',
COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) = TIME('00:00:00')) 
THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))  ELSE 
TIME(TIMEDIFF(CONCAT(DATE_ADD(attendance.date, INTERVAL 1 DAY),' ',
COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
 END)  END),'%H:%i:%s') AS duration  
FROM attendance  
INNER JOIN student ON student.id = attendance.userid AND attendance.userrole='STUDENT' 
WHERE student.id = sid AND (STR_TO_DATE(attendance.date,'%Y-%m-%d')  
BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') and STR_TO_DATE(toDate,'%d/%m/%Y')) 
order by attendance.date DESC ) AS qry ORDER BY qry.id DESC;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.reports_student_getStudentAttendanceGroupByDate
DROP PROCEDURE IF EXISTS `reports_student_getStudentAttendanceGroupByDate`;
DELIMITER //
CREATE PROCEDURE `reports_student_getStudentAttendanceGroupByDate`(
	IN `sid` INT,
	IN `fromDate` VARCHAR(50),
	IN `toDate` VARCHAR(50)
)
BEGIN
 
SELECT attendance.id,attendance.username , DATE_FORMAT(attendance.date,'%d/%m/%Y') AS `date` , 
  TIME_FORMAT(MIN(attendance.login),'%h:%i %p') as login,
  COALESCE(TIME_FORMAT(MAX(attendance.logout),'%h:%i %p'),TIME_FORMAT(MAX(attendance.login),'%h:%i %p'))  AS logout,
	TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(
	(CASE WHEN ((TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) > TIME('00:00:00')) = 1)
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	ELSE (CASE WHEN (TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) = TIME('00:00:00')) 
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))  ELSE 
	TIME(TIMEDIFF(CONCAT(DATE_ADD(attendance.date, INTERVAL 1 DAY ),' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	END)  END) 
	))),'%H:%i:%s') as duration,
	   SUM(TIME_TO_SEC(
	(CASE WHEN ((TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) > TIME('00:00:00')) = 1)
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	ELSE (CASE WHEN (TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) = TIME('00:00:00')) 
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))  ELSE 
	TIME(TIMEDIFF(CONCAT(DATE_ADD(attendance.date, INTERVAL 1 DAY ),' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	END)  END) 
	)) AS minutee 
	 
	FROM attendance JOIN student ON student.id = attendance.userid AND attendance.userrole='STUDENT' 
	where attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y')  and STR_TO_DATE(toDate,'%d/%m/%Y') AND student.id = sid
	GROUP BY attendance.date
   ORDER BY attendance.id DESC;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.reports_teacher_getAllTeacherAttendance
DROP PROCEDURE IF EXISTS `reports_teacher_getAllTeacherAttendance`;
DELIMITER //
CREATE PROCEDURE `reports_teacher_getAllTeacherAttendance`(
	IN `fromDate` VARCHAR(20),
	IN `toDate` VARCHAR(20)
)
BEGIN

SELECT teacher.id,teacher.name,teacher.image, nologin.logins, nodays.days, 
CONCAT(' ', totalduration.times, ' ' ) AS times , attendance.username  
FROM teacher 
INNER JOIN attendance ON teacher.id=attendance.userid 
	AND (attendance.userrole='TEACHER' OR attendance.userrole='ADMIN') 
INNER JOIN (select COUNT(id) AS logins,attendance.userid 
FROM attendance WHERE attendance.userrole='TEACHER' OR attendance.userrole='ADMIN' 
AND attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y') 
GROUP BY attendance.userid) AS nologin ON (nologin.userid = attendance.userid) 
INNER JOIN (SELECT COUNT(DISTINCT attendance.date) as days, attendance.userid 
FROM attendance WHERE attendance.userrole='TEACHER' OR attendance.userrole='ADMIN' 
AND attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y') 
GROUP BY attendance.userid) AS nodays  ON (nodays.userid = attendance.userid)  
INNER JOIN 
(SELECT 
   TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(
	(CASE WHEN ((TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) > TIME('00:00:00')) = 1)
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	ELSE (CASE WHEN (TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) = TIME('00:00:00')) 
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))  ELSE 
	TIME(TIMEDIFF(CONCAT(DATE_ADD(attendance.date, INTERVAL 1 DAY ),' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	END)  END) 
	))),'%H:%i:%s') AS times ,
attendance.userid FROM attendance WHERE attendance.userrole='TEACHER' OR attendance.userrole='ADMIN'
AND attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y') 
GROUP BY attendance.userid) AS totalduration  on (totalduration.userid = attendance.userid) 
WHERE attendance.userrole='TEACHER' OR attendance.userrole='ADMIN'
AND attendance.date BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') AND STR_TO_DATE(toDate,'%d/%m/%Y')
GROUP by attendance.userid;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.reports_teacher_getTeacherReports
DROP PROCEDURE IF EXISTS `reports_teacher_getTeacherReports`;
DELIMITER //
CREATE PROCEDURE `reports_teacher_getTeacherReports`(
	IN `tid` VARCHAR(10),
	IN `fromDate` VARCHAR(20),
	IN `toDate` VARCHAR(20)
)
BEGIN

SELECT qry.*, ROUND (TIME_TO_SEC(qry.duration) / 60 ) AS minutee FROM ( 
select attendance.id,DATE_FORMAT(attendance.date,'%d/%m/%Y') AS `date`,  
TIME_FORMAT(attendance.login,'%h:%i %p') AS login,TIME_FORMAT(COALESCE(attendance.logout,attendance.login),'%h:%i %p') AS logout,teacher.id AS teacherId, 

 	TIME_FORMAT(SEC_TO_TIME(TIME_TO_SEC(
	(CASE WHEN ((TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) > TIME('00:00:00')) = 1)
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	ELSE (CASE WHEN (TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login))) = TIME('00:00:00')) 
	THEN TIME(TIMEDIFF(CONCAT(attendance.date,' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))  ELSE 
	TIME(TIMEDIFF(CONCAT(DATE_ADD(attendance.date, INTERVAL 1 DAY ),' ',
	COALESCE(attendance.logout,attendance.login)),CONCAT(attendance.date,' ',attendance.login)))
	END)  END) 
	)),'%H:%i:%s') as duration  
 
FROM attendance 
INNER JOIN teacher ON teacher.userName = attendance.username 
WHERE teacher.id= tid AND (STR_TO_DATE(attendance.date,'%Y-%m-%d') 
BETWEEN STR_TO_DATE(fromDate,'%d/%m/%Y') and STR_TO_DATE(toDate,'%d/%m/%Y'))  order by attendance.date DESC ) AS qry;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.report_getExamWiseStudentReports
DROP PROCEDURE IF EXISTS `report_getExamWiseStudentReports`;
DELIMITER //
CREATE PROCEDURE `report_getExamWiseStudentReports`(
	IN `studentId` INT,
	IN `timeStampStr` VARCHAR(30),
	IN `standard` INT,
	IN `examType` INT
)
BEGIN

If examType = 0  THEN

  SELECT student.id,student.standard,student.name,tab.* , COALESCE(student.division,'') AS division FROM student  LEFT JOIN ( 
				 		 SELECT mark.mark,mark.grade,exam.totalMark, (case when (exam.passMark != '' && exam.passMark IS NOT null) THEN 
				 		 (case when (exam.passMark > mark.mark) then 'Fail' ELSE 'Pass' END) ELSE '' END) AS examStatus , exam.passMark , assignedexam.id, 
				         student.`name`,student.id AS studentid FROM student  
						 JOIN mark ON mark.sid = student.id 
						 JOIN assignedexam ON mark.assignedExamId = assignedexam.id 
						 JOIN exam ON exam.id = assignedexam.examId WHERE assignedexam.id = studentId 
						 AND mark.examType = 0 AND student.isDeleted = 0  AND assignedexam.isDeleted = 0 
						 AND exam.isDeleted = 0  AND student.standard = standard AND mark.examType = 0 ) AS tab ON tab.studentid = student.id 
						  WHERE student.standard = standard AND student.isDeleted = 0  ORDER BY tab.studentid DESC ; 

ELSEIF examType = '1' THEN

SELECT student.id,student.standard,student.name,tab.* , COALESCE(student.division,'') AS division  FROM student 
			 		  LEFT JOIN (  SELECT mark.mark,mark.grade,newexam.totalmark as totalMark, 
			 		  (case when (newexam.passMark != '' && newexam.passMark IS NOT null ) THEN 
			 		  (case when (newexam.passMark > mark.mark) then 'Fail' ELSE 'Pass' END) ELSE '' END) AS  examStatus , 
			 		 newexam.passMark , assignednewexam.id ,  student.`name`, student.id AS studentid , 
			 		 assignednewexam.groupTimeStamp FROM student  JOIN mark ON mark.sid = student.id 
			 		 JOIN assignednewexam ON mark.assignedExamId = assignednewexam.id 
			 		 JOIN newexam ON newexam.id = assignednewexam.examId WHERE 
			 		 assignednewexam.groupTimeStamp = timeStampStr AND 
			 		 mark.examType = 1 AND student.isDeleted = 0  AND assignednewexam.isDeleted =0 
			 		 AND newexam.isDeleted = 0  AND student.standard = standard AND mark.examType = 1  ) 
			 		 AS tab ON tab.studentid = student.id   WHERE student.standard = standard AND 
			 		 student.isDeleted = 0 ORDER BY tab.studentid DESC;
 
END IF;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.retrieveExamFileTimeQuestions
DROP PROCEDURE IF EXISTS `retrieveExamFileTimeQuestions`;
DELIMITER //
CREATE PROCEDURE `retrieveExamFileTimeQuestions`(
	IN `eid` VARCHAR(50)
)
BEGIN

SELECT * FROM assignedexamfile JOIN examfile ON examfile.id = assignedexamfile.examId 
JOIN examfilelist ON examfile.id = examfilelist.examFileId  WHERE examfile.isDeleted = 0 
AND assignedexamfile.isDeleted = 0 AND examfilelist.isDeleted = 0 AND assignedexamfile.id = eid;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.split_up
DROP PROCEDURE IF EXISTS `split_up`;
DELIMITER //
CREATE PROCEDURE `split_up`(
	IN `StringValue` LONGTEXT,
	IN `DelimiterChar` VARCHAR(50)
)
BEGIN

 DECLARE INDX INT;

 DECLARE SLICE varchar(4000); 

 DECLARE tempVariable INT;

 SET INDX = 1;

 set tempVariable=StringValue;

 DROP TEMPORARY TABLE IF EXISTS results;

 CREATE TEMPORARY TABLE results(Items VARCHAR(100));

 select exam.selectedQuestions into @str from assignedexam join exam on assignedexam.examId=exam.id  where assignedexam.id=StringValue;

 MyLoop: WHILE INDX != 0 DO

 

 SET INDX = LOCATE(',',@str);

 

 IF INDX !=0 THEN

  SET SLICE = LEFT(@str,INDX - 1);

 ELSE

  SET SLICE = @str;

 END IF;

 

 INSERT INTO results(Items) VALUES(SLICE);

 

 SET @str = RIGHT(@str,LENGTH(@str) - INDX);

 

 IF LENGTH(@str) = 0 THEN

  LEAVE MyLoop;

 END IF;

 END WHILE;

select exam.totalMark into tempVariable from exam join assignedexam on exam.id=assignedexam.examId where assignedexam.id=tempVariable;

select exercise.id,exercise.question,exercise.type,exercise.file,options.opt1,options.opt2,options.opt3,options.opt4,options.opt5,
options.opt1File,options.opt2File,options.opt3File,options.opt4File,options.opt5File 
from  results join exercise on (results.Items=exercise.id) left join options on (exercise.id=options.exerciseId) ORDER BY RAND() limit tempVariable ; 

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.stored_getFolderLessonsForOnlineHomework
DROP PROCEDURE IF EXISTS `stored_getFolderLessonsForOnlineHomework`;
DELIMITER //
CREATE PROCEDURE `stored_getFolderLessonsForOnlineHomework`(
	IN `studentId` VARCHAR(50)










)
BEGIN


select student.standard into @classid from student where student.id=studentId;


select student.groupId into @groupid from student where student.id=studentId;

select   lesson.id,lesson.name, folder.name as folderName ,secondfolder.name as secondFolderName ,module.moduleName,part.id as partId,module.id as moduleId  from onlinehomework 
left join secondfolder on  (secondfolder.id = onlinehomework.moduleTypeId )
left join folder on(folder.id = secondfolder.folderId)
left join thirdfolder on (thirdfolder.secondFolderId = secondfolder.id )
left join fourthfolder on (thirdfolder.id=fourthfolder.thirdFolderId )
left join fifthfolder on (fifthfolder.fourthFolderId = fourthfolder.id )
left join sixthfolder on (fifthfolder.id = sixthfolder.id ) 
left join assignedmodules on ( (assignedmodules.folderId = secondfolder.id  and assignedmodules.folderType='second') or (assignedmodules.folderId = thirdfolder.id and assignedmodules.folderType='third') or (assignedmodules.folderId = fourthfolder.id and assignedmodules.folderType='fourth') or (assignedmodules.folderId = fifthfolder.id and assignedmodules.folderType='fifth') or (assignedmodules.folderId = sixthfolder.id and assignedmodules.folderType='sixth') )
left join part on ( (part.module=assignedmodules.moduleId && assignedmodules.`type`='module') or (assignedmodules.`type`='part' and assignedmodules.moduleId=part.id))
left join lesson on( (assignedmodules.moduleId=lesson.id && assignedmodules.`type`='lesson') or (assignedmodules.`type`='module' and lesson.partId=part.id) or (assignedmodules.`type`='part' && lesson.partId=part.id) )
left join module on ((assignedmodules.`type`='module' and assignedmodules.moduleId = module.id) or (assignedmodules.`type`='part' and part.module=module.id) or (assignedmodules.`type`='lesson' and part.module=module.id))
where  onlinehomework.moduleType='folder' 
and onlinehomework.assignedTo='student' and onlinehomework.isDeleted=0
 and onlinehomework.assignedToId=studentId and lesson.isDeleted=0 and part.isDeleted=0
 and (select DATE_FORMAT((select CURDATE()), '%d/%m/%Y'))  <= onlinehomework.endDate
 
 union
 
 select   lesson.id,lesson.name, folder.name as folderName ,secondfolder.name as secondFolderName ,module.moduleName,part.id as partId,module.id as moduleId  from onlinehomework 
left join secondfolder on  (secondfolder.id = onlinehomework.moduleTypeId )
left join folder on(folder.id = secondfolder.folderId)
left join thirdfolder on (thirdfolder.secondFolderId = secondfolder.id )
left join fourthfolder on (thirdfolder.id=fourthfolder.thirdFolderId )
left join fifthfolder on (fifthfolder.fourthFolderId = fourthfolder.id )
left join sixthfolder on (fifthfolder.id = sixthfolder.id ) 
left join assignedmodules on ( (assignedmodules.folderId = secondfolder.id  and assignedmodules.folderType='second') or (assignedmodules.folderId = thirdfolder.id and assignedmodules.folderType='third') or (assignedmodules.folderId = fourthfolder.id and assignedmodules.folderType='fourth') or (assignedmodules.folderId = fifthfolder.id and assignedmodules.folderType='fifth') or (assignedmodules.folderId = sixthfolder.id and assignedmodules.folderType='sixth') )
left join part on ( (part.module=assignedmodules.moduleId && assignedmodules.`type`='module') or (assignedmodules.`type`='part' and assignedmodules.moduleId=part.id))
left join lesson on( (assignedmodules.moduleId=lesson.id && assignedmodules.`type`='lesson') or (assignedmodules.`type`='module' and lesson.partId=part.id) or (assignedmodules.`type`='part' && lesson.partId=part.id) )
left join module on ((assignedmodules.`type`='module' and assignedmodules.moduleId = module.id) or (assignedmodules.`type`='part' and part.module=module.id) or (assignedmodules.`type`='lesson' and part.module=module.id))
where  onlinehomework.moduleType='folder' 
and onlinehomework.assignedTo='class' and onlinehomework.isDeleted=0
 and onlinehomework.assignedToId=@classid and lesson.isDeleted=0 and part.isDeleted=0
 and (select DATE_FORMAT((select CURDATE()), '%d/%m/%Y'))  <= onlinehomework.endDate
 
 union
 
  select   lesson.id,lesson.name, folder.name as folderName ,secondfolder.name as secondFolderName ,module.moduleName,part.id as partId,module.id as moduleId  from onlinehomework 
left join secondfolder on  (secondfolder.id = onlinehomework.moduleTypeId )
left join folder on(folder.id = secondfolder.folderId)
left join thirdfolder on (thirdfolder.secondFolderId = secondfolder.id )
left join fourthfolder on (thirdfolder.id=fourthfolder.thirdFolderId )
left join fifthfolder on (fifthfolder.fourthFolderId = fourthfolder.id )
left join sixthfolder on (fifthfolder.id = sixthfolder.id ) 
left join assignedmodules on ( (assignedmodules.folderId = secondfolder.id  and assignedmodules.folderType='second') or (assignedmodules.folderId = thirdfolder.id and assignedmodules.folderType='third') or (assignedmodules.folderId = fourthfolder.id and assignedmodules.folderType='fourth') or (assignedmodules.folderId = fifthfolder.id and assignedmodules.folderType='fifth') or (assignedmodules.folderId = sixthfolder.id and assignedmodules.folderType='sixth') )
left join part on ( (part.module=assignedmodules.moduleId && assignedmodules.`type`='module') or (assignedmodules.`type`='part' and assignedmodules.moduleId=part.id))
left join lesson on( (assignedmodules.moduleId=lesson.id && assignedmodules.`type`='lesson') or (assignedmodules.`type`='module' and lesson.partId=part.id) or (assignedmodules.`type`='part' && lesson.partId=part.id) )
left join module on ((assignedmodules.`type`='module' and assignedmodules.moduleId = module.id) or (assignedmodules.`type`='part' and part.module=module.id) or (assignedmodules.`type`='lesson' and part.module=module.id))
where  onlinehomework.moduleType='folder' 
and onlinehomework.assignedTo='group' and onlinehomework.isDeleted=0
 and onlinehomework.assignedToId=@groupid and lesson.isDeleted=0 and part.isDeleted=0
 and (select DATE_FORMAT((select CURDATE()), '%d/%m/%Y'))  <= onlinehomework.endDate;
 
  

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.stored_getLessonInsideFolder
DROP PROCEDURE IF EXISTS `stored_getLessonInsideFolder`;
DELIMITER //
CREATE PROCEDURE `stored_getLessonInsideFolder`(
	IN `studentId` INT



)
BEGIN


select student.standard into @classid from student where student.id=studentId;


select student.groupId into @groupid from student where student.id=studentId;

SELECT distinct onlinehomework.id as homeworkId,assignedmodules.type,
COALESCE(lesson.module,part.module,module.id) AS moduleId,  
COALESCE(lesson.partId,part.id) as partId,  
lesson.id , assignedmodules.moduleId,lesson.`name`,   
assignedmodules.folderType,assignedmodules.folderId,  
sixthfolder.NAME AS folder6, fifthfolder.NAME AS folder5,  
fourthfolder.NAME AS folder4, thirdfolder.NAME AS moduleName,  
secondfolder.NAME AS secondFolderName, folder.NAME AS folderName  
from onlinehomework      
LEFT JOIN assignedmodules ON assignedmodules.id=onlinehomework.moduleTypeId and onlinehomework.moduleType='lessonInFolder'  
LEFT JOIN module ON module.id=assignedmodules.moduleId AND assignedmodules.type='module' AND module.isDeleted=0 
LEFT JOIN part ON (CASE WHEN (assignedmodules.type='part') THEN part.id=assignedmodules.moduleId  
WHEN (assignedmodules.`type`='module')  THEN part.module=module.id END) AND part.isDeleted=0 
LEFT JOIN lesson ON (CASE WHEN (assignedmodules.type='lesson') THEN  lesson.id=assignedmodules.moduleId   
WHEN (assignedmodules.`type`='module' OR assignedmodules.`type`='part')  THEN lesson.partId=part.id END) AND lesson.isDeleted=0 
LEFT JOIN sixthfolder ON assignedmodules.folderId=sixthfolder.id AND assignedmodules.folderType='sixth' AND sixthfolder.isDeleted=0
LEFT JOIN fifthfolder ON (CASE WHEN (assignedmodules.folderType='fifth')   
	THEN assignedmodules.folderId=fifthfolder.id ELSE sixthfolder.fifthFolderId = fifthfolder.id END) AND fifthfolder.isDeleted=0 
LEFT JOIN fourthfolder ON (CASE WHEN (assignedmodules.folderType='fourth')   
THEN assignedmodules.folderId=fourthfolder.id ELSE fifthfolder.fourthFolderId = fourthfolder.id END) AND fourthfolder.isDeleted=0 
LEFT JOIN thirdfolder ON (CASE WHEN (assignedmodules.folderType='third')   
THEN assignedmodules.folderId=thirdfolder.id ELSE fourthfolder.thirdFolderId = thirdfolder.id END) AND thirdfolder.isDeleted=0	  
LEFT JOIN secondfolder ON (CASE WHEN (assignedmodules.folderType='second')   
THEN assignedmodules.folderId=secondfolder.id ELSE thirdfolder.secondFolderId = secondfolder.id END) AND secondfolder.isDeleted=0  
LEFT JOIN folder ON (CASE WHEN (assignedmodules.folderType='first')   
THEN assignedmodules.folderId=folder.id ELSE secondfolder.folderId = folder.id END) AND folder.isDeleted=0	  
 where (case when onlinehomework.assignedTo='class' then onlinehomework.assignedToId=@classid
  when onlinehomework.assignedTo='student' then onlinehomework.assignedToId=studentId
 else onlinehomework.assignedToId=@groupid  END)
 and (select DATE_FORMAT((select CURDATE()), '%d/%m/%Y'))  <= onlinehomework.endDate
 GROUP BY lesson.id; 


END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.stored_getSelfStudyModules
DROP PROCEDURE IF EXISTS `stored_getSelfStudyModules`;
DELIMITER //
CREATE PROCEDURE `stored_getSelfStudyModules`(
	IN `studentId` VARCHAR(50)
)
BEGIN

select student.standard into @classid from student where student.id=studentId;


select student.groupId into @groupid from student where student.id=studentId;

SELECT DISTINCT selfstudy.moduleId as id, COALESCE(module.moduleName,folder.name)AS moduleName,
teacher.id AS teacherid,COALESCE(module.moduleType,'ELT Studio') AS moduleType,
COALESCE(selfstudy.assignedType,0) as assignedType
FROM selfstudy
LEFT JOIN module ON module.id=selfstudy.moduleId AND COALESCE(selfstudy.assignedType,0)=0 AND module.isDeleted=0
LEFT JOIN folder ON folder.id=selfstudy.moduleId AND COALESCE(selfstudy.assignedType,0)=1 AND folder.isDeleted=0
INNER JOIN teacher ON teacher.userName=COALESCE(module.createdBy,folder.createdBy) AND teacher.isDeleted=0
WHERE (CASE WHEN selfstudy.assignedTo='class' THEN selfstudy.assignedToId=@classid
WHEN selfstudy.assignedTo='group' THEN selfstudy.assignedToId=@groupid 
WHEN selfstudy.assignedTo='student' THEN selfstudy.assignedToId=studentId  END)
AND selfstudy.isDeleted=0;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.stored_getThingsToDoForOnlineHomework
DROP PROCEDURE IF EXISTS `stored_getThingsToDoForOnlineHomework`;
DELIMITER //
CREATE PROCEDURE `stored_getThingsToDoForOnlineHomework`(
	IN `studentId` VARCHAR(50)



)
BEGIN
 
    select  student.standard into @standard from student join typesclass  on student.standard = typesclass.id where student.id=studentId;
    
    select student.groupId into @groupid from student join groups on groups.id = student.groupId where student.id=studentId and student.isDeleted=0 and groups.isDeleted=0;
   

   select  lesson.id, lesson.name,part.id as partId,part.part , module.moduleName,module.id as moduleId  from onlinehomework
	inner join part on part.id = onlinehomework.assignedToId
	inner join lesson on lesson.partId = part.id
	inner join module on module.id = part.module 
 	where 
	  onlinehomework.assignedTo='student' and onlinehomework.isDeleted=0 and onlinehomework.moduleType='part'
	   and (select DATE_FORMAT((select CURDATE()), '%d/%m/%Y'))  <= onlinehomework.endDate and onlinehomework.assignedToId=studentId
	   and  part.isDeleted=0 and lesson.isDeleted=0 and part.isDeleted=0 and module.isDeleted=0
	   
	 union  
	   
   select lesson.id, lesson.name,part.id as partId,part.part , module.moduleName,module.id as moduleId  from onlinehomework
	inner join lesson on lesson.id = onlinehomework.moduleTypeId
	inner join part on part.id = lesson.partId	
	inner join module on module.id = part.module 
 	where 
	  onlinehomework.assignedTo='student' and onlinehomework.isDeleted=0 and onlinehomework.moduleType='lesson'
	   and (select DATE_FORMAT((select CURDATE()), '%d/%m/%Y'))  <= onlinehomework.endDate and onlinehomework.assignedToId=studentId
	   and  part.isDeleted=0 and lesson.isDeleted=0 and part.isDeleted=0 and module.isDeleted=0 
	   
	union
	
select lesson.id, lesson.name,part.id as partId,part.part , module.moduleName,module.id as moduleId from onlinehomework 
join part on part.id = onlinehomework.moduleTypeId 
join lesson on lesson.partId = part.id
join module on module.id = part.module
where onlinehomework.isDeleted=0 and onlinehomework.moduleType='part' and (select DATE_FORMAT((select CURDATE()), '%d/%m/%Y')) <= onlinehomework.endDate and onlinehomework.assignedTo='class' and lesson.isDeleted=0 and part.isDeleted=0 and module.isDeleted=0 and onlinehomework.assignedToId =  @standard

    union
    
   select lesson.id, lesson.name,part.id as partId,part.part , module.moduleName,module.id as moduleId  from onlinehomework 
join lesson on lesson.id = onlinehomework.moduleTypeId
join part on part.id = lesson.partId 
join module on module.id = part.module
where onlinehomework.isDeleted=0 and onlinehomework.moduleType='lesson' and (select DATE_FORMAT((select CURDATE()), '%d/%m/%Y')) <= onlinehomework.endDate and onlinehomework.assignedTo='class' and lesson.isDeleted=0 and part.isDeleted=0 and module.isDeleted=0 and onlinehomework.assignedToId =  @standard 

   union
	
	
	select lesson.id, lesson.name,part.id as partId,part.part , module.moduleName,module.id as moduleId  from onlinehomework
join part on  part.id = onlinehomework.moduleTypeId
join lesson on lesson.partId = part.id
join module on module.id = part.module
 where onlinehomework.assignedTo='group'  and onlinehomework.assignedToId=@groupid 
 and onlinehomework.moduleType ='part' and part.isDeleted=0 and onlinehomework.isDeleted=0
 and (select DATE_FORMAT((select CURDATE()), '%d/%m/%Y'))  <= onlinehomework.endDate  and part.isDeleted=0 and lesson.isDeleted=0 and module.isDeleted=0
 
 union 
 
 select lesson.id, lesson.name,part.id as partId,part.part , module.moduleName,module.id as moduleId from onlinehomework
join lesson on lesson.id = onlinehomework.moduleTypeId
join part on  part.id = lesson.partId
join module on module.id = part.module
 where onlinehomework.assignedTo='group'  and onlinehomework.assignedToId=@groupid 
 and onlinehomework.moduleType ='lesson' and part.isDeleted=0 and onlinehomework.isDeleted=0
 and (select DATE_FORMAT((select CURDATE()), '%d/%m/%Y'))  <= onlinehomework.endDate  and part.isDeleted=0 and lesson.isDeleted=0 and module.isDeleted=0; 
		 
   
END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.stored_retrieveStudentHomeWork
DROP PROCEDURE IF EXISTS `stored_retrieveStudentHomeWork`;
DELIMITER //
CREATE PROCEDURE `stored_retrieveStudentHomeWork`(
	IN `classId` VARCHAR(50),
	IN `sid` VARCHAR(50)
)
BEGIN
 


(SELECT  homework.*,assignedhomework.id as assignedHomeWorkId , DATE_FORMAT(assignedhomework.createdDate,'%d/%m/%Y %h:%i %p') AS created , false AS isCompleted FROM assignedhomework 
join homework on homework.id=assignedhomework.homeWorkId  
		WHERE (CASE WHEN (COALESCE(assignedhomework.classId,0)!=0)  
			THEN assignedhomework.classId= classId ELSE assignedhomework.studentId= sid END)  
				AND homework.isDeleted = 0 and assignedhomework.id NOT IN (SELECT studenthomeworkcompleted.assignedHomeWorkId FROM studenthomeworkcompleted WHERE studenthomeworkcompleted.sid = sid) ORDER BY assignedhomework.createdDate DESC)
UNION ALL
( select homework.*,studenthomeworkcompleted.id as assignedHomeWorkId,
 DATE_FORMAT(assignedhomework.createdDate,'%d/%m/%Y %h:%i %p') AS created , true AS isCompleted  from assignedhomework join homework on homework.id=assignedhomework.homeWorkId   join studenthomeworkcompleted on studenthomeworkcompleted.assignedHomeWorkId=assignedhomework.id  
  where studenthomeworkcompleted.sid=sid ORDER BY studenthomeworkcompleted.id DESC);


END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.stored_retrieveStudentHomeWorkCompleted
DROP PROCEDURE IF EXISTS `stored_retrieveStudentHomeWorkCompleted`;
DELIMITER //
CREATE PROCEDURE `stored_retrieveStudentHomeWorkCompleted`(
	IN `sid` VARCHAR(10)
)
BEGIN

 select homework.*,assignedhomework.id as assignedHomeWorkId,studenthomeworkcompleted.sid as student ,
 DATE_FORMAT(assignedhomework.createdDate,'%d/%m/%Y %h:%i %p') AS created  from assignedhomework join homework on homework.id=assignedhomework.homeWorkId   join studenthomeworkcompleted on studenthomeworkcompleted.assignedHomeWorkId=assignedhomework.id  
  where studenthomeworkcompleted.sid=sid ORDER BY assignedhomework.id DESC;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.student_getStudentExamFiles
DROP PROCEDURE IF EXISTS `student_getStudentExamFiles`;
DELIMITER //
CREATE PROCEDURE `student_getStudentExamFiles`(
	IN `sid` VARCHAR(20),
	IN `classid` VARCHAR(20),
	IN `hasStartAndEndTime` VARCHAR(50)
)
BEGIN

if hasStartAndEndTime = 'yes' THEN

SELECT examfile.*,TIME_FORMAT(assignedexamfile.startTime,'%h:%i %p') AS startTime,
 TIME_FORMAT(assignedexamfile.endTime,'%h:%i %p') AS endTime,
assignedexamfile.id as aid, DATE_FORMAT(assignedexamfile.examDate,'%d/%m/%Y') AS examDate 
FROM examfile 
JOIN assignedexamfile ON examfile.id=assignedexamfile.examId AND examfile.isDeleted = 0
WHERE  assignedexamfile.statusOver=0 and  assignedexamfile.isDeleted=0 
      and assignedexamfile.examDate >= (select CURDATE()) and (CASE WHEN (COALESCE(assignedexamfile.classId,0)!=0) THEN assignedexamfile.classId=classid
		  ELSE assignedexamfile.studentId = sid END) AND assignedexamfile.id NOT IN 
		  (SELECT efa.assignedExamId FROM examfileanswer efa WHERE efa.sid GROUP BY efa.assignedExamId);
		  
ELSE

SELECT examfile.*,TIME_FORMAT(assignedexamfile.startTime,'%h:%i %p') AS startTime,
 TIME_FORMAT(assignedexamfile.endTime,'%h:%i %p') AS endTime,
assignedexamfile.id as aid, DATE_FORMAT(assignedexamfile.examDate,'%d/%m/%Y') AS examDate 
FROM examfile 
JOIN assignedexamfile ON examfile.id=assignedexamfile.examId AND examfile.isDeleted = 0
WHERE  assignedexamfile.statusOver=0 and  assignedexamfile.isDeleted=0 
      and assignedexamfile.examDate >= (select CURDATE()) and (CASE WHEN (COALESCE(assignedexamfile.classId,0)!=0) THEN assignedexamfile.classId=classid
		  ELSE assignedexamfile.studentId = sid END) ;

END if;		  

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.student_showExamResult
DROP PROCEDURE IF EXISTS `student_showExamResult`;
DELIMITER //
CREATE PROCEDURE `student_showExamResult`(
	IN `sid` INT,
	IN `startDate` VARCHAR(20),
	IN `endDate` VARCHAR(20)
)
BEGIN

select * FROM 
( SELECT assignedexam.id,assignedexam.examDate,exam.examName,exam.totalHour,exam.totalMinute,
 exam.totalMark,mark.mark,mark.grade ,     exam.passMark , 
 ( case when ( mark.mark >= exam.passMark ) THEN 'Pass' ELSE 'FAIL' END) AS examStatus  
 FROM exampublishstatus 
 JOIN mark ON mark.assignedExamId = exampublishstatus.assignedId  AND 
    mark.examType = exampublishstatus.examType  
 JOIN assignedexam ON assignedexam.id = mark.assignedExamId  
 JOIN exam ON exam.id = assignedexam.examId  
 WHERE mark.examType = 0 AND mark.sid = sid
    AND exam.isDeleted = 0 AND assignedexam.isDeleted = 0 
	 AND (DATE_FORMAT(STR_TO_DATE(assignedexam.examDate,'%Y-%m-%d'),'%Y-%m-%d' ) 
	 BETWEEN  DATE_FORMAT(STR_TO_DATE(startDate,'%d/%m/%Y'), '%Y-%m-%d') 
	 AND DATE_FORMAT(STR_TO_DATE(endDate,'%d/%m/%Y'),'%Y-%m-%d') )   
UNION  
SELECT  assignednewexam.id,assignednewexam.examDate,newexam.examName,
   newexam.totalHour,newexam.totalMinute,   newexam.totalMark,mark.mark,mark.grade , 
	newexam.passMark , ( case when ( mark.mark >= newexam.passMark )  
	THEN 'Pass' ELSE 'FAIL' END) AS examStatus  
FROM exampublishstatus 
JOIN mark ON  mark.assignedExamId = exampublishstatus.assignedId 
   AND mark.examType = exampublishstatus.examType  
JOIN assignednewexam ON assignednewexam.id = mark.assignedExamId  
JOIN newexam ON newexam.id = assignednewexam.examId  
WHERE mark.examType = 1 AND mark.sid = sid AND newexam.isDeleted = 0 
   AND assignednewexam.isDeleted = 0  
	AND (DATE_FORMAT(STR_TO_DATE(assignednewexam.examDate,'%Y-%m-%d'),'%Y-%m-%d' )  
	BETWEEN  DATE_FORMAT(STR_TO_DATE(startDate,'%d/%m/%Y'), '%Y-%m-%d') 
	AND  DATE_FORMAT(STR_TO_DATE(endDate,'%d/%m/%Y'),'%Y-%m-%d') )
UNION
SELECT assignedexamfile.id,assignedexamfile.examDate,examfile.examName,
   '' as totalHour,DATE_FORMAT(examfilemark.createdDate,'%d/%m/%Y') as totalMinute, examfile.totalMark, COALESCE(examfilemark.mark,'') AS mark ,examfilemark.grade as grade , 
	examfile.passMark , ( case when ( CAST(examfilemark.mark as SIGNED ) >= 
 cast(examfile.passMark AS SIGNED) )  
	THEN 'Pass' ELSE 'FAIL' END) AS examStatus 
	FROM exampublishstatus 
JOIN examfilemark ON  examfilemark.assignedExamId = exampublishstatus.assignedId 
   AND 2 = exampublishstatus.examType 
   JOIN assignedexamfile ON assignedexamfile.id = examfilemark.assignedExamId  
JOIN examfile ON examfile.id = assignedexamfile.examId  
WHERE  examfile.isDeleted = 0 
   AND assignedexamfile.isDeleted = 0 AND examfilemark.sid = sid
   AND (DATE_FORMAT(STR_TO_DATE(assignedexamfile.examDate,'%Y-%m-%d'),'%Y-%m-%d' )  
	BETWEEN  DATE_FORMAT(STR_TO_DATE(startDate,'%d/%m/%Y'), '%Y-%m-%d') 
	AND  DATE_FORMAT(STR_TO_DATE(endDate,'%d/%m/%Y'),'%Y-%m-%d') ) 	  
) as tab  ORDER BY tab.examDate DESC ;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.teacher_getRegisteredExamFileListById
DROP PROCEDURE IF EXISTS `teacher_getRegisteredExamFileListById`;
DELIMITER //
CREATE PROCEDURE `teacher_getRegisteredExamFileListById`(
	IN `examid` INT
)
BEGIN

SELECT examfilelist.* FROM examfile JOIN examfilelist ON examfile.id = examfilelist.examFileId
WHERE examfile.isDeleted = 0 AND examfilelist.isDeleted = 0 AND examfile.id = examid;

END//
DELIMITER ;

-- Dumping structure for procedure newprocedures.testlessons
DROP PROCEDURE IF EXISTS `testlessons`;
DELIMITER //
CREATE PROCEDURE `testlessons`()
BEGIN
SELECT DISTINCT lesson.`name`,qry.*,lesson.id AS lesson,lesson.module,lesson.partId  
FROM (SELECT COALESCE(a2.folder1,a3.folder1,a4.folder1,a5.folder1,a6.folder1) AS folder1,  
COALESCE(a2.folder2,a3.folder2,a4.folder2,a5.folder2,a6.folder2) AS folder2,  
COALESCE(a3.folder3,a4.folder3,a5.folder3,a6.folder3) AS folder3,  
COALESCE(a4.folder4,a5.folder4,a6.folder4) AS folder4,COALESCE(a5.folder5,a6.folder5) AS folder5,  
COALESCE(a6.folder6) AS folder6,alls.folderId,alls.folderType,  
COALESCE(a6.id6,a5.id5,a4.id4,a3.id3,a2.id2,a2.id1) AS fid, alls.isTextContent,
COALESCE(a6.`type`,a5.`type`,a5.`type`,a4.`type`,a3.`type`,a2.`type`) AS ftype
FROM assignallfolders AS alls  
LEFT JOIN (SELECT 'second' AS `type`, secondfolder.id AS id2,folder.id as id1,folder.NAME AS folder1,secondfolder.NAME AS folder2 FROM secondfolder   
 INNER JOIN folder ON folder.id=secondfolder.folderId AND folder.isDeleted=0 
WHERE secondfolder.isDeleted=0 )AS a2 ON (case when(alls.foldertype='second') then alls.folderId=a2.id2 END)  
LEFT JOIN (SELECT 'third' AS `type`,thirdfolder.id AS id3,secondfolder.id AS id2,folder.NAME AS folder1,secondfolder.NAME AS folder2,thirdfolder.NAME AS folder3  
 FROM thirdfolder INNER JOIN secondfolder ON secondfolder.id=thirdfolder.secondFolderId  AND secondfolder.isDeleted=0 
 INNER JOIN folder ON folder.id=secondfolder.folderId AND folder.isDeleted=0  
WHERE thirdfolder.isDeleted=0 ) AS a3 ON (case when(alls.foldertype='third') then alls.folderId=a3.id3 ELSE a3.id2=a2.id2 END)	  
LEFT JOIN (SELECT 'fourth' AS `type`,fourthfolder.id AS id4,thirdfolder.id AS id3,folder.NAME AS folder1,secondfolder.NAME AS folder2,  
 thirdfolder.NAME AS folder3,fourthfolder.NAME AS folder4 FROM fourthfolder  
 INNER JOIN thirdfolder ON thirdfolder.id=fourthfolder.thirdFolderId AND thirdfolder.isDeleted=0 
 INNER JOIN secondfolder ON secondfolder.id=thirdfolder.secondFolderId  AND secondfolder.isDeleted=0 
 INNER JOIN folder ON folder.id=secondfolder.folderId AND folder.isDeleted=0  
WHERE fourthfolder.isDeleted=0) AS a4 ON (case when(alls.foldertype='fourth') then alls.folderId=a4.id4 ELSE a4.id3=a3.id3 END)
LEFT JOIN (SELECT 'fifth' AS `type`,fifthfolder.id AS id5,fourthfolder.id AS id4,folder.NAME AS folder1,secondfolder.NAME AS folder2,  
 thirdfolder.NAME AS folder3,fourthfolder.NAME AS folder4,fifthfolder.name AS folder5 FROM fifthfolder   
 INNER JOIN fourthfolder ON fourthfolder.id=fifthfolder.fourthFolderId  AND fourthfolder.isDeleted=0 
 INNER JOIN thirdfolder ON thirdfolder.id=fourthfolder.thirdFolderId AND thirdfolder.isDeleted=0 
 INNER JOIN secondfolder ON secondfolder.id=thirdfolder.secondFolderId  AND secondfolder.isDeleted=0 
 INNER JOIN folder ON folder.id=secondfolder.folderId AND folder.isDeleted=0  
WHERE fifthfolder.isDeleted=0 ) AS a5 ON (case when(alls.foldertype='fifth') then alls.folderId=a5.id5 ELSE a5.id4=a4.id4 END)	  
LEFT JOIN (SELECT 'sixth' AS `type`,sixthfolder.id AS id6,fifthfolder.id AS id5,folder.NAME AS folder1,secondfolder.NAME AS folder2,  
 thirdfolder.NAME AS folder3,fourthfolder.NAME AS folder4,fifthfolder.name AS folder5,  
 sixthfolder.name AS folder6 FROM sixthfolder   
 INNER JOIN fifthfolder ON fifthfolder.id=sixthfolder.fifthFolderId AND fifthfolder.isDeleted=0 
 INNER JOIN fourthfolder ON fourthfolder.id=fifthfolder.fourthFolderId AND fourthfolder.isDeleted=0 
 INNER JOIN thirdfolder ON thirdfolder.id=fourthfolder.thirdFolderId AND thirdfolder.isDeleted=0 
 INNER JOIN secondfolder ON secondfolder.id=thirdfolder.secondFolderId AND secondfolder.isDeleted=0 
 INNER JOIN folder ON folder.id=secondfolder.folderId AND folder.isDeleted=0  
WHERE sixthfolder.isDeleted=0)AS a6 ON (case when(alls.foldertype='sixth') then alls.folderId=a6.id6  ELSE a6.id5=a5.id5 END) 	 
WHERE (CASE WHEN (COALESCE(alls.classId,0)!=0) THEN alls.classId=12  
ELSE alls.studentId=0 END) AND STR_TO_DATE('09/07/2020','%d/%m/%Y') BETWEEN STR_TO_DATE(alls.assigningDate,'%d/%m/%Y') 
AND STR_TO_DATE(COALESCE(alls.endDate,alls.assigningDate),'%d/%m/%Y') ORDER BY alls.id) AS qry  
INNER JOIN assignedmodules ON assignedmodules.folderId=qry.fid 
INNER JOIN lesson ON lesson.id=assignedmodules.moduleId AND assignedmodules.`type`='lesson'   
GROUP BY lesson.`name`;
END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
