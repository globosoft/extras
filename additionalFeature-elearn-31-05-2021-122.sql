-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.60 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

UPDATE exercise SET exercise.mark = 1 WHERE exercise.mark IS NULL;
UPDATE exam SET exam.negativeMark = 0  WHERE exam.negativeMark IS null ;
UPDATE newexam SET newexam.negativeMark = 0  WHERE newexam.negativeMark IS null;

-- Dumping structure for table elearn.additionalfeature
DROP TABLE IF EXISTS `additionalfeature`;
CREATE TABLE IF NOT EXISTS `additionalfeature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isActive` bit(1) DEFAULT NULL,
  `isDeleted` bit(1) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Dumping data for table elearn.additionalfeature: 26 rows
/*!40000 ALTER TABLE `additionalfeature` DISABLE KEYS */;
INSERT INTO `additionalfeature` (`id`, `isActive`, `isDeleted`, `name`) VALUES
	(1, b'0', b'0', 'liveclass'),
	(2, b'1', b'0', 'googlemeet'),
	(3, b'1', b'0', 'grade_attendance'),
	(4, b'0', b'0', 'index'),
	(5, b'0', b'0', 'header_logo'),
	(6, b'0', b'0', 'combinedclass'),
	(7, b'0', b'0', 'timetable'),
	(8, b'1', b'0', 'examfile'),
	(9, b'0', b'0', 'assignmentExam_dashboard'),
	(10, b'1', b'0', 'exam_dashboard'),
	(11, b'1', b'0', 'assignment_dashboard'),
	(12, b'1', b'0', 'classview'),
	(13, b'1', b'0', 'studentQueries'),
	(14, b'1', b'0', 'communication'),
	(15, b'1', b'0', 'help'),
	(16, b'1', b'0', 'billboard'),
	(17, b'0', b'0', 'profile_dashboard_position'),
	(18, b'0', b'0', 'assignmentExamReport_dashboard'),
	(19, b'1', b'0', 'passmark_examfile'),
	(20, b'1', b'0', 'start_end_time_examfile'),
	(21, b'1', b'0', 'submitted_date_examfile'),
	(22, b'1', b'0', 'assignment_examfile'),
	(23, b'1', b'0', 'video_crop'),
	(24, b'1', b'0', 'doubts_queries'),
	(25, b'0', b'0', 'publish_exercise'),
	(26, b'1', b'0', 'forgot_password');
/*!40000 ALTER TABLE `additionalfeature` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
